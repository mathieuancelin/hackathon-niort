Liste des paquets à installer
==========================


IDE + emulateur
-----------------

* Andorid Studio 1.la_derniere (http://developer.android.com/sdk/index.html)
* Genymotion  (https://www.genymotion.com/#!/)

SDK + image Tel
----------------------

depuis le SDK manager (icone tete d'android fleche vers le bas dans le studio 
* tools 
    *  Android SDK Tools
	*  Android SDK Platform Tools
	*  Android SDK Build Tools   (20 , 21 et 22)
* API 21    ==> pour faire du android 5.0
	*  SDK Platform
    *  Intel X86 Atom System image	  
* API 15     ==> pour faire du android 4.0 +
    *  SDK Platform
    *  Intel X86 Atom System image	  
* Extras 
    *  Android Support Repository
	*  Android Support Library
	*  Google Repository
	*  Google USBDriver
	*  Intel X86 Emulator Accelerator (HAXM installer)
	  
Dans genymotion il faut recupperer une image en Android 4.0 , une en 4.4 et une autre en 5.0 (on sais jamais :)
	  

configuration de l'emulateur Android : 
http://developer.android.com/tools/devices/emulator.html


	  