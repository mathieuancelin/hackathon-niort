import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.common.collect.Lists;
import models.Location;
import models.Parking;
import models.Station;
import models.User;
import play.Application;
import play.GlobalSettings;
import play.api.libs.Files;
import utils.collection.Container;
import utils.json.Jackson;
import utils.json.JsValue;
import utils.json.Json;
import utils.json.mapping.DefaultReaders;
import utils.json.mapping.JsResult;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class Global extends GlobalSettings {
    @Override
    public void onStart(Application app) {
        final ClassLoader classLoader = this.getClass().getClassLoader();
        SimpleModule module = new SimpleModule("JongoBridge", Version.unknownVersion()) {
            @Override
            public void setupModule(SetupContext setupContext) {
                setupContext.addDeserializers(new Jackson.JsDeserializers(classLoader));
                setupContext.addSerializers(new Jackson.JsSerializers());
            }
        };
        play.libs.Json.setObjectMapper(new ObjectMapper().registerModule(module));
        Parking.store.collection().ensureIndex("{ _l: '2d' }");
        Station.store.collection().ensureIndex("{ location: '2d' }");

        injectParkings();
        injectStations();
        injectMockData();
    }

    @Override
    public void onStop(Application app) {
    }

    public static void injectParkings() {
        JsValue jsValue = Json.parse(Files.readFile(play.Play.application().getFile("/conf/parkings.json")));
        JsResult<List<Parking>> parkings = Json.fromJson(jsValue, DefaultReaders.<Parking>seq(Parking.PARKING_FORMAT));
        parkings.get().forEach(e -> Parking.store.insert(e));
    }

    public static void injectStations() {
        Station.store.delete(Json.obj());
        File file = play.Play.application().getFile("/public/arrets.txt");
        String content = Files.readFile(file);
        Container<String> lines = Container.<String>from(Arrays.asList(content.split("\n"))).skip(1);
        Container<Station> stations = lines.map(l -> Arrays.asList(l.split(",")))
                .map(parts -> new Station(new Location(Double.valueOf(parts.get(2)), Double.valueOf(parts.get(3))), parts.get(1), "BUS", Lists.newArrayList("Ligne 1")))
                .map(station -> station.insert());
    }

    public static void injectMockData() {
        User.store.delete(Json.obj());
        User.store.collection().ensureIndex("{ location: '2d' }");
        User.store.insert(new User("john.doe01@gmail.com", "John", "Doe01", new Location(40.738868, -73.99171)));
        User.store.insert(new User("john.doe02@gmail.com", "John", "Doe02", new Location(-73.988135, 40.741404)));
        User.store.insert(new User("john.doe03@gmail.com", "John", "Doe03", new Location(-73.997812, 40.739128)));
        User.store.insert(new User("john.doe04@gmail.com", "John", "Doe04", new Location(-73.992491, 40.738673)));
        User.store.insert(new User("john.doe05@gmail.com", "John", "Doe05", new Location(-73.992491, 40.738673)));
        User.store.insert(new User("john.doe06@gmail.com", "John", "Doe06", new Location(-73.985839, 40.731698)));
        User.store.insert(new User("john.doe07@gmail.com", "John", "Doe07", new Location(-73.98820, 40.74164)));
        User.store.insert(new User("john.doe08@gmail.com", "John", "Doe08", new Location(-73.99408, 40.75057)));
        User.store.insert(new User("john.doe09@gmail.com", "John", "Doe09", new Location(-73.98602, 40.74894)));
        User.store.insert(new User("john.doe10@gmail.com", "John", "Doe10", new Location(-73.99756, 40.73083)));
        User.store.insert(new User("john.doe11@gmail.com", "John", "Doe11", new Location(106.9154, 47.9245)));
        User.store.insert(new User("john.doe12@gmail.com", "John", "Doe12", new Location(-74.2713, 40.73137)));
    }
}
