package env;

import org.jongo.MongoCollection;
import uk.co.panaxiom.playjongo.PlayJongo;
import utils.concurrent.NamedExecutors;
import utils.elasticsearch.Index;
import utils.json.Json;
import utils.json.Syntax;

import java.util.concurrent.ScheduledExecutorService;

import static utils.json.Syntax.$;

public class Env {

    public final ScheduledExecutorService ec = NamedExecutors.newFixedThreadPool(10, "MongoExecutor");

    public static MongoCollection users() {
        return PlayJongo.getCollection("users");
    }

    public static final Index userIndex = Index.of("users", Syntax.nill(), Json.obj(), Json.obj(
        $("user", Json.obj(
            $("properties", Json.obj(
                $("email", Json.obj($("type", "string"))),
                $("name", Json.obj($("type", "string"))),
                $("surname", Json.obj($("type", "string"))),
                $("location", Json.obj($("type", "geo_point")))
            ))
        ))
    ));
}
