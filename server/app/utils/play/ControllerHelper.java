package utils.play;

import play.api.mvc.Codec;
import play.core.j.JavaResults;
import play.mvc.Result;
import play.mvc.Results;
import utils.json.JsValue;

public class ControllerHelper {

    static Codec utf8 = Codec.javaSupported("utf-8");

    public static Result jsonOk(JsValue value) {
        return new Results.Status(JavaResults.Ok(), value.asJackson(), utf8);
    }

    public static Results.Status jsonBadRequest(JsValue value) {
        return new Results.Status(JavaResults.BadRequest(), value.asJackson(), utf8);
    }

    public static Results.Status jsonNotFound(JsValue value) {
        return new Results.Status(JavaResults.NotFound(), value.asJackson(), utf8);
    }

    public static Results.Status jsonInternalServerError(JsValue value) {
        return new Results.Status(JavaResults.InternalServerError(), value.asJackson(), utf8);
    }
}
