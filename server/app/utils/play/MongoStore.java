package utils.play;

import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.WriteResult;
import org.jongo.MongoCollection;
import play.Logger;
import uk.co.panaxiom.playjongo.PlayJongo;
import utils.collection.Container;
import utils.functional.Option;
import utils.json.Jackson;
import utils.json.JsObject;
import utils.json.JsValue;
import utils.json.Json;
import utils.json.mapping.Format;

import java.util.Iterator;

import static utils.json.Syntax.$;


public class MongoStore<T> {

    private final String collectionName;
    private final Format<T> jsonFormatter;

    private MongoStore(String collectionName, Format<T> jsonFormatter) {
        this.collectionName = collectionName;
        this.jsonFormatter = jsonFormatter;
    }

    public static <T> MongoStore<T> of(String name, Format<T> format) {
        return new MongoStore<>(name, format);
    }

    public MongoCollection collection() {
        return PlayJongo.getCollection(collectionName);
    }

    public WriteResult deleteOne(String _id) {
        return collection().remove(Json.obj($("_id", _id)).stringify());
    }

    public WriteResult delete(JsObject query) {
        return collection().remove(query.stringify());
    }

    public WriteResult delete(String query) {
        return collection().remove(query);
    }

    public T insert(T payload) {
        collection().insert(jsonFormatter.write(payload).asJackson());
        return payload;
    }

    public T update(String _id, T payload) {
        collection().update(Json.obj($("_id", _id)).stringify()).with($("$set", jsonFormatter.write(payload).as(JsObject.class).remove("_id")).stringify());
        return payload;
    }

    public Option<T> findById(String _id) {
        return findOne(Json.obj($("_id", _id)).stringify());
    }

    public Option<T> findOne(JsObject query) {
        return findOne(query.stringify());
    }

    public Option<T> findOne(String query) {
        Iterator<JsonNode> it = collection().find(query).limit(1).as(JsonNode.class).iterator();
        if (it.hasNext()) {
            return Option.apply(Jackson.jsonNodeToJsValue(it.next()).as(jsonFormatter));
        }
        return Option.none();
    }

    public Container<T> find(JsObject query) {
        return Container.from(collection().find(query.stringify()).as(JsonNode.class)).map(Jackson::jsonNodeToJsValue).map(o -> o.as(jsonFormatter));
    }

    public Container<T> find(String query) {
        return Container.from(collection().find(query).as(JsonNode.class)).map(Jackson::jsonNodeToJsValue).map(o -> o.as(jsonFormatter));
    }

    public Container<T> findAll() {
        return Container.from(collection().find().as(JsonNode.class)).map(Jackson::jsonNodeToJsValue).map((JsValue o) -> o.as(jsonFormatter));
    }
}
