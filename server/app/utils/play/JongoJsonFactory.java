package utils.play;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.jongo.Mapper;
import org.jongo.marshall.jackson.JacksonMapper;
import uk.co.panaxiom.playjongo.JongoMapperFactory;
import utils.json.Jackson;

public class JongoJsonFactory implements JongoMapperFactory {

    public JongoJsonFactory() {
    }

    public Mapper create() {
        final ClassLoader classLoader = this.getClass().getClassLoader();
        SimpleModule module = new SimpleModule("JongoBridge", Version.unknownVersion()) {
            @Override
            public void setupModule(SetupContext setupContext) {
                setupContext.addDeserializers(new Jackson.JsDeserializers(classLoader));
                setupContext.addSerializers(new Jackson.JsSerializers());
            }
        };
        return new JacksonMapper.Builder().registerModule(module).build();
    }
}