package utils.functional;

import com.google.common.base.Function;

public class Either<A, B> {

    final public Left<A, B> left;
    final public Right<B, A> right;

    Either(A left, B right) {
        this.left = new Left<A, B>(left, this);
        this.right = new Right<B, A>(right, this);
    }

    public static <A, B> Either<A, B> eitherLeft(A value) {
        return new Either<A, B>(value, null);
    }

    public static <A, B> Either<A, B> eitherRight(B value) {
        return new Either<A, B>(null, value);
    }

    public <A, B> Either<A, B> left(A value) {
        if (value != null) {
            return new Either<A, B>(value, null);
        }
        return new Either(left, right);
    }

    public <A, B> Either<A, B> right(B value) {
        if (value != null) {
            return new Either<A, B>(null, value);
        }
        return new Either(left, right);
    }

    public <A, B> Either<A, B> left(Option<A> value) {
        if (value.isDefined()) {
            return new Either<A, B>(value.get(), null);
        }
        return new Either(left, right);
    }

    public <A, B> Either<A, B> right(Option<B> value) {
        if (value.isDefined()) {
            return new Either<A, B>(null, value.get());
        }
        return new Either(left, right);
    }

    public <X> Option<X> fold(Function<A, X> fa, Function<B, X> fb) {
        if (isLeft()) {
            return Option.apply(fa.apply(left.get()));
        } else if (isRight()) {
            return Option.apply(fb.apply(right.get()));
        } else {
            return (Option<X>) Option.none();
        }
    }

    public boolean isLeft() {
        return left.isDefined();
    }

    public boolean isRight() {
        return right.isDefined();
    }

    public Either<B, A> swap() {
        A vLeft = null;
        B vRight = null;
        if (left.isDefined()) {
            vLeft = left.get();
        }
        if (right.isDefined()) {
            vRight = right.get();
        }
        return new Either<B, A>(vRight, vLeft);
    }

    @Override
    public String toString() {
        return "Either ( left: " + left + ", right: " + right + " )";
    }
}

