package utils.functional;

import com.google.common.base.Objects;

public class Tuple<A, B> extends T2<A, B> {

    public static <A> T1<A> tuple(A _1) {
        return new T1<A>(_1);
    }

    public static <A, B> Tuple<A, B> tuple(A _1, B _2) {
        return new Tuple<A, B>(_1, _2);
    }

    public static <A, B, C> T3<A, B, C> tuple(A _1, B _2, C _3) {
        return new T3<A, B, C>(_1, _2, _3);
    }

    public static <A, B, C, D> T4<A, B, C, D> tuple(A _1, B _2, C _3, D _4) {
        return new T4<A, B, C, D>(_1, _2, _3, _4);
    }

    public static <A, B, C, D, E> T5<A, B, C, D, E> tuple(A _1, B _2, C _3, D _4, E _5) {
        return new T5<A, B, C, D, E>(_1, _2, _3, _4, _5);
    }

    public static <A, B, C, D, E, F> T6<A, B, C, D, E, F> tuple(A _1, B _2, C _3, D _4, E _5, F _6) {
        return new T6<A, B, C, D, E, F>(_1, _2, _3, _4, _5, _6);
    }

    public static <A, B, C, D, E, F, G> T7<A, B, C, D, E, F, G> tuple(A _1, B _2, C _3, D _4, E _5, F _6, G _7) {
        return new T7<A, B, C, D, E, F, G>(_1, _2, _3, _4, _5, _6, _7);
    }

    public static <A, B, C, D, E, F, G, H> T8<A, B, C, D, E, F, G, H> tuple(A _1, B _2, C _3, D _4, E _5, F _6, G _7, H _8) {
        return new T8<A, B, C, D, E, F, G, H>(_1, _2, _3, _4, _5, _6, _7, _8);
    }

    public static <A, B, C, D, E, F, G, H, I> T9<A, B, C, D, E, F, G, H, I> tuple(A _1, B _2, C _3, D _4, E _5, F _6, G _7, H _8, I _9) {
        return new T9<A, B, C, D, E, F, G, H, I>(_1, _2, _3, _4, _5, _6, _7, _8, _9);
    }

    public static <A, B, C, D, E, F, G, H, I, J> T10<A, B, C, D, E, F, G, H, I, J> tuple(A _1, B _2, C _3, D _4, E _5, F _6, G _7, H _8, I _9, J _10) {
        return new T10<A, B, C, D, E, F, G, H, I, J>(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10);
    }

    public static <A> T1<A> of(A _1) {
        return new T1<A>(_1);
    }

    public static <A, B> Tuple<A, B> of(A _1, B _2) {
        return new Tuple<A, B>(_1, _2);
    }

    public static <A, B, C> T3<A, B, C> of(A _1, B _2, C _3) {
        return new T3<A, B, C>(_1, _2, _3);
    }

    public static <A, B, C, D> T4<A, B, C, D> of(A _1, B _2, C _3, D _4) {
        return new T4<A, B, C, D>(_1, _2, _3, _4);
    }

    public static <A, B, C, D, E> T5<A, B, C, D, E> of(A _1, B _2, C _3, D _4, E _5) {
        return new T5<A, B, C, D, E>(_1, _2, _3, _4, _5);
    }

    public static <A, B, C, D, E, F> T6<A, B, C, D, E, F> of(A _1, B _2, C _3, D _4, E _5, F _6) {
        return new T6<A, B, C, D, E, F>(_1, _2, _3, _4, _5, _6);
    }

    public static <A, B, C, D, E, F, G> T7<A, B, C, D, E, F, G> of(A _1, B _2, C _3, D _4, E _5, F _6, G _7) {
        return new T7<>(_1, _2, _3, _4, _5, _6, _7);
    }

    public static <A, B, C, D, E, F, G, H> T8<A, B, C, D, E, F, G, H> of(A _1, B _2, C _3, D _4, E _5, F _6, G _7, H _8) {
        return new T8<A, B, C, D, E, F, G, H>(_1, _2, _3, _4, _5, _6, _7, _8);
    }

    public static <A, B, C, D, E, F, G, H, I> T9<A, B, C, D, E, F, G, H, I> of(A _1, B _2, C _3, D _4, E _5, F _6, G _7, H _8, I _9) {
        return new T9<A, B, C, D, E, F, G, H, I>(_1, _2, _3, _4, _5, _6, _7, _8, _9);
    }

    public static <A, B, C, D, E, F, G, H, I, J> T10<A, B, C, D, E, F, G, H, I, J> of(A _1, B _2, C _3, D _4, E _5, F _6, G _7, H _8, I _9, J _10) {
        return new T10<A, B, C, D, E, F, G, H, I, J>(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10);
    }

    public static <A> T1<A> from(T1<A> t) {
        return of(t._1);
    }

    public static <A, B> T2<A, B> from(T2<A, B> t) {
        return of(t._1, t._2);
    }

    public static <A, B> Tuple<A, B> from(Tuple<A, B> t) {
        return of(t._1, t._2);
    }

    public static <A, B, C> T3<A, B, C> from(T3<A, B, C> t) {
        return of(t._1, t._2, t._3);
    }

    public static <A, B, C, D> T4<A, B, C, D> from(T4<A, B, C, D> t) {
        return of(t._1, t._2, t._3, t._4);
    }

    public static <A, B, C, D, E> T5<A, B, C, D, E> from(T5<A, B, C, D, E> t) {
        return of(t._1, t._2, t._3, t._4, t._5);
    }

    public static <A, B, C, D, E, F> T6<A, B, C, D, E, F> from(T6<A, B, C, D, E, F> t) {
        return of(t._1, t._2, t._3, t._4, t._5, t._6);
    }

    public static <A, B, C, D, E, F, G> T7<A, B, C, D, E, F, G> from(T7<A, B, C, D, E, F, G> t) {
        return of(t._1, t._2, t._3, t._4, t._5, t._6, t._7);
    }

    public static <A, B, C, D, E, F, G, H> T8<A, B, C, D, E, F, G, H> from(T8<A, B, C, D, E, F, G, H> t) {
        return of(t._1, t._2, t._3, t._4, t._5, t._6, t._7, t._8);
    }

    public static <A, B, C, D, E, F, G, H, I> T9<A, B, C, D, E, F, G, H, I> from(T9<A, B, C, D, E, F, G, H, I> t) {
        return of(t._1, t._2, t._3, t._4, t._5, t._6, t._7, t._8, t._9);
    }

    public static <A, B, C, D, E, F, G, H, I, J> T10<A, B, C, D, E, F, G, H, I, J> from(T10<A, B, C, D, E, F, G, H, I, J> t) {
        return of(t._1, t._2, t._3, t._4, t._5, t._6, t._7, t._8, t._9, t._10);
    }

    public Tuple(A _1, B _2) {
        super(_1, _2);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Tuple that = (Tuple) o;

        return Objects.equal(this._1, that._1) &&
                Objects.equal(this._2, that._2);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(_1, _2);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("_1", _1)
                .add("_2", _2)
                .toString();
    }
}