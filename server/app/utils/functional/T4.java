package utils.functional;

import com.google.common.base.Objects;

public class T4<A, B, C, D> {

    public final A _1;
    public final B _2;
    public final C _3;
    public final D _4;

    public T4(A _1, B _2, C _3, D _4) {
        this._1 = _1;
        this._2 = _2;
        this._3 = _3;
        this._4 = _4;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        T4 that = (T4) o;

        return Objects.equal(this._1, that._1) &&
                Objects.equal(this._2, that._2) &&
                Objects.equal(this._3, that._3) &&
                Objects.equal(this._4, that._4);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(_1, _2, _3, _4);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("_1", _1)
                .add("_2", _2)
                .add("_3", _3)
                .add("_4", _4)
                .toString();
    }
}