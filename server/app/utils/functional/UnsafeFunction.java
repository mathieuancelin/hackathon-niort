package utils.functional;

import com.google.common.base.Function;
import com.google.common.base.Throwables;

public abstract class UnsafeFunction<T, R> implements Function<T, R> {
    public R apply(T input) {
        try {
            return perform(input);
        } catch (Throwable e) {
            throw Throwables.propagate(e);
        }
    }
    public abstract R perform(T input) throws Throwable;
}
