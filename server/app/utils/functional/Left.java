package utils.functional;

import com.google.common.base.Function;

import java.util.Collections;
import java.util.Iterator;

public class Left<A, B> implements Iterable<A> {

    private final A input;

    public final Either<A, B> e;

    Left(A value, Either<A, B> e) {
        this.e = e;
        this.input = value;
    }

    public boolean isDefined() {
        return !(input == null);
    }

    public A get() {
        return input;
    }

    @Override
    public Iterator<A> iterator() {
        if (input == null) {
            return Collections.<A>emptyList().iterator();
        } else {
            return Collections.singletonList(input).iterator();
        }
    }

    @Override
    public String toString() {
        return "Left ( " + input + " )";
    }

    public boolean isEmpty() {
        return !isDefined();
    }

    public A getOrElse(A value) {
        return isEmpty() ? value : get();
    }

    public A getOrElse(Function<Unit, A> function) {
        return isEmpty() ? function.apply(Unit.unit()) : get();
    }

    public A getOrNull() {
        return isEmpty() ? null : get();
    }

    public Option<Either<A, B>> filter(Function<A, Boolean> predicate) {
        if (isDefined()) {
            if (predicate.apply(get())) {
                return Option.apply(e);
            } else {
                return Option.none();
            }
        }
        return Option.none();
    }

    public Option<Either<A, B>> filterNot(Function<A, Boolean> predicate) {
        if (isDefined()) {
            if (!predicate.apply(get())) {
                return Option.apply(e);
            } else {
                return Option.none();
            }
        }
        return Option.none();
    }

    public <R> Either<R, B> map(Function<A, R> function) {
        if (isDefined()) {
            return new Either<R, B>(function.apply(get()), null);
        } else {
            return new Either<R, B>(null, e.right.get());
        }
    }

    public <R> Either<R, B> flatMap(Function<A, Either<R, B>> action) {
        if (isDefined()) {
            return action.apply(get());
        } else {
            return new Either<R, B>(null, e.right.get());
        }
    }
}