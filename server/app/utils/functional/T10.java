package utils.functional;

import com.google.common.base.Objects;

public class T10<A, B, C, D, E, F, G, H, I, J> {
    public final A _1;
    public final B _2;
    public final C _3;
    public final D _4;
    public final E _5;
    public final F _6;
    public final G _7;
    public final H _8;
    public final I _9;
    public final J _10;
    public T10(A _1, B _2, C _3, D _4, E _5, F _6, G _7, H _8, I _9, J _10) {
        this._1 = _1;
        this._2 = _2;
        this._3 = _3;
        this._4 = _4;
        this._5 = _5;
        this._6 = _6;
        this._7 = _7;
        this._8 = _8;
        this._9 = _9;
        this._10 = _10;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        T10 that = (T10) o;

        return Objects.equal(this._1, that._1) &&
                Objects.equal(this._2, that._2) &&
                Objects.equal(this._3, that._3) &&
                Objects.equal(this._4, that._4) &&
                Objects.equal(this._5, that._5) &&
                Objects.equal(this._6, that._6) &&
                Objects.equal(this._7, that._7) &&
                Objects.equal(this._8, that._8) &&
                Objects.equal(this._9, that._9) &&
                Objects.equal(this._10, that._10);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("_1", _1)
                .add("_2", _2)
                .add("_3", _3)
                .add("_4", _4)
                .add("_5", _5)
                .add("_6", _6)
                .add("_7", _7)
                .add("_8", _8)
                .add("_9", _9)
                .add("_10", _10)
                .toString();
    }
}
