package utils.functional;

import com.google.common.base.Function;

import java.util.Collections;
import java.util.Iterator;

public class Right<B, A> implements Iterable<B> {

    private final B input;

    public final Either<A, B> e;

    Right(B value, Either<A, B> e) {
        this.e = e;
        this.input = value;
    }

    public boolean isDefined() {
        return !(input == null);
    }

    public B get() {
        return input;
    }

    @Override
    public Iterator<B> iterator() {
        if (input == null) {
            return Collections.<B>emptyList().iterator();
        } else {
            return Collections.singletonList(input).iterator();
        }
    }

    @Override
    public String toString() {
        return "Left ( " + input + " )";
    }

    public boolean isEmpty() {
        return !isDefined();
    }

    public B getOrElse(B value) {
        return isEmpty() ? value : get();
    }

    public B getOrElse(Function<Unit, B> function) {
        return isEmpty() ? function.apply(Unit.unit()) : get();
    }

    public B getOrNull() {
        return isEmpty() ? null : get();
    }

    public Option<Either<A, B>> filter(Function<B, Boolean> predicate) {
        if (isDefined()) {
            if (predicate.apply(get())) {
                return Option.apply(e);
            } else {
                return Option.none();
            }
        }
        return Option.none();
    }

    public Option<Either<A, B>> filterNot(Function<B, Boolean> predicate) {
        if (isDefined()) {
            if (!predicate.apply(get())) {
                return Option.apply(e);
            } else {
                return Option.none();
            }
        }
        return Option.none();
    }

    public <R> Either<A, R> map(Function<B, R> function) {
        if (isDefined()) {
            return new Either<A, R>(null, function.apply(get()));
        } else {
            return new Either<A, R>(e.left.get(), null);
        }
    }

    public <R> Either<A, R> flatMap(Function<B, Either<A, R>> action) {
        if (isDefined()) {
            return action.apply(get());
        } else {
            return new Either<A, R>(e.left.get(), null);
        }
    }
}