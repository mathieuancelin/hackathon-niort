package utils.functional;

import com.google.common.base.Objects;

public class T3<A, B, C> {

    public final A _1;
    public final B _2;
    public final C _3;

    public T3(A _1, B _2, C _3) {
        this._1 = _1;
        this._2 = _2;
        this._3 = _3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        T3 that = (T3) o;

        return Objects.equal(this._1, that._1) &&
                Objects.equal(this._2, that._2) &&
                Objects.equal(this._3, that._3);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(_1, _2, _3);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("_1", _1)
                .add("_2", _2)
                .add("_3", _3)
                .toString();
    }
}