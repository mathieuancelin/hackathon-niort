package utils.functional;

import com.google.common.base.Objects;

public class T1<A> {

    public final A _1;

    public T1(A _1) {
        this._1 = _1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        T1 that = (T1) o;

        return Objects.equal(this._1, that._1);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(_1);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("_1", _1)
                .toString();
    }
}