package utils.functional;

public interface Action<T> {
    public void apply(T t);
}