package utils.functional;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Throwables;

import java.io.Serializable;
import java.util.concurrent.Callable;

public abstract class Option<T> implements Iterable<T>, Serializable {

    public abstract Optional<T> toOptional();

    public abstract boolean isDefined();

    public abstract boolean isEmpty();

    public abstract T get();

    public Option<T> orElse(T value) {
        return isEmpty() ? Option.apply(value) : this;
    }

    public T getOrElse(T value) {
        return isEmpty() ? value : get();
    }

    public T getOrElse(Function<Unit, T> function) {
        return isEmpty() ? function.apply(Unit.unit()) : get();
    }

    public T getOrThrow(Throwable t) {
        if (isEmpty()) {
            throw Throwables.propagate(t);
        } else {
            return get();
        }
    }

    public void foreach(Function<T, ?> f) {
        if (!isEmpty()) f.apply(get());
    }

    public Boolean forall(Function<T, Boolean> f) {
        return isEmpty() || f.apply(get());
    }

    public Boolean exists(Function<T, Boolean> f) {
        return !isEmpty() && f.apply(get());
    }

    public T getOrElse(Callable<T> function) {
        T ret = null;
        try {
            ret = function.call();
        } catch(Exception e) {}
        return isEmpty() ? ret : get();
    }

    public T getOrNull() {
        return isEmpty() ? null : get();
    }

    public Option<T> filter(Function<T, Boolean> predicate) {
        if (isDefined()) {
            if (predicate.apply(get())) {
                return this;
            } else {
                return Option.none();
            }
        }
        return Option.none();
    }

    public Option<T> filterNot(Function<T, Boolean> predicate) {
        if (isDefined()) {
            if (!predicate.apply(get())) {
                return this;
            } else {
                return Option.none();
            }
        }
        return Option.none();
    }

    /**
     * Pattern matching like (with extraction) on clazz
     */
    public <O> Option<T> notMatch(Class<O> clazz) {
        if (isDefined()) {
            T obj = get();
            if (clazz.isAssignableFrom(obj.getClass())) {
                return Option.none();
            } else {
                return Option.some(get());
            }
        } else {
            return Option.none();
        }
    }

    /**
     * Pattern matching like (with extraction) on clazz
     */
    public <O> Option<O> match(Class<O> clazz) {
        if (isDefined()) {
            T obj = get();
            if (clazz.isAssignableFrom(obj.getClass())) {
                return Option.some(clazz.cast(obj));
            } else {
                return Option.none();
            }
        } else {
            return Option.none();
        }
    }

    /**
     * Pattern matching like (with extraction) on clazz and predicate
     */
    public <O> Option<O> match(Class<O> clazz, Function<O, Boolean> predicate) {
        if (isDefined()) {
            T obj = get();
            if (clazz.isAssignableFrom(obj.getClass())) {
                O newObj = clazz.cast(obj);
                if (predicate.apply(newObj)) {
                    return Option.some(newObj);
                } else {
                    return Option.none();
                }
            } else {
                return Option.none();
            }
        } else {
            return Option.none();
        }
    }

    public <X> Either<X, T> toRight(X left) {
        if (isDefined()) {
            return Either.eitherRight(get());
        } else {
            return Either.eitherLeft(left);
        }
    }

    public <X> Either<T, X> toLeft(X right) {
        if (isDefined()) {
            return Either.eitherLeft(get());
        } else {
            return Either.eitherRight(right);
        }
    }

    public <R> Option<R> map(Function<T, R> function) {
        if (isDefined()) {
            return Option.apply(function.apply(get()));
        }
        return Option.none();
    }

    public Option<T> map(Callable<T> function) {
        if (isDefined()) {
            T ret = null;
            try {
                ret = function.call();
            } catch(Throwable e) {}
            return Option.apply(ret);
        }
        return Option.none();
    }

    public <R> Option<R> flatMap(Callable<Option<R>> action) {
        if (isDefined()) {
            try {
                return action.call();
            } catch(Throwable e) {
                return Option.none();
            }
        }
        return Option.none();
    }

    public <R> Option<R> flatMap(Function<T, Option<R>> action) {
        if (isDefined()) {
            return action.apply(get());
        }
        return Option.none();
    }

    /**
     * Returns the globally defined None object
     *
     * @param <T>
     * @return
     */
    public static <T> None<T> none() {
        return (None<T>) None.NONE_INSTANCE;
    }

    /**
     * Create a new Some object containing the object 'value'
     *
     * @param value
     * @param <T>
     * @return
     */
    public static <T> Some<T> some(T value) {
        return new Some<T>(value);
    }

    /**
     * Create an object None if value==null else a Some object
     * @param value
     * @param <T>
     * @return
     */
    public static <T> Option<T> apply(T value) {
        if (value == null) {
            return Option.none();
        } else {
            return Option.some(value);
        }
    }

    /**
     * Some if match clazz and predicate
     */
    public static <A> Option<A> matching(final Object value, Class<A> clazz, Predicate<A> predicate) {
        if (value == null) return none();
        Preconditions.checkNotNull(clazz);
        Preconditions.checkNotNull(predicate);
        if (clazz.isAssignableFrom(value.getClass())) {
            A actual = clazz.cast(value);
            if (predicate.apply(actual)) {
                return some(actual);
            } else {
                return none();
            }
        }
        return none();
    }

    /**
     * Some if match clazz
     */
    public static <T> Option<T> of( final Object value, final Class<T> clazz) {
        if (value == null) return none();
        Preconditions.checkNotNull(clazz);
        if (clazz.isAssignableFrom(value.getClass())) {
            return some(clazz.cast(value));
        }
        return none();
    }

    /**
     * Some if match predicate
     */
    public static <T> Option<T> matching(T value, Predicate<T> predicate) {
        if (value == null) return none();
        Preconditions.checkNotNull(predicate);
        if (predicate.apply(value)) {
            return some(value);
        }
        return none();
    }

    public <O> O fold(Function<Unit, O> errorHandler, Function<T, O> successHandler) {
        if (isEmpty()) {
            return errorHandler.apply(Unit.unit());
        } else {
            return successHandler.apply(get());
        }
    }

    public T fold(Function<Unit, T> errorHandler) {
        if (isEmpty()) {
            return errorHandler.apply(Unit.unit());
        } else {
            return get();
        }
    }
}