package utils.functional;

import com.google.common.base.Optional;

import java.util.Collections;
import java.util.Iterator;

public class None<T> extends Option<T> {

    final static None<Object> NONE_INSTANCE = new None<Object>();

    /**
     * Always returns false
     * @return
     */
    @Override
    public boolean isDefined() {
        return false;
    }

    /**
     * Always returns an exception
     * @return
     */
    @Override
    public T get() {
        throw new IllegalStateException("No value");
    }

    /**
     * Returns an iterator on an empty list
     * @return
     */
    @Override
    public Iterator<T> iterator() {
        return Collections.<T>emptyList().iterator();
    }

    @Override
    public String toString() {
        return "None";
    }

    /**
     * Always returns true
     * @return
     */
    @Override
    public boolean isEmpty() {
        return true;
    }

    @Override
    public Optional<T> toOptional() {
        return Optional.absent();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof None)) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return 424242;
    }
}