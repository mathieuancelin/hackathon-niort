package utils.functional;

import com.google.common.base.Function;

public class ComposableFunction<I, O> implements Function<I, O> {
    private final Function<I, O> function;

    private ComposableFunction(Function<I, O> function) {
        this.function = function;
    }

    public <OO> ComposableFunction<I, OO> andThen(final Function<O, OO> f) {
        return new ComposableFunction<>(i -> f.apply(function.apply(i)));
    }

    @Override
    public O apply(I i) {
        return function.apply(i);
    }

    public Function<I, O> asFunction() {
        return this;
    }

    public static <I, O> ComposableFunction<I, O> chain(Function<I, O> function) {
        return new ComposableFunction<>(function);
    }
}