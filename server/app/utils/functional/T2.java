package utils.functional;

import com.google.common.base.Objects;

public class T2<A, B> {

    public final A _1;
    public final B _2;

    public T2(A _1, B _2) {
        this._1 = _1;
        this._2 = _2;
    }

    public static <A, B> T2<A, B> of(A _1, B _2) {
        return new T2<A, B>(_1, _2);
    }

    public static <A, B> T2<A, B> from(T2<A, B> t) {
        return of(t._1, t._2);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        T2 that = (T2) o;

        return Objects.equal(this._1, that._1) &&
                Objects.equal(this._2, that._2);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(_1, _2);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("_1", _1)
                .add("_2", _2)
                .toString();
    }
}