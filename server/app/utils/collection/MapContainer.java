package utils.collection;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import utils.functional.Option;
import utils.functional.Tuple;
import utils.functional.Unit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MapContainer<A, B> implements Iterable<Tuple<A, B>> {

    private final Map<A, B> underlying;

    private MapContainer(Map<A, B> underlying) {
        if (underlying == null) throw new RuntimeException("Underlying list can't be null");
        this.underlying = underlying;
    }

    /**
     * The following methods are immutable and return copies of the underlying collection.
     * Don't forget to reassign the container id needed.
     * For perfs improvements, prefer mutable operation through the use of `.mutable()`
     */
    public Option<B> get(A key) {
        B b = underlying.get(key);
        return Option.apply(b);
    }

    public MapContainer<A, B> put(A key, B value) {
        Map<A, B> map = new HashMap<A, B>();
        map.putAll(underlying);
        map.put(key, value);
        return new MapContainer<A, B>(map);
    }

    public MapContainer<A, B> putAll(MapContainer<A, B> other) {
        Map<A, B> map = new HashMap<A, B>();
        map.putAll(underlying);
        map.putAll(other.underlying);
        return new MapContainer<A, B>(map);
    }

    public MapContainer<A, B> remove(A key) {
        Map<A, B> map = new HashMap<A, B>();
        map.putAll(underlying);
        map.remove(key);
        return new MapContainer<A, B>(map);
    }

    public MapContainer<A, B> clear() {
        return MapContainer.empty();
    }
    /********************************************************************************/

    // Efficient implementation for mutations
    public static class MutableMapContainer<A, B> implements Iterable<Tuple<A, B>> {

        private final Map<A, B> underlying;

        private MutableMapContainer(Map<A, B> underlying) {
            this.underlying = underlying;
        }

        public boolean contains(A key) {
            return underlying.containsKey(key);
        }

        @Override
        public Iterator<Tuple<A, B>> iterator() {
            return toList().iterator();  // OK
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Container)) return false;
            MutableMapContainer container = (MutableMapContainer) o;
            if (!underlying.equals(container.underlying)) return false;
            return true;
        }

        @Override
        public int hashCode() {
            return underlying.hashCode();
        }

        public MutableMapContainer<A, B> clear() {
            underlying.clear();
            return this;
        }

        public Option<B> get(A key) {
            if (key == null) return Option.none();
            B b = underlying.get(key);
            return Option.apply(b);
        }

        public MutableMapContainer<A, B> put(A key, B value) {
            if (key == null) return this;
            if (value == null) return this;
            underlying.put(key, value);
            return this;
        }

        public MutableMapContainer<A, B> putAll(MapContainer<A, B> other) {
            if (other == null) return this;
            underlying.putAll(other.underlying);
            return this;
        }

        public MutableMapContainer<A, B> remove(A key) {
            if (key == null) return this;
            underlying.remove(key);
            return this;
        }

        public List<Tuple<A, B>> toList() {
            List<Tuple<A, B>> list = new ArrayList<Tuple<A, B>>();
            for (Map.Entry<A, B> entry : underlying.entrySet()) {
                list.add(new Tuple<A, B>(entry.getKey(), entry.getValue()));
            }
            return list;
        }

        public Map<A, B> toMap() {
            return underlying;
        }

        /**
         * Back to immutable world with the same underlying structure.
         */
        public MapContainer<A, B> immutable() {
            return new MapContainer<A, B>(underlying);
        }

        /**
         * Back to immutable world with a copy of the underlying structure
         */
        public MapContainer<A, B> asImmutable() {
            return MapContainer.copyOf(underlying);
        }

        public int size() {
            return underlying.size();
        }

        public boolean isEmpty() {
            return underlying.isEmpty();
        }

        public boolean nonEmpty() {
            return !underlying.isEmpty();
        }
    }

    /**
     * mutable just shift paradigm when changing data in the structure. The underlying structure stay the same
     */
    public MutableMapContainer<A, B> mutable() {
        return new MutableMapContainer<A, B>(underlying);
    }

    /**
     * mutable shift paradigm when changing data in the structure and copy the underlying structure in a new one
     */
    public MutableMapContainer<A, B> asMutable() {
        Map<A, B> map = new HashMap<A, B>();
        map.putAll(underlying);
        return new MutableMapContainer<A, B>(map);
    }

    public boolean contains(A key) {
        return underlying.containsKey(key);
    }

    @Override
    public Iterator<Tuple<A, B>> iterator() {
        return toList().iterator(); // OK
    }

    @Override
    public boolean equals(Object o) {
        try {
            if (this == o) return true;
            if (!(o instanceof Container)) return false;

            MapContainer container = (MapContainer) o;

            if (!underlying.equals(container.underlying)) return false;

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return underlying.hashCode();
    }

    public static <A, B> MutableMapContainer<A, B> mempty() {
        return (MutableMapContainer<A, B>) empty().mutable();
    }

    public static <A, B> MapContainer<A, B> empty() {
        return new MapContainer<A, B>(Collections.<A, B>emptyMap());
    }

    public static <A, B> MutableMapContainer<A, B> mof(Tuple<A, B> element) {
        return of(element).mutable();
    }

    public static <A, B> MapContainer<A, B> of(Tuple<A, B> element) {
        if (element == null) return empty();
        return new MapContainer<A, B>(ImmutableMap.of(element._1, element._2));
    }

    public static <A, B> MutableMapContainer<A, B> mof(A key, B value) {
        return of(key, value).mutable();
    }

    public static <A, B> MapContainer<A, B> of(A key, B value) {
        if (key == null) return empty();
        if (value == null) return empty();
        return new MapContainer<A, B>(ImmutableMap.of(key, value));
    }

    public static <A, B> MutableMapContainer<A, B> mof(Tuple<A, B>... elements) {
        return of(elements).mutable();
    }

    public static <A, B> MapContainer<A, B> of(Tuple<A, B>... elements) {
        if (elements == null) return empty();
        Map<A, B> map = new HashMap<A, B>();
        for (Tuple<A, B> t : elements) {
            map.put(t._1, t._2);
        }
        return new MapContainer<A, B>(map);
    }

    public static <A, B> MutableMapContainer<A, B> mfrom(Iterable<Tuple<A, B>> elements) {
        return from(elements).mutable();
    }

    public static <A, B> MapContainer<A, B> from(Iterable<Tuple<A, B>> elements) {
        if (elements == null) return empty();
        Map<A, B> map = new HashMap<A, B>();
        for (Tuple<A, B> t : elements) {
            map.put(t._1, t._2);
        }
        return new MapContainer<A, B>(map);
    }

    public static <A, B> MutableMapContainer<A, B> mfrom(Tuple<A, B>... elements) {
        return from(elements).mutable();
    }

    public static <A, B> MapContainer<A, B> from(Tuple<A, B>... elements) {
        return of(elements);
    }

    public static <A, B> MutableMapContainer<A, B> mfrom(Map<A, B> underlying) {
        return from(underlying).mutable();
    }

    public static <A, B> MapContainer<A, B> from(Map<A, B> underlying) {
        return copyOf(underlying);
    }

    public static <A, B> MutableMapContainer<A, B> mcopyOf(Map<A, B> underlying) {
        return copyOf(underlying).mutable();
    }

    public static <A, B> MapContainer<A, B> copyOf(Map<A, B> underlying) {
        if (underlying == null) return empty();
        return new MapContainer<A, B>(ImmutableMap.copyOf(underlying));
    }

    public MapContainer<A, B> init() {
        return MapContainer.from(toContainer().init());
    }

    public Option<Tuple<A, B>> lastOption() {
        return toContainer().lastOption();
    }

    public MapContainer<A, B> tail() {
        return MapContainer.from(toContainer().tail());
    }

    public Tuple<A, B> last() {
        return lastOption().get();
    }

    public Option<Tuple<A, B>> headOption() {
        return toContainer().headOption();
    }

    public Tuple<A, B> head() {
        return headOption().get();
    }

    public MapContainer<A, B> drop(int n) {
        return MapContainer.from(toContainer().drop(n));
    }

    public MapContainer<A, B> limit(int n) {
        return MapContainer.from(toContainer().limit(n));
    }

    public MapContainer<A, B> dropRight(int n) {
        return MapContainer.from(toContainer().dropRight(n));
    }

    public MapContainer<A, B> skip(int n) {
        return MapContainer.from(toContainer().skip(n));
    }

    public Boolean isEmpty() {
        return underlying.isEmpty();
    }

    public Boolean nonEmpty() {
        return !underlying.isEmpty();
    }

    public int size() {
        return underlying.size();
    }

    public Container<A> keys() {
        return Container.from(underlying.keySet());
    }

    public Container<B> values() {
        return Container.from(underlying.values());
    }

    public Boolean exists(Predicate<Tuple<A, B>> p) {
        if (p == null) return false;
        for (Map.Entry<A, B> entry : underlying.entrySet()) {
            Tuple<A, B> tuple = Tuple.tuple(entry.getKey(), entry.getValue());
            if (p.apply(tuple)) {
                return true;
            }
        }
        return false;
    }

    public int count(Predicate<Tuple<A, B>> p) {
        if (p == null) return 0;
        int count = 0;
        for (Map.Entry<A, B> entry : underlying.entrySet()) {
            Tuple<A, B> tuple = Tuple.tuple(entry.getKey(), entry.getValue());
            if (p.apply(tuple)) {
                count++;
            }
        }
        return count;
    }

    public Option<Tuple<A, B>> find(Predicate<Tuple<A, B>> p) {
        if (p == null) return Option.none();
        for (Map.Entry<A, B> entry : underlying.entrySet()) {
            Tuple<A, B> tuple = Tuple.tuple(entry.getKey(), entry.getValue());
            if (p.apply(tuple)) {
                Option.some(tuple);
            }
        }
        return Option.none();
    }

    public MapContainer<A, B> dropWhile(Predicate<Tuple<A, B>> p) {
        if (p == null) return MapContainer.empty();
        Map<A, B> map = new HashMap<A, B>();
        for (Map.Entry<A, B> elem : underlying.entrySet()) {
            Tuple<A, B> t = new Tuple<A, B>(elem.getKey(), elem.getValue());
            if (!p.apply(t)) {
                map.put(t._1, t._2);
            }
        }
        return new MapContainer<A, B>(map);
    }

    public <C, D> MapContainer<C, D> map(Function<Tuple<A, B>, Tuple<C, D>> function) {
        if (function == null) return MapContainer.empty();
        Map<C, D> map = new HashMap<C, D>();
        for (Map.Entry<A, B> entry : underlying.entrySet()) {
            Tuple<A, B> tuple = Tuple.tuple(entry.getKey(), entry.getValue());
            Tuple<C, D> t2 = function.apply(tuple);
            map.put(t2._1, t2._2);
        }
        return new MapContainer<C, D>(map);
    }

    public <C> MapContainer<C, B> mapKeys(Function<A, C> function) {
        if (function == null) return MapContainer.empty();
        Map<C, B> map = new HashMap<C, B>();
        for (Map.Entry<A, B> entry : underlying.entrySet()) {
            Tuple<A, B> t = Tuple.tuple(entry.getKey(), entry.getValue());
            map.put(function.apply(t._1), t._2);
        }
        return new MapContainer<C, B>(map);
    }

    public <C> MapContainer<A, C> mapValues(Function<B, C> function) {
        if (function == null) return MapContainer.empty();
        Map<A, C> map = new HashMap<A, C>();
        for (Map.Entry<A, B> entry : underlying.entrySet()) {
            Tuple<A, B> t = Tuple.tuple(entry.getKey(), entry.getValue());
            map.put(t._1, function.apply(t._2));
        }
        return new MapContainer<A, C>(map);
    }

    public MapContainer<A, B> filterByKey(final Predicate<A> function) {
        return filter(new Predicate<Tuple<A, B>>() {
            @Override
            public boolean apply(Tuple<A, B> input) {
                return function.apply(input._1);
            }
        });
    }

    public MapContainer<A, B> filterNotByKey(Predicate<A> function) {
        return filterByKey(Predicates.not(function));
    }

    public MapContainer<A, B> filterByValue(final Predicate<B> function) {
        return filter(new Predicate<Tuple<A, B>>() {
            @Override
            public boolean apply(Tuple<A, B> input) {
                return function.apply(input._2);
            }
        });
    }

    public MapContainer<A, B> filterNotByValue(Predicate<B> function) {
        return filterByValue(Predicates.not(function));
    }

    public MapContainer<A, B> filter(Predicate<Tuple<A, B>> function) {
        if (function == null) return this;
        Map<A, B> map = new HashMap<A, B>();
        for (Map.Entry<A, B> entry : underlying.entrySet()) {
            Tuple<A, B> t = Tuple.tuple(entry.getKey(), entry.getValue());
            if (function.apply(t)) {
                map.put(t._1, t._2);
            }
        }
        return new MapContainer<A, B>(map);
    }

    public MapContainer<A, B> filterNot(Predicate<Tuple<A, B>> function) {
        if (function == null) return this;
        return filter(Predicates.not(function));
    }

    public MapContainer<A, Container<B>> flatMap(Function<A, Iterable<B>> function) {
        if (function == null) return MapContainer.empty();
        Map<A, Container<B>> map = new HashMap<A, Container<B>>();
        for (Map.Entry<A, B> entry : underlying.entrySet()) {
            Tuple<A, B> t = Tuple.tuple(entry.getKey(), entry.getValue());
            Container<B> container = Container.from(function.apply(t._1));
            map.put(t._1, container);
        }
        return new MapContainer<A, Container<B>>(map);
    }

    public void foreach(Function<Tuple<A, B>, Unit> f) {
        if (f == null) return;
        for (Map.Entry<A, B> entry : underlying.entrySet()) {
            Tuple<A, B> tuple = Tuple.tuple(entry.getKey(), entry.getValue());
            f.apply(tuple);
        }
    }

    public <K> MapContainer<K, MapContainer<A, B>> groupBy(Function<Tuple<A, B>, K> f) {
        if (f == null) return MapContainer.empty();
        Map<K, MapContainer<A, B>> map = new HashMap<K, MapContainer<A, B>>();
        for (Map.Entry<A, B> entry : underlying.entrySet()) {
            Tuple<A, B> e = Tuple.tuple(entry.getKey(), entry.getValue());
            K key = f.apply(e);
            if (!map.containsKey(key)) {
                map.put(key, MapContainer.<A, B>empty());
            }
            map.put(key, map.get(key).put(e._1, e._2));
        }
        return new MapContainer<K, MapContainer<A, B>>(map);
    }

    public String join() {
        return join(", ");
    }

    public String join(String sep) {
        if (sep == null) sep = ", ";
        return Joiner.on(sep).join(toContainer().map(new Function<Tuple<A,B>, String>() {   // OK
            @Override
            public String apply(Tuple<A, B> input) {
                return "[" + input._1.toString() + ", " + input._2.toString() + "]";
            }
        }));
    }

    public String toString() {
        return "MapContainer[" + join(", ") + "]";
    }

    public Container<MapContainer<A, B>> partition(Predicate<Tuple<A, B>> p) {
        if (p == null) return Container.empty();
        return Container.from(
                filter(p),
                filterNot(p)
        );
    }

    public Option<Tuple<A, B>> reduceLeftOption(Function<Tuple<Tuple<A, B>, Tuple<A, B>>, Tuple<A, B>> f) {
        return Option.apply(reduceLeft(f));
    }

    public Tuple<A, B> reduceLeft(Function<Tuple<Tuple<A, B>, Tuple<A, B>>, Tuple<A, B>> f) {
        return toContainer().reduceLeft(f);  // OK
    }

    public Option<Tuple<A, B>> reduceRightOption(Function<Tuple<Tuple<A, B>, Tuple<A, B>>, Tuple<A, B>> f) {
        return Option.apply(reduceRight(f));
    }

    public Tuple<A, B> reduceRight(Function<Tuple<Tuple<A, B>, Tuple<A, B>>, Tuple<A, B>> f) {
        return toContainer().reduceRight(f); // OK
    }

    public <U> Option<U> foldLeftOption(U start, Function<Tuple<U, Tuple<A, B>>, U> f) {
        return toContainer().foldLeftOption(start, f); // OK
    }

    public <U> U foldLeft(U start, Function<Tuple<U, Tuple<A, B>>, U> f) {
        return toContainer().foldLeft(start, f);  // OK
    }

    public <U> Option<U> foldRightOption(U start, Function<Tuple<U, Tuple<A, B>>, U> f) {
        return Option.apply(foldRight(start, f));
    }

    public <U> U foldRight(U start, Function<Tuple<U, Tuple<A, B>>, U> f) {
        return toContainer().foldRight(start, f);  // OK
    }

    public MapContainer<A, B> takeWhile(Predicate<Tuple<A, B>> p) {
        if (p == null) return MapContainer.empty();
        Map<A, B> map = new HashMap<A, B>();
        for (Map.Entry<A, B> entry : map.entrySet()) {
            Tuple<A, B> t = Tuple.tuple(entry.getKey(), entry.getValue());
            if (p.apply(t)) {
                map.put(t._1, t._2);
            } else {
                return new MapContainer<A, B>(map);
            }
        }
        return new MapContainer<A, B>(map);
    }

    public MapContainer<Integer, A> zipWithIndex() {
        return MapContainer.from(keys().zipWithIndex());
    }

    public <C, D> MapContainer<C, D> collect(Function<Tuple<A, B>, Option<Tuple<C, D>>> function) {
        if (function == null) return MapContainer.empty();
        Map<C, D> map = new HashMap<C, D>();
        for (Map.Entry<A, B> entry : underlying.entrySet()) {
            Tuple<A, B> t = Tuple.tuple(entry.getKey(), entry.getValue());
            Option<Tuple<C, D>> option = function.apply(t);
            for(Tuple<C, D> t2 : option) {
                map.put(t2._1, t2._2);
            }
        }
        return new MapContainer<C, D>(map);
    }

    public Container<Tuple<A, B>> toContainer() {
        return Container.from(toList());  // OK
    }

    public List<Tuple<A, B>> toList() {
        List<Tuple<A, B>> list = new ArrayList<Tuple<A, B>>();
        for (Map.Entry<A, B> entry : underlying.entrySet()) {
            list.add(new Tuple<A, B>(entry.getKey(), entry.getValue()));
        }
        return list;
    }

    public Map<A, B> toMap() {
        return underlying;
    }
}
