package utils.collection;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Queues;
import com.google.common.collect.Sets;

import java.util.Deque;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public class Builders {

    public static <T> Function<Iterable<T>, ImmutableList<T>> asImmutableList() {
        return ImmutableList::copyOf;
    }

    public static <T> Function<Iterable<T>, ImmutableSet<T>> asImmutableSet() {
        return ImmutableSet::copyOf;
    }

    public static <T> Function<Iterable<T>, Queue<T>> asQueue() {
        return Queues::newLinkedBlockingQueue;
    }

    public static <T> Function<Iterable<T>, Queue<T>> asBlockingQueue() {
        return Queues::newArrayDeque;
    }

    public static <T> Function<Iterable<T>, Queue<T>> asConcurrentQueue() {
        return Queues::newConcurrentLinkedQueue;
    }

    public static <T> Function<Iterable<T>, Deque<T>> asDeque() {
        return Queues::newArrayDeque;
    }

    public static <T> Function<Iterable<T>, List<T>> asList() {
        return Lists::newArrayList;
    }

    public static <T> Function<Iterable<T>, List<T>> asLinkedList() {
        return Lists::newLinkedList;
    }

    public static <T> Function<Iterable<T>, List<T>> asCopyOnWriteList() {
        return Lists::newCopyOnWriteArrayList;
    }

    public static <T> Function<Iterable<T>, Set<T>> asSet() {
        return Sets::newHashSet;
    }

    public static <T> Function<Iterable<T>, Set<T>> asCopyOnWriteArraySet() {
        return Sets::newCopyOnWriteArraySet;
    }

    public static <T> Function<Iterable<T>, Set<T>> asLinkedHashSet() {
        return Sets::newLinkedHashSet;
    }

    public static <T> Function<Iterable<T>, Set<T>> asConcurrentSet() {
        return Sets::newConcurrentHashSet;
    }
}
