package utils.collection;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import utils.functional.Option;
import utils.functional.Tuple;
import utils.functional.Unit;
import utils.json.JsArray;
import utils.json.Json;
import utils.json.mapping.DefaultWriters;
import utils.json.mapping.Format;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class Container<T> implements Iterable<T> {

    private final List<T> underlying;

    private Container(Iterable<T> underlying) {
        if (underlying == null) throw new RuntimeException("Underlying list can't be null");
        if (List.class.isAssignableFrom(underlying.getClass())) {
            this.underlying = (List<T>) underlying;
        } else {
            this.underlying = Lists.newArrayList(underlying);
        }
    }


    public JsArray asJsArray(Format<T> format) {
        return Json.toJson(this.toList(), DefaultWriters.seq(format)).as(JsArray.class);
    }

    public JsonNode asJacksonArray(Format<T> format) {
        return asJsArray(format).asJackson();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Container)) return false;

        Container container = (Container) o;

        if (!underlying.equals(container.underlying)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return underlying.hashCode();
    }

    /**
     * The following methods are immutable and return copies of the underlying collection.
     * Don't forget to reassign the container id needed.
     * For perfs improvements, prefer mutable operation through the use of `.mutable()`
     */
    public <U extends T> Container<T> add(U elem) {
        if (elem == null) return this;
        return new Container<T>(Iterables.concat(underlying, Lists.<T>newArrayList(elem)));
    }

    public Container<T> addAll(Iterable<? extends T> elem) {
        if (elem == null) return this;
        return new Container<T>(Iterables.concat(underlying, elem));
    }

    public Container<T> remove(T object) {
        if (object == null) return this;
        List<T> l = new ArrayList<T>();
        l.addAll(underlying);
        l.remove(object);
        return new Container<T>(l);
    }

    public Container<T> clear() {
        return Container.empty();
    }
    /**************************************************************************************/

    /**
     * mutable just shift paradigm when changing data in the structure. The underlying structure stay the same
     */
    public MutableContainer<T> mutable() {
        return new MutableContainer<T>(underlying);
    }

    /**
     * mutable shift paradigm when changing data in the structure and copy the underlying structure in a new one
     */
    public MutableContainer<T> asMutable() {
        List<T> list = new ArrayList<T>();
        list.addAll(underlying);
        return new MutableContainer<T>(list);
    }

    // Efficient implementation for mutations
    public static class MutableContainer<T> implements Iterable<T> {
        private final List<T> underlying;

        public MutableContainer(List<T> underlying) {
            this.underlying = underlying;
        }

        public boolean contains(T key) {
            return underlying.contains(key);
        }

        @Override
        public Iterator<T> iterator() {
            return underlying.iterator();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Container)) return false;
            MutableContainer container = (MutableContainer) o;
            if (!underlying.equals(container.underlying)) return false;
            return true;
        }

        @Override
        public int hashCode() {
            return underlying.hashCode();
        }

        public MutableContainer<T> clear() {
            underlying.clear();
            return this;
        }

        public Option<T> get(int index) {
            try {
                return Option.apply(underlying.get(index));
            } catch(IndexOutOfBoundsException e) {
                return Option.none();
            }
        }

        public MutableContainer<T> add(T item) {
            if (item == null) return this;
            underlying.add(item);
            return this;
        }

        public MutableContainer<T> addAll(Container<T> other) {
            if (other == null) return this;
            underlying.addAll(other.underlying);
            return this;
        }

        public MutableContainer<T> remove(T item) {
            if (item == null) return this;
            underlying.remove(item);
            return this;
        }

        /**
         * Back to immutable world with the same underlying structure
         */
        public Container<T> immutable() {
            return new Container<T>(underlying);
        }

        /**
         * Back to immutable world with a copy of the underlying structure
         */
        public Container<T> asImmutable() {
            return Container.copyOf(underlying);
        }

        public int size() {
            return underlying.size();
        }

        public boolean isEmpty() {
            return underlying.isEmpty();
        }

        public boolean nonEmpty() {
            return !underlying.isEmpty();
        }
    }

    public static <T> MutableContainer<T> mempty() {
        return (MutableContainer<T>) empty().mutable();
    }

    public static <T> Container<T> empty() {
        return new Container<T>(Collections.<T>emptyList());
    }

    public static <T> MutableContainer<T> mof(T element) {
        return of(element).mutable();
    }

    public static <T> Container<T> of(T element) {
        if (element == null) return empty();
        return new Container<T>(ImmutableList.of(element));
    }

    public static <T> MutableContainer<T> mof(T... elements) {
        return of(elements).mutable();
    }

    public static <T> Container<T> of(T... elements) {
        if (elements == null) return empty();
        return new Container<T>(Lists.newArrayList(elements));
    }

    public static <T> MutableContainer<T> mfrom(T... elements) {
        return from(elements).mutable();
    }

    public static <T> Container<T> from(T... elements) {
        if (elements == null) return empty();
        return new Container<T>(Lists.newArrayList(elements));
    }

    public static <T> MutableContainer<T> mfrom(Iterable<T> underlying) {
        return from(underlying).mutable();
    }

    public static <T> Container<T> from(Iterable<T> underlying) {
        return copyOf(underlying);
    }

    public static <T> MutableContainer<T> mcopyOf(Iterable<T> underlying) {
        return copyOf(underlying).mutable();
    }

    public static <T> Container<T> copyOf(Iterable<T> underlying) {
        if (underlying == null) return empty();
        return new Container<T>(ImmutableList.copyOf(underlying));
    }

    public Option<T> get(int i) {
        try {
            return Option.apply(underlying.get(i));
        } catch(IndexOutOfBoundsException e) {
            return Option.none();
        }
    }

    public Container<T> distinct() {
        return new Container<T>(Sets.newHashSet(underlying));
    }

    @Override
    public Iterator<T> iterator() {
        return underlying.iterator();
    }

    public Container<T> cycle() {
        return new Container<T>(Iterables.cycle(underlying));
    }

    public Boolean isEmpty() {
        return !underlying.iterator().hasNext();
    }

    public Boolean nonEmpty() {
        return underlying.iterator().hasNext();
    }

    public int size() {
        return toList().size();
    }

    public Container<T> concat(Iterable<? extends T> elem) {
        if (elem == null) return this;
        return new Container<T>(Iterables.concat(underlying, elem));
    }

    public Container<T> init() {
        return new Container<T>(Iterables.limit(underlying, size() - 1));
    }

    public Option<T> lastOption() {
        return Option.apply(Iterables.getLast(underlying, null));
    }

    public Container<T> tail() {
        return new Container<T>(Iterables.skip(underlying, 1));
    }

    public T last() {
        return lastOption().get();
    }

    public Option<T> headOption() {
        return Option.apply(Iterables.getFirst(underlying, null));
    }

    public T head() {
        return headOption().get();
    }

    public <U extends T> Boolean contains(U elem) {
        if (elem == null) return false;
        return underlying.contains(elem);
    }

    public Boolean exists(Predicate<T> p) {
        if (p == null) return false;
        return Iterables.tryFind(underlying, p).isPresent();
    }

    public int count(Predicate<T> p) {
        if (p == null) return 0;
        return filter(p).size();
    }

    public Option<T> find(Predicate<T> p) {
        if (p == null) return Option.none();
        return Option.apply(Iterables.tryFind(underlying, p).orNull());
    }

    public Container<T> intersection(Iterable<T> other) {
        return new Container<T>(Sets.intersection(Sets.newHashSet(underlying), Sets.newHashSet(other)));
    }

    public Container<T> diff(Iterable<T> other) {
        return new Container<T>(Sets.difference(Sets.newHashSet(underlying), Sets.newHashSet(other)));
    }

    public Container<T> union(Container<T> other) {
        return new Container<T>(Sets.union(Sets.newHashSet(underlying), Sets.newHashSet(other)));
    }

    public Container<T> drop(int n) {
        return new Container<T>(Iterables.limit(underlying, n));
    }

    public Container<T> limit(int n) {
        return new Container<T>(Iterables.limit(underlying, n));
    }

    public Container<T> dropRight(int n) {
        return new Container<T>(Iterables.skip(underlying, n));
    }

    public Container<T> skip(int n) {
        return new Container<T>(Iterables.skip(underlying, n));
    }

    public <U extends Comparable> Container<T> sortBy(final Function<T, U> sorter) {
        if (sorter == null) return this;
        return new Container<T>(Ordering.from(new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return sorter.apply(o1).compareTo(sorter.apply(o2));
            }
        }).sortedCopy(underlying));
    }

    public Container<T> sortWith(final Function<Tuple<T, T>, Boolean> sorter) {
        if (sorter == null) return this;
        return new Container<T>(Ordering.from(new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return sorter.apply(new Tuple<T, T>(o1, o2)) ? 1 : -1;
            }
        }).sortedCopy(underlying));
    }

    public Container<T> dropWhile(Predicate<T> p) {
        if (p == null) return Container.empty();
        List<T> list = new ArrayList<T>();
        for (T elem : underlying) {
            if (!p.apply(elem)) {
                list.add(elem);
            }
        }
        return new Container<T>(list);
    }

    public Container<T> flatMap(Function<T, Iterable<T>> f) {
        if (f == null) return this;
        List<T> list = new ArrayList<T>();
        for (T elem : underlying) {
            list.addAll(Lists.newArrayList(f.apply(elem)));
        }
        return new Container<T>(list);
    }

    public void foreach(Function<T, Unit> f) {
        if (f == null) return;
        for (T e : underlying) {
            f.apply(e);
        }
    }

    public <U> Map<U, Container<T>> groupBy(Function<T, U> f) {
        if (f == null) return Collections.emptyMap();
        Map<U, Container<T>> map = new HashMap<U, Container<T>>();
        for (T e : underlying) {
            U key = f.apply(e);
            if (!map.containsKey(key)) {
                map.put(key, Container.<T>empty());
            }
            map.put(key, map.get(key).add(e));
        }
        return map;
    }

    public <U> MapContainer<U, Container<T>> cgroupBy(Function<T, U> f) {
        return MapContainer.copyOf(groupBy(f));
    }

    public String join() {
        return join(", ");
    }

    public String join(String sep) {
        if (sep == null) sep = ", ";
        return Joiner.on(sep).join(underlying);
    }

    public String toString() {
        return "Container[" + join(", ") + "]";
    }

    public Container<Container<T>> partition(int size) {
        if (size < 1) return Container.empty();
        return new Container<Container<T>>(Iterables.transform(Iterables.partition(underlying, size), new Function<List<T>, Container<T>>() {
            @Override
            public Container<T> apply(List<T> input) {
                return new Container<T>(input);
            }
        }));
    }

    public Container<Container<T>> partition(Predicate<T> p) {
        if (p == null) return Container.empty();
        return Container.from(
                filter(p),
                filterNot(p)
        );
    }

    public Option<T> reduceLeftOption(Function<Tuple<T, T>, T> f) {
        return Option.apply(reduceLeft(f));
    }

    public T reduceLeft(Function<Tuple<T, T>, T> f) {
        if (f == null) return null;
        T prevResult = null;
        T result = null;
        for (T elem : underlying) {
            if (prevResult == null) {
                result = elem;
            } else {
                result = f.apply(new Tuple<T, T>(prevResult, elem));
            }
            prevResult = result;
        }
        return result;
    }

    public Option<T> reduceRightOption(Function<Tuple<T, T>, T> f) {
        return Option.apply(reduceRight(f));
    }

    public T reduceRight(Function<Tuple<T, T>, T> f) {
        return this.reverse().reduceLeft(f);
    }

    public <U> Option<U> foldLeftOption(U start, Function<Tuple<U, T>, U> f) {
        return Option.apply(foldLeft(start, f));
    }

    public <U> U foldLeft(U start, Function<Tuple<U, T>, U> f) {
        if (start == null) return null;
        if (f == null) return null;
        U prevResult = start;
        U result = null;
        for (T elem : underlying) {
            result = f.apply(new Tuple<U, T>(prevResult, elem));
            prevResult = result;
        }
        return result;
    }

    public <U> Option<U> foldRightOption(U start, Function<Tuple<U, T>, U> f) {
        return Option.apply(foldRight(start, f));
    }

    public <U> U foldRight(U start, Function<Tuple<U, T>, U> f) {
        if (start == null) return null;
        if (f == null) return null;
        U prevResult = start;
        U result = null;
        for (T elem : this.reverse()) {
            result = f.apply(new Tuple<U, T>(prevResult, elem));
            prevResult = result;
        }
        return result;
    }

    public Container<T> reverse() {
        return new Container<T>(Lists.reverse(underlying));
    }

    public Container<T> takeWhile(Predicate<T> p) {
        if (p == null) return this;
        List<T> list = new ArrayList<T>();
        for (T elem : underlying) {
            if (p.apply(elem)) {
                list.add(elem);
            } else {
                return new Container<T>(list);
            }
        }
        return new Container<T>(list);
    }

    public Container<Tuple<Integer, T>> zipWithIndex() {
        int count = 0;
        List<Tuple<Integer, T>> list = new ArrayList<Tuple<Integer, T>>();
        for (T elem : underlying) {
            list.add(new Tuple<Integer, T>(count, elem));
            count++;
        }
        return new Container<Tuple<Integer, T>>(list);
    }

    public <A, B> Container<Tuple<A, B>> zipWith(Function<T, A> extractor1, Function<T, B> extractor2) {
        List<Tuple<A, B>> list = new ArrayList<Tuple<A, B>>();
        for (T elem : underlying) {
            A a = extractor1.apply(elem);
            B b = extractor2.apply(elem);
            list.add(new Tuple<A, B>(a, b));
        }
        return new Container<Tuple<A, B>>(list);
    }


    public <V> Container<Tuple<T, V>> zip(Container<V> other) {
        List<Tuple<T, V>> list = new ArrayList<Tuple<T, V>>();
        int count = 0;
        for (T elem : underlying) {
            if (count > other.size()) {
                break;
            }
            list.add(new Tuple<T, V>(this.get(count).get(), other.get(count).get()));
            count++;
        }
        return new Container<Tuple<T, V>>(list);
    }

    public <U> Container<U> map(Function<T, U> function) {
        if (function == null) return Container.empty();
        return new Container<U>(Iterables.transform(underlying, function));
    }

    public Container<T> filter(Predicate<T> function) {
        if (function == null) return this;
        return new Container<T>(Iterables.filter(underlying, function));
    }

    public Container<T> filterNot(Predicate<T> function) {
        if (function == null) return this;
        return new Container<T>(Iterables.filter(underlying, com.google.common.base.Predicates.not(function)));
    }

    public <U> Container<U> collect(Function<T, Option<U>> function) {
        if (function == null) return Container.empty();
        return map(function).filter(new Predicate<Option<U>>() {
            @Override
            public boolean apply(Option<U> input) {
                return input.isDefined();
            }
        }).map(new Function<Option<U>, U>() {

            @Override public U apply(Option<U> input) {
                return input.get();
            }
        });
    }

    public <U> Container<U> flatten() {
        Container<U> results = Container.empty();
        for (T t : underlying) {
            if (t instanceof Iterable<?>) {
                results = results.addAll((Iterable<? extends U>) t);
            } else {
                results = results.add((U) t);
            }
        }
        return results;
    }

    public <C extends Iterable<T>> C as(Function<Iterable<T>, C> builder) {
        return builder.apply(underlying);
    }

    public List<T> toList() {
        return this.as(Builders.<T>asList());
    }

    public Queue<T> toBlockingQueue() {
        return this.as(Builders.<T>asBlockingQueue());
    }

    public Queue<T> toConcurrentQueue() {
        return this.as(Builders.<T>asConcurrentQueue());
    }

    public Set<T> toConcurrentSet() {
        return this.as(Builders.<T>asConcurrentSet());
    }

    public Set<T> toCopyOnWriteArraySet() {
        return this.as(Builders.<T>asCopyOnWriteArraySet());
    }

    public List<T> toCopyOnWriteList() {
        return this.as(Builders.<T>asCopyOnWriteList());
    }

    public Deque<T> toDeque() {
        return this.as(Builders.<T>asDeque());
    }

    public List<T> toImmutableList() {
        return this.as(Builders.<T>asImmutableList());
    }

    public Set<T> toImmutableSet() {
        return this.as(Builders.<T>asImmutableSet());
    }

    public Set<T> toLinkedHashSet() {
        return this.as(Builders.<T>asLinkedHashSet());
    }

    public List<T> toLinkedList() {
        return this.as(Builders.<T>asLinkedList());
    }

    public Queue<T> toQueue() {
        return this.as(Builders.<T>asQueue());
    }

    public Set<T> toSet() {
        return this.as(Builders.<T>asSet());
    }
}