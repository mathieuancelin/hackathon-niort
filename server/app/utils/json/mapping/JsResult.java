package utils.json.mapping;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import utils.functional.Option;
import utils.functional.T10;
import utils.functional.T2;
import utils.functional.T3;
import utils.functional.T4;
import utils.functional.T5;
import utils.functional.T6;
import utils.functional.T7;
import utils.functional.T8;
import utils.functional.T9;
import utils.json.JsError;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class JsResult<T> implements Iterable<T> {
    public abstract T get();
    public abstract Option<T> getOpt();
    public Option<T> asOpt() {
        return getOpt();
    }
    public abstract JsResult<T> getOrElse(JsResult<T> result);
    public abstract T getValueOrNull();
    public abstract T getValueOrElse(T result);
    public abstract T getValueOrElse(Throwable result);
    public abstract <B> JsResult<B> map(Function<T, B> map);
    public abstract <B> JsResult<B> flatMap(Function<T, JsResult<B>> map);
    public abstract JsResult<T> filter(Function<T, Boolean> predicate);
    public abstract JsResult<T> filterNot(Function<T, Boolean> predicate);
    public abstract JsResult<T> filter(Function<T, Boolean> predicate, List<Throwable> errors);
    public abstract JsResult<T> filterNot(Function<T, Boolean> predicate, List<Throwable> errors);
    public abstract JsResult<T> filter(Function<T, Boolean> predicate, Throwable errors);
    public abstract JsResult<T> filterNot(Function<T, Boolean> predicate, Throwable errors);
    public abstract boolean hasErrors();
    public abstract boolean isErrors();
    public abstract boolean isSuccess();
    public abstract int countErrors();
    public abstract Option<JsError<T>> asError();
    public abstract Option<JsSuccess<T>> asSuccess();

    public static <T> JsResult<T> error(Throwable t) {
        return new JsError<T>(t);
    }

    public static <T> JsResult<T> success(T t) {
        return new JsSuccess<>(t);
    }

    public Option<Throwable> onError() {
        if (isErrors()) {
            return Option.some(asError().get().firstError());
        }
        return Option.none();
    }

    public List<Throwable> onErrors() {
        if (isErrors()) {
            return ImmutableList.copyOf(asError().get().errors);
        }
        return Collections.emptyList();
    }

    public Option<T> onSuccess() {
        if (isSuccess()) {
            return Option.some(get());
        }
        return Option.none();
    }

    public abstract T orError(Throwable t);

    public T recover(Function<JsError, T> block) {
        if (isSuccess()) {
            return get();
        } else {
            return block.apply(asError().get());
        }
    }

    private static <T> JsResult<T> populateErrs(JsResult<T> finalResult, JsResult<?>... results) {
        List<Throwable> throwables = new ArrayList<Throwable>();
        for (JsResult<?> res : results) {
            if (res.isErrors()) {
                for (Throwable t : res.asError().get().errors)
                    throwables.add(t);
            }
        }
        if (throwables.isEmpty() && finalResult.isSuccess()) {
            return new JsSuccess<T>(finalResult.asSuccess().get().get());
        } else {
            // should never happens
        }
        return new JsError<T>(throwables);
    }

    public static <A, B> JsResult<T2<A, B>> combine(final JsResult<A> res1, final JsResult<B> res2) {
        return populateErrs(res1.flatMap(new Function<A, JsResult<T2<A, B>>>() {
            public JsResult<T2<A, B>> apply(final A a) {
                return res2.map(new Function<B, T2<A, B>>() {
                    public T2<A, B> apply(B b) {
                        return new T2(a, b);
                    }
                });
            }
        }), res1, res2);
    }

    public static <A, B, C> JsResult<T3<A, B, C>> combine(final JsResult<A> res1, final JsResult<B> res2, final JsResult<C> res3) {
        return populateErrs(res1.flatMap(new Function<A, JsResult<T3<A, B, C>>>() {
            public JsResult<T3<A, B, C>> apply(final A a) {
                return res2.flatMap(new Function<B, JsResult<T3<A, B, C>>>() {
                    public JsResult<T3<A, B, C>> apply(final B b) {
                        return res3.map(new Function<C, T3<A, B, C>>() {
                            public T3<A, B, C> apply(final C c) {
                                return new T3(a, b, c);
                            }
                        });
                    }
                });
            }
        }), res1, res2, res3);
    }

    public static <A, B, C, D> JsResult<T4<A, B, C, D>> combine(final JsResult<A> res1, final JsResult<B> res2, final JsResult<C> res3, final JsResult<D> res4) {
        return populateErrs(res1.flatMap(new Function<A, JsResult<T4<A, B, C, D>>>() {
            public JsResult<T4<A, B, C, D>> apply(final A a) {
                return res2.flatMap(new Function<B, JsResult<T4<A, B, C, D>>>() {
                    public JsResult<T4<A, B, C, D>> apply(final B b) {
                        return res3.flatMap(new Function<C, JsResult<T4<A, B, C, D>>>() {
                            public JsResult<T4<A, B, C, D>> apply(final C c) {
                                return res4.map(new Function<D, T4<A, B, C, D>>() {
                                    public T4<A, B, C, D> apply(final D d) {
                                        return new T4(a, b, c, d);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }), res1, res2, res3, res4);
    }

    public static <A, B, C, D, E> JsResult<T5<A, B, C, D, E>> combine(final JsResult<A> res1, final JsResult<B> res2, final JsResult<C> res3, final JsResult<D> res4, final JsResult<E> res5) {
        return populateErrs(res1.flatMap(new Function<A, JsResult<T5<A, B, C, D, E>>>() {
            public JsResult<T5<A, B, C, D, E>> apply(final A a) {
                return res2.flatMap(new Function<B, JsResult<T5<A, B, C, D, E>>>() {
                    public JsResult<T5<A, B, C, D, E>> apply(final B b) {
                        return res3.flatMap(new Function<C, JsResult<T5<A, B, C, D, E>>>() {
                            public JsResult<T5<A, B, C, D, E>> apply(final C c) {
                                return res4.flatMap(new Function<D, JsResult<T5<A, B, C, D, E>>>() {
                                    public JsResult<T5<A, B, C, D, E>> apply(final D d) {
                                        return res5.map(new Function<E, T5<A, B, C, D, E>>() {
                                            public T5<A, B, C, D, E> apply(final E e) {
                                                return new T5(a, b, c, d, e);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }), res1, res2, res3, res4, res5);
    }

    public static <A, B, C, D, E, F> JsResult<T6<A, B, C, D, E, F>> combine(final JsResult<A> res1, final JsResult<B> res2, final JsResult<C> res3, final JsResult<D> res4, final JsResult<E> res5, final JsResult<F> res6) {
        return populateErrs(res1.flatMap(new Function<A, JsResult<T6<A, B, C, D, E, F>>>() {
            public JsResult<T6<A, B, C, D, E, F>> apply(final A a) {
                return res2.flatMap(new Function<B, JsResult<T6<A, B, C, D, E, F>>>() {
                    public JsResult<T6<A, B, C, D, E, F>> apply(final B b) {
                        return res3.flatMap(new Function<C, JsResult<T6<A, B, C, D, E, F>>>() {
                            public JsResult<T6<A, B, C, D, E, F>> apply(final C c) {
                                return res4.flatMap(new Function<D, JsResult<T6<A, B, C, D, E, F>>>() {
                                    public JsResult<T6<A, B, C, D, E, F>> apply(final D d) {
                                        return res5.flatMap(new Function<E, JsResult<T6<A, B, C, D, E, F>>>() {
                                            public JsResult<T6<A, B, C, D, E, F>> apply(final E e) {
                                                return res6.map(new Function<F, T6<A, B, C, D, E, F>>() {
                                                    public T6<A, B, C, D, E, F> apply(final F f) {
                                                        return new T6(a, b, c, d, e, f);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }), res1, res2, res3, res4, res5, res6);
    }

    public static <A, B, C, D, E, F, G> JsResult<T7<A, B, C, D, E, F, G>> combine(final JsResult<A> res1, final JsResult<B> res2, final JsResult<C> res3, final JsResult<D> res4, final JsResult<E> res5, final JsResult<F> res6, final JsResult<G> res7) {
        return populateErrs(res1.flatMap(new Function<A, JsResult<T7<A, B, C, D, E, F, G>>>() {
            public JsResult<T7<A, B, C, D, E, F, G>> apply(final A a) {
                return res2.flatMap(new Function<B, JsResult<T7<A, B, C, D, E, F, G>>>() {
                    public JsResult<T7<A, B, C, D, E, F, G>> apply(final B b) {
                        return res3.flatMap(new Function<C, JsResult<T7<A, B, C, D, E, F, G>>>() {
                            public JsResult<T7<A, B, C, D, E, F, G>> apply(final C c) {
                                return res4.flatMap(new Function<D, JsResult<T7<A, B, C, D, E, F, G>>>() {
                                    public JsResult<T7<A, B, C, D, E, F, G>> apply(final D d) {
                                        return res5.flatMap(new Function<E, JsResult<T7<A, B, C, D, E, F, G>>>() {
                                            public JsResult<T7<A, B, C, D, E, F, G>> apply(final E e) {
                                                return res6.flatMap(new Function<F, JsResult<T7<A, B, C, D, E, F, G>>>() {
                                                    public JsResult<T7<A, B, C, D, E, F, G>> apply(final F f) {
                                                        return res7.map(new Function<G, T7<A, B, C, D, E, F, G>>() {
                                                            public T7<A, B, C, D, E, F, G> apply(final G g) {
                                                                return new T7(a, b, c, d, e, f, g);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }), res1, res2, res3, res4, res5, res6, res7);
    }

    public static <A, B, C, D, E, F, G, H> JsResult<T8<A, B, C, D, E, F, G, H>> combine(final JsResult<A> res1, final JsResult<B> res2, final JsResult<C> res3, final JsResult<D> res4, final JsResult<E> res5, final JsResult<F> res6, final JsResult<G> res7, final JsResult<H> res8) {
        return populateErrs(res1.flatMap(new Function<A, JsResult<T8<A, B, C, D, E, F, G, H>>>() {
            public JsResult<T8<A, B, C, D, E, F, G, H>> apply(final A a) {
                return res2.flatMap(new Function<B, JsResult<T8<A, B, C, D, E, F, G, H>>>() {
                    public JsResult<T8<A, B, C, D, E, F, G, H>> apply(final B b) {
                        return res3.flatMap(new Function<C, JsResult<T8<A, B, C, D, E, F, G, H>>>() {
                            public JsResult<T8<A, B, C, D, E, F, G, H>> apply(final C c) {
                                return res4.flatMap(new Function<D, JsResult<T8<A, B, C, D, E, F, G, H>>>() {
                                    public JsResult<T8<A, B, C, D, E, F, G, H>> apply(final D d) {
                                        return res5.flatMap(new Function<E, JsResult<T8<A, B, C, D, E, F, G, H>>>() {
                                            public JsResult<T8<A, B, C, D, E, F, G, H>> apply(final E e) {
                                                return res6.flatMap(new Function<F, JsResult<T8<A, B, C, D, E, F, G, H>>>() {
                                                    public JsResult<T8<A, B, C, D, E, F, G, H>> apply(final F f) {
                                                        return res7.flatMap(new Function<G, JsResult<T8<A, B, C, D, E, F, G, H>>>() {
                                                            public JsResult<T8<A, B, C, D, E, F, G, H>> apply(final G g) {
                                                                return res8.map(new Function<H, T8<A, B, C, D, E, F, G, H>>() {
                                                                    public T8<A, B, C, D, E, F, G, H> apply(final H h) {
                                                                        return new T8(a, b, c, d, e, f, g, h);
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }), res1, res2, res3, res4, res5, res6, res7, res8);
    }

    public static <A, B, C, D, E, F, G, H, I> JsResult<T9<A, B, C, D, E, F, G, H, I>> combine(final JsResult<A> res1, final JsResult<B> res2, final JsResult<C> res3, final JsResult<D> res4, final JsResult<E> res5, final JsResult<F> res6, final JsResult<G> res7, final JsResult<H> res8, final JsResult<I> res9) {
        return populateErrs(res1.flatMap(new Function<A, JsResult<T9<A, B, C, D, E, F, G, H, I>>>() {
            public JsResult<T9<A, B, C, D, E, F, G, H, I>> apply(final A a) {
                return res2.flatMap(new Function<B, JsResult<T9<A, B, C, D, E, F, G, H, I>>>() {
                    public JsResult<T9<A, B, C, D, E, F, G, H, I>> apply(final B b) {
                        return res3.flatMap(new Function<C, JsResult<T9<A, B, C, D, E, F, G, H, I>>>() {
                            public JsResult<T9<A, B, C, D, E, F, G, H, I>> apply(final C c) {
                                return res4.flatMap(new Function<D, JsResult<T9<A, B, C, D, E, F, G, H, I>>>() {
                                    public JsResult<T9<A, B, C, D, E, F, G, H, I>> apply(final D d) {
                                        return res5.flatMap(new Function<E, JsResult<T9<A, B, C, D, E, F, G, H, I>>>() {
                                            public JsResult<T9<A, B, C, D, E, F, G, H, I>> apply(final E e) {
                                                return res6.flatMap(new Function<F, JsResult<T9<A, B, C, D, E, F, G, H, I>>>() {
                                                    public JsResult<T9<A, B, C, D, E, F, G, H, I>> apply(final F f) {
                                                        return res7.flatMap(new Function<G, JsResult<T9<A, B, C, D, E, F, G, H, I>>>() {
                                                            public JsResult<T9<A, B, C, D, E, F, G, H, I>> apply(final G g) {
                                                                return res8.flatMap(new Function<H, JsResult<T9<A, B, C, D, E, F, G, H, I>>>() {
                                                                    public JsResult<T9<A, B, C, D, E, F, G, H, I>> apply(final H h) {
                                                                        return res9.map(new Function<I, T9<A, B, C, D, E, F, G, H, I>>() {
                                                                            public T9<A, B, C, D, E, F, G, H, I> apply(final I i) {
                                                                                return new T9(a, b, c, d, e, f, g, h, i);
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }), res1, res2, res3, res4, res5, res6, res7, res8, res9);
    }

    public static <A, B, C, D, E, F, G, H, I, J> JsResult<T10<A, B, C, D, E, F, G, H, I, J>> combine(final JsResult<A> res1, final JsResult<B> res2, final JsResult<C> res3, final JsResult<D> res4, final JsResult<E> res5, final JsResult<F> res6, final JsResult<G> res7, final JsResult<H> res8, final JsResult<I> res9, final JsResult<J> res10) {
        return populateErrs(res1.flatMap(new Function<A, JsResult<T10<A, B, C, D, E, F, G, H, I, J>>>() {
            public JsResult<T10<A, B, C, D, E, F, G, H, I, J>> apply(final A a) {
                return res2.flatMap(new Function<B, JsResult<T10<A, B, C, D, E, F, G, H, I, J>>>() {
                    public JsResult<T10<A, B, C, D, E, F, G, H, I, J>> apply(final B b) {
                        return res3.flatMap(new Function<C, JsResult<T10<A, B, C, D, E, F, G, H, I, J>>>() {
                            public JsResult<T10<A, B, C, D, E, F, G, H, I, J>> apply(final C c) {
                                return res4.flatMap(new Function<D, JsResult<T10<A, B, C, D, E, F, G, H, I, J>>>() {
                                    public JsResult<T10<A, B, C, D, E, F, G, H, I, J>> apply(final D d) {
                                        return res5.flatMap(new Function<E, JsResult<T10<A, B, C, D, E, F, G, H, I, J>>>() {
                                            public JsResult<T10<A, B, C, D, E, F, G, H, I, J>> apply(final E e) {
                                                return res6.flatMap(new Function<F, JsResult<T10<A, B, C, D, E, F, G, H, I, J>>>() {
                                                    public JsResult<T10<A, B, C, D, E, F, G, H, I, J>> apply(final F f) {
                                                        return res7.flatMap(new Function<G, JsResult<T10<A, B, C, D, E, F, G, H, I, J>>>() {
                                                            public JsResult<T10<A, B, C, D, E, F, G, H, I, J>> apply(final G g) {
                                                                return res8.flatMap(new Function<H, JsResult<T10<A, B, C, D, E, F, G, H, I, J>>>() {
                                                                    public JsResult<T10<A, B, C, D, E, F, G, H, I, J>> apply(final H h) {
                                                                        return res9.flatMap(new Function<I, JsResult<T10<A, B, C, D, E, F, G, H, I, J>>>() {
                                                                            public JsResult<T10<A, B, C, D, E, F, G, H, I, J>> apply(final I i) {
                                                                                return res10.map(new Function<J, T10<A, B, C, D, E, F, G, H, I, J>>() {
                                                                                    public T10<A, B, C, D, E, F, G, H, I, J> apply(final J j) {
                                                                                        return new T10(a, b, c, d, e, f, g, h, i, j);
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }), res1, res2, res3, res4, res5, res6, res7, res8, res9, res10);
    }
}