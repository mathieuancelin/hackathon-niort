package utils.json.mapping;

import com.google.common.base.Predicate;
import utils.json.JsError;
import utils.json.JsValue;
import utils.json.exceptions.ValidationError;

public class ReaderConstraints {

    public static CReader<String> email() {
        return matches("[\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[a-zA-Z0-9](?:[\\w-]*[\\w])?");
    }
    public static Reader<String> url() {
        return matches("^(http|https|ftp)\\://[a-zA-Z0-9\\-\\.]+\\.[a-z" +
                "A-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\\-\\._\\?\\,\\'/\\\\\\+&amp;%\\$#\\=~\\!])*$");
    }
    public static CReader<String> phone() {
        return matches("^([\\+][0-9]{1,3}([ \\.\\-]))?([\\(]{1}[0-9]{2,6}[\\)])?([0-9 \\.\\-/]{3,20})((x|ext|extension)[ ]?[0-9]{1,4})?$");
    }
    public static CReader<String> matches(final String pattern) {
        return new CReader<String>() {
            @Override
            public JsResult<String> read(JsValue value) {
                try {
                    String str = value.as(String.class);
                    if (str.matches(pattern)) {
                        return new JsSuccess<String>(str);
                    } else {
                        return new JsError<String>(new ValidationError("'" + str + "' does not match pattern '" + pattern + "'"));
                    }
                } catch (Exception e) {
                    return new JsError<String>(new ValidationError(e.getMessage()));
                }
            }
        };
    }
    public static CReader<Integer> min(final Integer min) {
        return new CReader<Integer>() {
            @Override
            public JsResult<Integer> read(JsValue value) {
                try {
                    Integer str = value.as(Integer.class);
                    if (str < min) {
                        return new JsError<Integer>(new ValidationError("'" + str + "' is below limit '" + min + "'"));
                    } else {
                        return new JsSuccess<Integer>(str);
                    }
                } catch (Exception e) {
                    return new JsError<Integer>(new ValidationError(e.getMessage()));
                }
            }
        };
    }
    public static CReader<Integer> max(final Integer max) {
        return new CReader<Integer>() {
            @Override
            public JsResult<Integer> read(JsValue value) {
                try {
                    Integer str = value.as(Integer.class);
                    if (str > max) {
                        return new JsError<Integer>(new ValidationError("'" + str + "' is over limit '" + max + "'"));
                    } else {
                        return new JsSuccess<Integer>(str);
                    }
                } catch (Exception e) {
                    return new JsError<Integer>(new ValidationError(e.getMessage()));
                }
            }
        };
    }
    public static <A> CReader<A> verify(final Predicate<A> p, final Reader<A> reads) {
        return new CReader<A>() {
            @Override
            public JsResult<A> read(JsValue value) {
                try {
                    JsResult<A> str = value.read(reads);
                    for (JsError<A> err : str.asError()) {
                        return err;
                    }
                    for (JsSuccess<A> success : str.asSuccess()) {
                        if (p.apply(success.get())) {
                            return new JsSuccess<A>(success.get());
                        } else {
                            return new JsError<A>(new ValidationError("Doesn't validate the predicate"));
                        }
                    }
                    throw new RuntimeException("Can't happen");
                } catch (Exception e) {
                    return new JsError<A>(new ValidationError(e.getMessage()));
                }
            }
        };
    }
}