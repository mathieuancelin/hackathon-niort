package utils.json.mapping;

import utils.json.JsValue;

public interface Writer<T>  {
    public JsValue write(T value);
}