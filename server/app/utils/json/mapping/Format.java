package utils.json.mapping;

public interface Format<T> extends Reader<T>, Writer<T> {}