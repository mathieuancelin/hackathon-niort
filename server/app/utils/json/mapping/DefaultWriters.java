package utils.json.mapping;

import utils.json.JsArray;
import utils.json.JsObject;
import utils.json.JsValue;
import utils.json.Json;

import java.util.List;

public class DefaultWriters {

    public static <T> CWriter<List<T>> seq(final Format<T> writer) {
        return seq((Writer<T>) writer);
    }

    public static <T> CWriter<List<T>> seq(final Writer<T> writer) {
        return new CWriter<List<T>>() {
            @Override
            public JsValue write(List<T> value) {
                JsArray array = Json.arr();
                for (T val : value) {
                    array = array.addElement(writer.write(val));
                }
                return array;
            }
        };
    }

    public static JsValue throwableAsJson(Throwable t, boolean printstacks) {
        return Json.toJson(t, new ThrowableWriter(printstacks));
    }

    public static JsObject throwableAsJsObject(Throwable t, boolean printstacks) {
        return Json.toJson(t, new ThrowableWriter(printstacks)).as(JsObject.class);
    }

    public static JsValue throwableAsJson(Throwable t) {
        return Json.toJson(t, new ThrowableWriter(true));
    }

    public static JsObject throwableAsJsObject(Throwable t) {
        return Json.toJson(t, new ThrowableWriter(true)).as(JsObject.class);
    }
}