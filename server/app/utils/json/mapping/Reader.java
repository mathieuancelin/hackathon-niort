package utils.json.mapping;

import utils.json.JsValue;

public interface Reader<T>  {
    public JsResult<T> read(JsValue value);
}