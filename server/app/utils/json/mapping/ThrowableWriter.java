package utils.json.mapping;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import utils.json.JsObject;
import utils.json.JsValue;
import utils.json.Json;
import utils.json.Syntax;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThrowableWriter extends CWriter<Throwable> {

    private final boolean printStacks;

    public ThrowableWriter(boolean printStacks) {
        this.printStacks = printStacks;
    }

    @Override
    public JsValue write(Throwable value) {
        StackTraceElement[] els = value.getStackTrace();
        List<StackTraceElement> elements = new ArrayList<StackTraceElement>();
        if (els != null && els.length != 0) {
            elements.addAll(Arrays.asList(els));
        }
        List<String> elementsAsStr = Lists.transform(elements, new Function<StackTraceElement, String>() {
            @Override
            public String apply(StackTraceElement input) {
                return input.toString();
            }
        });
        JsObject base = Json.obj(
                Syntax.$("message", value.getMessage()),
                Syntax.$("type", value.getClass().getName())
        );
        if (printStacks) {
            base = base.add(Syntax.$("stack", Json.arr(elementsAsStr)));
        }
        if (value.getCause() != null) {
            base = base.add(Json.obj(
                    Syntax.$("cause", Json.toJson(value.getCause(), new ThrowableWriter(printStacks)))
            ));
        }
        return base;
    }
}