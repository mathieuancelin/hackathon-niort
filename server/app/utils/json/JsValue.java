package utils.json;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.Lists;
import utils.functional.Option;
import utils.json.mapping.DefaultReaders;
import utils.json.mapping.JsResult;
import utils.json.mapping.Reader;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public abstract class JsValue implements Serializable {
    public <T extends JsValue> Iterable<T> extractAs(Class<T> clazz) {
        if (is(clazz)) {
            return Collections.singletonList(clazz.cast(this));
        }
        return Collections.emptyList();
    }
    public <T extends JsValue> boolean is(Class<T> clazz) {
        return clazz.isAssignableFrom(this.getClass());
    }
    abstract String toJsonString();
    public <T> T as(Class<T> clazz, Reader<T> reader) {
        return reader.read(this).getOpt().get();
    }
    public <T> T as(Reader<T> reader) {
        return reader.read(this).getOpt().get();
    }
    public <T> T as(Class<T> clazz) {
        return asOpt(clazz).get();
    }
    public <T> Option<T> asOpt(Reader<T> reader) {
        return reader.read(this).getOpt();
    }
    public <T> Option<T> asOpt(Class<T> clazz, Reader<T> reader) {
        return reader.read(this).getOpt();
    }
    public <T> Option<T> asOpt(Class<T> clazz) {
        for (Reader<T> reader : DefaultReaders.getReader(clazz)) {
            return reader.read(this).getOpt();
        }
        return Option.none();
    }
    public <T> JsResult<T> read(Class<T> clazz) {
        for (Reader<T> reader : DefaultReaders.getReader(clazz)) {
            return reader.read(this);
        }
        return new JsError<T>(new IllegalStateException("Cannot find reader for type " + clazz.getName()));
    }
    public <T> JsResult<T> read(Reader<T> reader) {
        return reader.read(this);
    }
    public <T> JsResult<T> validate(Reader<T> reader) {
        return reader.read(this);
    }
    public <A extends JsValue> JsResult<A> transform(Reader<A> reader) {
        return reader.read(this);
    }
    public Boolean exists(String field) {
        return false;
    }
    public JsValue field(String field) {
        return JsUndefined.JSUNDEFINED_INSTANCE;
    }
    public Option<JsValue> fieldAsOpt(String field) {
        return Option.none();
    }
    public List<JsValue> fields(String fieldName) {
        return Lists.newArrayList();
    }
    public List<JsValue> __(String fieldName) {
        return this.fields(fieldName);
    }
    public JsValue get(int idx) {
        return JsUndefined.JSUNDEFINED_INSTANCE;
    }
    public abstract boolean deepEquals(Object o);
    public abstract JsValue cloneNode();

    public JsonNode asPlay() { return asJackson(); }
    public JsonNode asJackson() { return Json.toJackson(this); }

    public String stringify() { return Json.stringify(this); }
    public String pretty() { return Json.prettyPrint(this); }
}