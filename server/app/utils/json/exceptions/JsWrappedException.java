package utils.json.exceptions;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;

import java.util.List;

public class JsWrappedException extends JsException {
    private final List<Exception> exceptions;

    public JsWrappedException(List<Exception> exceptions) {
        this.exceptions = exceptions;
    }

    @Override
    public void setStackTrace(StackTraceElement[] stackTraceElements) {
        throw new IllegalAccessError("Can set stackstrace");
    }

    @Override
    public String getMessage() {
        return Joiner.on(" | ").join(Iterables.transform(exceptions, new Function<Exception, String>() {
            @Override
            public String apply(java.lang.Exception e) {
                return e.getMessage();
            }
        }));
    }

    @Override
    public String getLocalizedMessage() {
        return Joiner.on(" | ").join(Iterables.transform(exceptions, new Function<Exception, String>() {
            @Override
            public String apply(java.lang.Exception e) {
                return e.getLocalizedMessage();
            }
        }));
    }

    @Override
    public String toString() {
        return getMessage();
    }

    @Override
    public void printStackTrace() {
        for (Exception e : exceptions) {
            e.printStackTrace();
        }
    }
}