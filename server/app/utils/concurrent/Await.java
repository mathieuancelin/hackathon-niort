package utils.concurrent;

import com.google.common.base.Preconditions;
import com.google.common.base.Throwables;
import utils.common.Duration;

import java.util.concurrent.TimeUnit;

/**
 * Utility to await future based computations
 */
public class Await {
    public static <T> T result(Future<T> future, Long timeout, TimeUnit unit) {
        Preconditions.checkNotNull(future);
        try {
            return future.toJavaFuture().get(timeout, unit);
        } catch (Exception e) {
            throw Throwables.propagate(e);
        }
    }

    public static <T> T result(Future<T> future, Duration duration) {
        Preconditions.checkNotNull(future);
        try {
            return future.toJavaFuture().get(duration.value, duration.unit);
        } catch (Exception e) {
            throw Throwables.propagate(e);
        }
    }

    public static <T> T resultForever(Future<T> future) {
        Preconditions.checkNotNull(future);
        try {
            return future.toJavaFuture().get(Long.MAX_VALUE, TimeUnit.HOURS);
        } catch (Exception e) {
            throw Throwables.propagate(e);
        }
    }

    public static <T> T resultOr(Future<T> future, T defaultValue, Long timeout, TimeUnit unit) {
        Preconditions.checkNotNull(future);
        try {
            return future.toJavaFuture().get(timeout, unit);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static <T> T resultOr(Future<T> future, T defaultValue, Duration duration) {
        Preconditions.checkNotNull(future);
        try {
            return future.toJavaFuture().get(duration.value, duration.unit);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static <T> T resultForeverOr(Future<T> future, T defaultValue) {
        Preconditions.checkNotNull(future);
        try {
            return future.toJavaFuture().get(Long.MAX_VALUE, TimeUnit.HOURS);
        } catch (Exception e) {
            return defaultValue;
        }
    }
}
