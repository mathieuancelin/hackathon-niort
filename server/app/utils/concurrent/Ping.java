package utils.concurrent;

import com.google.common.base.Preconditions;
import utils.common.Duration;
import utils.functional.Unit;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Enhanced countdown latch with Future callback mecanism
 */
public class Ping<T> {

    private final Promise<T> p = new Promise<T>();
    private final AtomicInteger latch;
    private final AtomicInteger count = new AtomicInteger(0);
    private final T what;

    private Ping(int times, T what) {
        Preconditions.checkArgument(times > 0);
        this.latch = new AtomicInteger(times);
        this.what = what;
    }

    public void ping() {
        countDown();
    }
    public void ping(T w) {
        countDown(w);
    }

    public Integer count() {
        return count.get();
    }

    public synchronized void countDown() {
        countDown(what);
    }
    public synchronized void countDown(T w) {
        count.incrementAndGet();
        if (latch.compareAndSet(0, -1)) {
            p.trySuccess(w); // will be triggered only once even if count is looping
        } else {
            if (latch.get() > 0) latch.decrementAndGet();
        }
    }

    public T await() {
        return await(new Duration(Long.MAX_VALUE, TimeUnit.MILLISECONDS));
    }

    public T await(Duration duration) {
        return Await.result(p.future(), duration);
    }

    public Future<T> asFuture() {
        return p.future();
    }

    public static Ping<Unit> times(int times) {
        return new Ping<Unit>(times, Unit.unit());
    }

    public static <T> Ping<T> timesWithValue(int times, T what) {
        return new Ping<T>(times, what);
    }
    public static <T> Ping<T> timesWithValues(int times) {
        return new Ping<T>(times, null);
    }
}
