package utils.concurrent;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import utils.common.Duration;
import utils.functional.Unit;

/**
 * Lazy reference to something. Memoization container
 *
 * @param <T>
 */
public class Lazy<T> {

    private final Supplier<T> supplier;

    private Lazy(final Function<Unit, T> f) {
        this.supplier = Suppliers.memoize(() -> f.apply(Unit.unit()));
    }

    private Lazy(final Function<Unit, T> f, Duration duration) {
        this.supplier = Suppliers.memoizeWithExpiration(() -> f.apply(Unit.unit()), duration.value, duration.unit);
    }

    public static <T> Lazy<T> of(final Function<Unit, T> f) {
        return new Lazy<T>(f);
    }

    public static <T> Lazy<T> cached(final Function<Unit, T> f, Duration duration) {
        return new Lazy<T>(f, duration);
    }

    public T get() {
        return supplier.get();
    }
}
