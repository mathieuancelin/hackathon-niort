package utils.common;

public class FastRuntimeException extends RuntimeException {

    public FastRuntimeException() {
    }

    public FastRuntimeException(String s) {
        super(s);
    }

    public FastRuntimeException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public FastRuntimeException(Throwable throwable) {
        super(throwable);
    }

    /**
     * Since we override this method, no stacktrace is generated - much faster
     * @return always null
     */
    public Throwable fillInStackTrace() {
        return null;
    }
}
