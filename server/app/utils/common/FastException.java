package utils.common;

public class FastException extends Exception {

    public FastException() {
    }

    public FastException(String s) {
        super(s);
    }

    public FastException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public FastException(Throwable throwable) {
        super(throwable);
    }

    /**
     * Since we override this method, no stacktrace is generated - much faster
     * @return always null
     */
    public Throwable fillInStackTrace() {
        return null;
    }
}
