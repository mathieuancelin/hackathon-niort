package utils.common;

import java.util.concurrent.atomic.AtomicLong;

public class MeasuredRate {

    private final AtomicLong lastBucket = new AtomicLong(0);
    private final AtomicLong currentBucket = new AtomicLong(0);
    private final long sampleInterval;
    private volatile long threshold;

    public MeasuredRate(long sampleInterval) {
        this.sampleInterval = sampleInterval;
        this.threshold = System.currentTimeMillis() + sampleInterval;
    }

    /**
     * Returns the count in the last sample interval
     */
    public long getCount() {
        checkAndResetWindow();
        return lastBucket.get();
    }

    /**
     * Returns the count in the current sample interval which will be incomplete.
     */
    public long getCurrentCount() {
        checkAndResetWindow();
        return currentBucket.get();
    }

    public void increment() {
        checkAndResetWindow();
        currentBucket.incrementAndGet();
    }

    public void mark() {
        increment();
    }

    public void increment(long of) {
        checkAndResetWindow();
        currentBucket.addAndGet(of);
    }

    public void mark(long of) {
        increment(of);
    }

    private void checkAndResetWindow() {
        long now = System.currentTimeMillis();
        if (threshold < now) {
            lastBucket.set(currentBucket.get());
            currentBucket.set(0);
            threshold = now + sampleInterval;
        }
    }

    public void reset() {
        lastBucket.set(0);
        currentBucket.set(0);
    }

    public String toString() {
        return "count:" + getCount() + "currentCount:" + getCurrentCount();
    }
}