package utils.elasticsearch;

import play.Logger;
import play.Play;
import play.libs.ws.WS;
import play.libs.ws.WSResponse;
import utils.common.Duration;
import utils.common.FastRuntimeException;
import utils.concurrent.Await;
import utils.concurrent.Future;
import utils.functional.Option;
import utils.functional.Try;
import utils.functional.Unit;
import utils.json.JsNull;
import utils.json.JsValue;
import utils.json.Json;
import utils.json.Syntax;
import utils.json.mapping.DefaultReaders;
import utils.json.mapping.JsResult;
import utils.json.mapping.Reader;
import utils.json.mapping.Writer;

import java.math.BigDecimal;
import java.util.List;

public class Index {

    private final String name;
    private final String indexUrl;

    public Index(String name, String rootIndexUrl) {
        this.name = name;
        this.indexUrl = rootIndexUrl + "/" + name;
    }

    public Future<JsValue> saveAsJsValue(String id, JsValue data) {
        return Future.toFuture(WS.url(indexUrl + "/" + id).post(data.asJackson())).map(r -> Json.parse(r.getBody()));
    }

    public Future<Unit> delete(String id) {
        return Future.toFuture(WS.url(indexUrl + "/" + id).delete()).map(r -> Unit.unit());
    }

    public <T> Future<JsValue> save(String id, T data, Writer<T> format) {
        return this.saveAsJsValue(id, Json.toJson(data, format));
    }

    public Future<Option<JsValue>> get(String id) {
        return Future.toFuture(WS.url(indexUrl + "/" + id).get()).map(r -> Json.parse(r.getBody()).field("_source").asOpt(JsValue.class));
    }

    public Future<SearchResult> search(JsValue query) {
       return Future.toFuture(WS.url(indexUrl + "/_search").setBody(query.asJackson()).get()).flatMap(this::handleResponse);
    }

    public Future<SearchResult> search(String query) {
        return Future.toFuture(WS.url(indexUrl + "/_search").setQueryParameter("q", query).get()).flatMap(this::handleResponse);
    }

    private Future<SearchResult> handleResponse(WSResponse r) {
        if (r.getStatus() == 200) {
            Try<SearchResult> ttry = Json.tryParse(r.getBody()).map(b -> b.as(SearchResult.reader));
            for(Throwable t : ttry.asFailure()) {
                return Future.failed(t);
            }
            return Future.successful(ttry.get());
        } else {
            Logger.error("Error while searching " + r.getBody());
            return Future.failed(new FastRuntimeException("Error while searching " + r.getBody()));
        }
    }

    public static Index of(String name) {
        return of(name, Json.obj(), Json.obj(), Json.obj());
    }

    public static Index of(String name, JsValue index, JsValue settings, JsValue mappings) {
        String url = Play.application().configuration().getString("elasticsearch.url", "http://localhost:9200");
        String rootIndex = Play.application().configuration().getString("elasticsearch.rootIndex", "hackathon");
        String rootIndexUrl = url + "/" + rootIndex;
        if (index.is(JsNull.class)) {
            index = Json.obj(
                Syntax.$("settings", settings),
                Syntax.$("mappings", mappings)
            );
        }
        final JsValue indexToCreate = index;
        Future fu = Future.toFuture(WS.url(rootIndexUrl + "/" + name).head()).map(response -> {
            if (response.getStatus() == 200) {
                Logger.info("Index " + name + " existing nothing to do");
            } else if (response.getStatus() == 404) {
                Logger.info("Creating index " + Json.prettyPrint(indexToCreate));
                WS.url(rootIndexUrl + "/" + name).post(indexToCreate.asJackson()).map(r -> {
                    if (r.getStatus() == 200 || r.getStatus() == 201) {
                        Logger.info("index created");
                    } else {
                        Logger.error("Error while creating index " + name + " : " + r.getBody());
                    }
                    return null;
                });
            } else {
                Logger.error("Error while creating index " + response.getBody());
            }
            return null;
        });
        Await.result(fu, Duration.parse("30s"));
        return new Index(name, rootIndexUrl);
    }

    public static class SearchResult  {
        public Integer took;
        public Boolean timed_out;
        public Shard _shards;
        public Hits hits;

        public SearchResult() {
        }

        public SearchResult(Integer took, Boolean timed_out, Shard _shards, Hits hits) {
            this.took = took;
            this.timed_out = timed_out;
            this._shards = _shards;
            this.hits = hits;
        }

        public Integer getTook() {
            return took;
        }

        public void setTook(Integer took) {
            this.took = took;
        }

        public Boolean getTimed_out() {
            return timed_out;
        }

        public void setTimed_out(Boolean timed_out) {
            this.timed_out = timed_out;
        }

        public Shard get_shards() {
            return _shards;
        }

        public void set_shards(Shard _shards) {
            this._shards = _shards;
        }

        public Hits getHits() {
            return hits;
        }

        public void setHits(Hits hits) {
            this.hits = hits;
        }

        @Override
        public String toString() {
            return "SearchResult{" +
                    "took=" + took +
                    ", timed_out=" + timed_out +
                    ", _shards=" + _shards +
                    ", hits=" + hits +
                    '}';
        }

        public static Reader<SearchResult> reader = value -> {
            try {
                return JsResult.success(new SearchResult(
                    value.field("took").as(Integer.class),
                    value.field("timed_out").as(Boolean.class),
                    value.field("_shards").as(Shard.reader),
                    value.field("hits").as(Hits.reader)
                ));
            } catch (Exception e) {
                return JsResult.error(e);
            }
        };
    }

    private static class Shard {
        public Integer total;
        public Integer successful;
        public Integer failed;

        public Shard() {
        }

        public Shard(Integer total, Integer successful, Integer failed) {
            this.total = total;
            this.successful = successful;
            this.failed = failed;
        }

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

        public Integer getSuccessful() {
            return successful;
        }

        public void setSuccessful(Integer successful) {
            this.successful = successful;
        }

        public Integer getFailed() {
            return failed;
        }

        public void setFailed(Integer failed) {
            this.failed = failed;
        }

        @Override
        public String toString() {
            return "Shard{" +
                    "total=" + total +
                    ", successful=" + successful +
                    ", failed=" + failed +
                    '}';
        }

        public static Reader<Shard> reader = value -> {
            try {
                return JsResult.success(new Shard(
                    value.field("total").as(Integer.class),
                    value.field("successful").as(Integer.class),
                    value.field("failed").as(Integer.class)
                ));
            } catch (Exception e) {
                return JsResult.error(e);
            }
        };
    }

    private static class Hits {
        public Integer total;
        public List<Hit> hits;

        public Hits() {
        }

        public Hits(Integer total, List<Hit> hits) {
            this.total = total;
            this.hits = hits;
        }

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

        public List<Hit> getHits() {
            return hits;
        }

        public void setHits(List<Hit> hits) {
            this.hits = hits;
        }

        @Override
        public String toString() {
            return "Hits{" +
                    "total=" + total +
                    ", hits=" + hits +
                    '}';
        }

        public static Reader<Hits> reader = value -> {
            try {
                return JsResult.success(new Hits(
                    value.field("total").as(Integer.class),
                    value.field("hits").as(DefaultReaders.seq(Hit.reader))
                ));
            } catch (Exception e) {
                return JsResult.error(e);
            }
        };
    }

    private static class Hit {
        public String _index;
        public String _type;
        public String _id;
        public Option<BigDecimal> _score;
        public JsValue _source;

        public Hit() {
        }

        public Hit(String _index, String _type, String _id, Option<BigDecimal> _score, JsValue _source) {
            this._index = _index;
            this._type = _type;
            this._id = _id;
            this._score = _score;
            this._source = _source;
        }

        public String get_index() {
            return _index;
        }

        public void set_index(String _index) {
            this._index = _index;
        }

        public String get_type() {
            return _type;
        }

        public void set_type(String _type) {
            this._type = _type;
        }

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public Option<BigDecimal> get_score() {
            return _score;
        }

        public void set_score(Option<BigDecimal> _score) {
            this._score = _score;
        }

        public JsValue get_source() {
            return _source;
        }

        public void set_source(JsValue _source) {
            this._source = _source;
        }

        @Override
        public String toString() {
            return "Hit{" +
                    "_index='" + _index + '\'' +
                    ", _type='" + _type + '\'' +
                    ", _id='" + _id + '\'' +
                    ", _score=" + _score +
                    ", _source=" + _source +
                    '}';
        }

        public static Reader<Hit> reader = value -> {
            try {
                return JsResult.success(new Hit(
                    value.field("_index").as(String.class),
                    value.field("_type").as(String.class),
                    value.field("_id").as(String.class),
                    value.field("_score").asOpt(BigDecimal.class),
                    value.field("_source").as(JsValue.class)
                ));
            } catch(Exception e) {
                return JsResult.error(e);
            }
        };
    }
}
