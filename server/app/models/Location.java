package models;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.Lists;
import utils.json.Jackson;
import utils.json.JsArray;
import utils.json.JsValue;
import utils.json.Json;
import utils.json.mapping.Format;
import utils.json.mapping.JsResult;
import utils.json.mapping.Reader;

import java.math.BigDecimal;

public class Location {
    public final BigDecimal lat;
    public final BigDecimal lon;

    public Location(Double lat, Double lon) {
        this.lat = BigDecimal.valueOf(lat);
        this.lon = BigDecimal.valueOf(lon);
    }

    public Location(BigDecimal lat, BigDecimal lon) {
        this.lat = lat;
        this.lon = lon;
    }

    @Override
    public String toString() {
        return "Location{" +
                "lat='" + lat + '\'' +
                ", lon='" + lon + '\'' +
                '}';
    }

    public Location withLongitude(Double lon) {
        return new Location(lat, BigDecimal.valueOf(lon));
    }

    public Location withLatitude(Double lat) {
        return new Location(BigDecimal.valueOf(lat), lon);
    }

    public static JsResult<Location> parse(String json) {
        return LOC_FORMAT.read(Json.parse(json));
    }

    public static JsResult<Location> parse(JsValue json) {
        return LOC_FORMAT.read(json);
    }

    public static JsResult<Location> parse(JsonNode json) {
        return LOC_FORMAT.read(Jackson.jsonNodeToJsValue(json));
    }

    public static JsResult<Location> parseObj(JsonNode json) {
        return OBJ_READER.read(Jackson.jsonNodeToJsValue(json));
    }

    public static Reader<Location> OBJ_READER = obj -> {
        try {
            return JsResult.success(new Location(
                    obj.field("lat").as(Double.class),
                    obj.field("lon").as(Double.class)
            ));
        } catch (Exception e) {
            e.printStackTrace();
            return JsResult.error(e);
        }
    };

    public static Format<Location> LOC_FORMAT = new Format<Location>() {
        @Override
        public JsResult<Location> read(JsValue value) {
            try {
                return JsResult.success(new Location(
                    value.as(JsArray.class).get(0).as(Double.class),
                    value.as(JsArray.class).get(1).as(Double.class)
                ));
            } catch (Exception e) {
                return JsResult.error(e);
            }
        }

        @Override
        public JsValue write(Location value) {
            return Json.arr(Lists.newArrayList(value.lat, value.lon));
        }
    };
}
