package models;

import utils.json.JsValue;
import utils.json.Json;
import utils.json.mapping.Format;
import utils.json.mapping.JsResult;
import static utils.json.Syntax.*;

/**
 * Created by mathieuancelin on 28/03/2015.
 */
public class TechnicalHashState {

    public Integer fix_key; // 1,
    public String authentification; // "N",
    public Integer near_start; // 0,
    public Integer immobilizer; // 1,
    public Integer contact; // 0,
    public Integer rental_status; // 0,
    public Integer near_end;// 0,
    public String range_type;// "N"

    @Override
    public String toString() {
        return "TechnicalHashState{" +
                "fix_key=" + fix_key +
                ", authentification='" + authentification + '\'' +
                ", near_start=" + near_start +
                ", immobilizer=" + immobilizer +
                ", contact=" + contact +
                ", rental_status=" + rental_status +
                ", near_end=" + near_end +
                ", range_type=" + range_type +
                '}';
    }

    public TechnicalHashState(Integer fix_key, String authentification, Integer near_start, Integer immobilizer, Integer contact, Integer rental_status, Integer near_end, String range_type) {
        this.fix_key = fix_key;
        this.authentification = authentification;
        this.near_start = near_start;
        this.immobilizer = immobilizer;
        this.contact = contact;
        this.rental_status = rental_status;
        this.near_end = near_end;
        this.range_type = range_type;
    }

    public TechnicalHashState() {
    }

    public TechnicalHashState setFix_key(Integer fix_key) {
        this.fix_key = fix_key;
        return this;
    }

    public TechnicalHashState setAuthentification(String authentification) {
        this.authentification = authentification;
        return this;
    }

    public TechnicalHashState setNear_start(Integer near_start) {
        this.near_start = near_start;
        return this;
    }

    public TechnicalHashState setImmobilizer(Integer immobilizer) {
        this.immobilizer = immobilizer;
        return this;
    }

    public TechnicalHashState setContact(Integer contact) {
        this.contact = contact;
        return this;
    }

    public TechnicalHashState setRental_status(Integer rental_status) {
        this.rental_status = rental_status;
        return this;
    }

    public TechnicalHashState setNear_end(Integer near_end) {
        this.near_end = near_end;
        return this;
    }

    public TechnicalHashState setRange_type(String range_type) {
        this.range_type = range_type;
        return this;
    }

    public static final Format<TechnicalHashState> FORMAT = new Format<TechnicalHashState>() {
        @Override
        public JsResult<TechnicalHashState> read(JsValue value) {
            try {
                return JsResult.success(new TechnicalHashState(
                    value.field("fix_key").as(Integer.class),
                    value.field("authentification").as(String.class),
                    value.field("near_start").as(Integer.class),
                    value.field("immobilizer").as(Integer.class),
                    value.field("contact").as(Integer.class),
                    value.field("rental_status").as(Integer.class),
                    value.field("near_end").as(Integer.class),
                    value.field("range_type").as(String.class)
                ));
            } catch (Exception e) {
                e.printStackTrace();
                return JsResult.error(e);
            }
        }

        @Override
        public JsValue write(TechnicalHashState value) {
            return Json.obj(
                $("fix_key", value.fix_key),
                $("authentification", value.authentification),
                $("near_start", value.near_start),
                $("immobilizer", value.immobilizer),
                $("contact", value.contact),
                $("rental_status", value.rental_status),
                $("near_end", value.near_end),
                $("range_type", value.range_type)
            );
        }
    };
}
