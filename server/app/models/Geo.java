package models;

import com.google.common.collect.Lists;
import utils.json.JsArray;
import utils.json.JsValue;
import utils.json.Json;
import utils.json.mapping.Format;
import utils.json.mapping.JsResult;

import static utils.json.Syntax.$;

/**
 * Created by florianpires on 28/03/15.
 */
public class Geo {

    public String name;

    public Geo(String name) {
        this.name = name;
    }

    public static Format<Geo> GEO_FORMAT = new Format<Geo>() {
        @Override
        public JsResult<Geo> read(JsValue value) {
            try {
                return JsResult.success(new Geo(
                        value.field("name").as(String.class)));
            } catch (Exception e) {
                return JsResult.error(e);
            }
        }

        @Override
        public JsValue write(Geo value) {return Json.obj(
                $("name", value.name));
        }
    };
}
