package models;

import services.CarServiceClient;
import utils.concurrent.Future;
import utils.functional.Option;
import utils.json.JsObject;
import utils.json.JsValue;
import utils.json.Json;
import utils.json.mapping.Format;
import utils.json.mapping.JsResult;

import static utils.json.Syntax.$;
import static utils.play.ControllerHelper.jsonOk;

/*
{"updated_at_utc":"2015-03-28T09:04:14Z","vehicle_m":74949901,"latitude":46.31561,"connection_status":"sleeping","serial_number":"217320096","public_id":"5ae87ac7-f155-4780-bb91-5fdc24567904","mode":"enabled","functional_state":"NORMAL","updated_at_ts":1427533454,"technical_state_hash":{"fix_key":1,"authentification":"N","near_start":0,"immobilizer":1,"contact":0,"rental_status":0,"near_end":0,"range_type":"N"},"technical_state_string":"NON DISPO LOCATION:0 -ADRESSE FIN -ADRESSE DEPART +FIXKEY +ANTIDEMARRAGE -CONTACT NON DISPO NONE    (241)","current_rfid":0,"matrice_state":241,"comment":"","phone_number":"0788356972","longitude":-0.41094}
 */
public class Car {

    public String updated_at_utc;
    public Long vehicle_m;
    public Double latitude;
    public String connection_status;
    public String serial_number;
    public String public_id;
    public String mode;
    public String functional_state;
    public Long updated_at_ts;
    public String technical_state_string;
    public Integer current_rfid;
    public Integer matrice_state;
    public String comment;
    public String phone_number;
    public Double longitude;
    public TechnicalHashState technical_state_hash;

    @Override
    public String toString() {
        return "Car{" +
                "updated_at_utc='" + updated_at_utc + '\'' +
                ", vehicle_m=" + vehicle_m +
                ", latitude=" + latitude +
                ", connection_status='" + connection_status + '\'' +
                ", serial_number='" + serial_number + '\'' +
                ", public_id='" + public_id + '\'' +
                ", mode='" + mode + '\'' +
                ", functional_state='" + functional_state + '\'' +
                ", updated_at_ts=" + updated_at_ts +
                ", technical_state_string='" + technical_state_string + '\'' +
                ", current_rfid=" + current_rfid +
                ", matrice_state=" + matrice_state +
                ", comment='" + comment + '\'' +
                ", phone_number='" + phone_number + '\'' +
                ", longitude=" + longitude +
                ", technical_state_hash=" + technical_state_hash +
                '}';
    }

    public static final Format<Car> FORMAT = new Format<Car>() {
        @Override
        public JsResult<Car> read(JsValue value) {
            try {
                return JsResult.success(new Car(
                    value.field("updated_at_utc").as(String.class),
                    value.field("vehicle_m").as(Long.class),
                    value.field("latitude").as(Double.class),
                    value.field("connection_status").as(String.class),
                    value.field("serial_number").as(String.class),
                    value.field("public_id").as(String.class),
                    value.field("mode").as(String.class),
                    value.field("functional_state").as(String.class),
                    value.field("updated_at_ts").as(Long.class),
                    value.field("technical_state_string").as(String.class),
                    value.field("current_rfid").as(Integer.class),
                    value.field("matrice_state").as(Integer.class),
                    value.field("comment").as(String.class),
                    value.field("phone_number").as(String.class),
                    value.field("longitude").as(Double.class),
                    value.field("technical_state_hash").as(TechnicalHashState.class, TechnicalHashState.FORMAT)
                ));
            } catch (Exception e ) {
                e.printStackTrace();
                return JsResult.error(e);
            }
        }

        @Override
        public JsValue write(Car value) {
            return Json.obj(
                $("updated_at_utc", value.updated_at_utc),
                $("vehicle_m", value.vehicle_m),
                $("latitude", value.latitude),
                $("connection_status", value.connection_status),
                $("serial_number", value.serial_number),
                $("public_id", value.public_id),
                $("mode", value.mode),
                $("functional_state", value.functional_state),
                $("updated_at_ts", value.updated_at_ts),
                $("technical_state_string", value.technical_state_string),
                $("technical_state", TechnicalHashState.FORMAT.write(value.technical_state_hash)),
                $("current_rfid", value.current_rfid),
                $("matrice_state", value.matrice_state),
                $("comment", value.comment),
                $("phone_number", value.phone_number),
                $("longitude", value.longitude)
            );
        }
    };

    public Car() {
    }

    public Car(String updated_at_utc, Long vehicle_m, Double latitude, String connection_status, String serial_number, String public_id, String mode, String functional_state, Long updated_at_ts, String technical_state_string, Integer current_rfid, Integer matrice_state, String comment, String phone_number, Double longitude, TechnicalHashState technical_state_hash) {
        this.updated_at_utc = updated_at_utc;
        this.vehicle_m = vehicle_m;
        this.latitude = latitude;
        this.connection_status = connection_status;
        this.serial_number = serial_number;
        this.public_id = public_id;
        this.mode = mode;
        this.functional_state = functional_state;
        this.updated_at_ts = updated_at_ts;
        this.technical_state_string = technical_state_string;
        this.current_rfid = current_rfid;
        this.matrice_state = matrice_state;
        this.comment = comment;
        this.phone_number = phone_number;
        this.longitude = longitude;
        this.technical_state_hash = technical_state_hash;
    }

    public Car setUpdated_at_utc(String updated_at_utc) {
        this.updated_at_utc = updated_at_utc;
        return this;
    }

    public Car setVehicle_m(Long vehicle_m) {
        this.vehicle_m = vehicle_m;
        return this;
    }

    public Car setLatitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public Car setConnection_status(String connection_status) {
        this.connection_status = connection_status;
        return this;
    }

    public Car setSerial_number(String serial_number) {
        this.serial_number = serial_number;
        return this;
    }

    public Car setPublic_id(String public_id) {
        this.public_id = public_id;
        return this;
    }

    public Car setMode(String mode) {
        this.mode = mode;
        return this;
    }

    public Car setFunctional_state(String functional_state) {
        this.functional_state = functional_state;
        return this;
    }

    public Car setUpdated_at_ts(Long updated_at_ts) {
        this.updated_at_ts = updated_at_ts;
        return this;
    }

    public Car setTechnical_state_string(String technical_state_string) {
        this.technical_state_string = technical_state_string;
        return this;
    }

    public Car setCurrent_rfid(Integer current_rfid) {
        this.current_rfid = current_rfid;
        return this;
    }

    public Car setMatrice_state(Integer matrice_state) {
        this.matrice_state = matrice_state;
        return this;
    }

    public Car setComment(String comment) {
        this.comment = comment;
        return this;
    }

    public Car setPhone_number(String phone_number) {
        this.phone_number = phone_number;
        return this;
    }

    public Car setLongitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    public Car setTechnical_state_hash(TechnicalHashState technical_state_hash) {
        this.technical_state_hash = technical_state_hash;
        return this;
    }

    public static Future<Option<Car>> fetchCar(String id) {
        return CarServiceClient.fetchBoxData(id).map(opt -> opt.map(car -> FORMAT.read(car)).filter(r -> r.isSuccess()).map(r -> r.get()));
    }

    public Future<JsObject> toggleClose() {
        return CarServiceClient.carAction(this.public_id, "switch_doors").map(s -> Json.obj($("rid", s)));
    }
}
