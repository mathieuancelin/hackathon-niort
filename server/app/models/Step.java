package models;

import com.fasterxml.jackson.databind.JsonNode;
import utils.json.Jackson;
import utils.json.JsValue;
import utils.json.Json;
import utils.json.mapping.Format;
import utils.json.mapping.JsResult;
import utils.play.MongoStore;

import static utils.json.Syntax.$;

/**
 * Created by florianpires on 28/03/15.
 */
public class Step {

    public final String type;
    public final String libType;
    public final String timeLeft;
    public final String startPoint;
    public final String endPoint;

    public Step(String type, String libType, String timeLeft, String startPoint, String endPoint) {
        this.type = type;
        this.libType = libType;
        this.timeLeft = timeLeft;
        this.startPoint = startPoint;
        this.endPoint = endPoint;
    }

    public static JsResult<Step> parse(String json) {
        return STEP_FORMAT.read(Json.parse(json));
    }

    public static JsResult<Step> parse(JsValue json) {
        return STEP_FORMAT.read(json);
    }

    public static JsResult<Step> parse(JsonNode json) {
        return STEP_FORMAT.read(Jackson.jsonNodeToJsValue(json));
    }

    public static Format<Step> STEP_FORMAT = new Format<Step>() {
        @Override
        public JsResult<Step> read(JsValue value) {
            try {
                return JsResult.success(
                        new Step(
                                value.field("type").as(String.class),
                                value.field("libType").as(String.class),
                                value.field("timeLeft").as(String.class),
                                value.field("startPoint").as(String.class),
                                value.field("endPoint").as(String.class)
                                )
                );
            } catch (Exception e) {
                return JsResult.error(e);
            }
        }

        @Override
        public JsValue write(Step value) {
            return Json.obj(
                    $("type", value.type),
                    $("libType", value.libType),
                    $("timeLeft", value.timeLeft),
                    $("startPoint", value.startPoint),
                    $("endPoint", value.endPoint)
            );
        }
    };

    public static final MongoStore<Step> store = MongoStore.of("Steps", STEP_FORMAT);

}
