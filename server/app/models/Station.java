package models;

import com.fasterxml.jackson.databind.JsonNode;
import utils.collection.Container;
import utils.json.Jackson;
import utils.json.JsString;
import utils.json.JsValue;
import utils.json.Json;
import utils.json.mapping.DefaultReaders;
import utils.json.mapping.DefaultWriters;
import utils.json.mapping.Format;
import utils.json.mapping.JsResult;
import utils.json.mapping.Writer;
import utils.play.MongoStore;

import java.util.List;

import static utils.json.Syntax.$;

public class Station {

    public final String _id;
    public final Location location;
    public final String name;
    public final String type;
    public final List<String> lines;

    public Station(Location location, String name, String type, List<String> lines) {
        this._id = org.bson.types.ObjectId.get().toString();
        this.location = location;
        this.name = name;
        this.lines = lines;
        this.type = type;
    }

    public Station(String _id, Location location, String name, String type, List<String> lines) {
        this._id = _id;
        this.location = location;
        this.name = name;
        this.lines = lines;
        this.type = type;
    }

    public Station setLocation(Location location) {
        return new Station(location, this.name, this.type, this.lines);
    }

    public Station setName(String name) {
        return new Station(this.location, name,  this.type, this.lines);
    }

    public Station setLines(List<String> lines) {
        return new Station(this.location, this.name,  this.type, lines);
    }

    public Station setType(String type) {
        return new Station(this.location, this.name, type, this.lines);
    }


    public static JsResult<Station> parse(String json) {
        return FORMAT.read(Json.parse(json));
    }

    public static JsResult<Station> parse(JsValue json) {
        return FORMAT.read(json);
    }

    public static JsResult<Station> parse(JsonNode json) {
        return FORMAT.read(Jackson.jsonNodeToJsValue(json));
    }

    public Station insert() {
        return store.insert(this);
    }

    public static final Format<Station> FORMAT = new Format<Station>() {
        @Override
        public JsResult<Station> read(JsValue value) {
            try {
                return JsResult.success(new Station(
                    value.field("_id").as(String.class),
                    value.field("location").as(Location.LOC_FORMAT),
                    value.field("name").as(String.class),
                    value.field("type").as(String.class),
                    value.field("lines").as(DefaultReaders.seq(DefaultReaders.STRING_READER))
                ));
            } catch (Exception e) {
                e.printStackTrace();
                return JsResult.error(e);
            }
        }

        @Override
        public JsValue write(Station value) {
            return Json.obj(
                    $("_id", value._id),
                    $("name", value.name),
                    $("type", value.type),
                    $("location", Location.LOC_FORMAT.write(value.location)),
                    $("lines", Json.toJson(value.lines, DefaultWriters.seq(new Writer<String>() {
                        @Override
                        public JsValue write(String value) {
                            return new JsString(value);
                        }
                    })))
            );
        }
    };

    public static Container<Station> near(Location location, Double distance) {
        return store.find(
            $("location", Json.obj(
                $("$near", Json.arr(location.lat, location.lon)),
                $("$maxDistance", distance / 111120)
        )));
    }

    public static final MongoStore<Station> store = MongoStore.of("stations", FORMAT);
}
