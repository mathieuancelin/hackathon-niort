package models;

import com.fasterxml.jackson.databind.JsonNode;
import org.joda.time.DateTime;
import utils.concurrent.Await;
import utils.json.Jackson;
import utils.json.JsObject;
import utils.json.JsValue;
import utils.json.Json;
import utils.json.mapping.Format;
import utils.json.mapping.JsResult;
import utils.play.MongoStore;

import static utils.json.Syntax.$;

public class UserSession {

    public final String   _id          ;
    public final String   uuid         ;
    public final String   carId        ;
    public final DateTime start        ;
    public final DateTime stop         ;
    public final Long     totalDistance;
    public final Boolean  done         ;
    public final Long startMeters;
    public final Long stopMeters;

    public UserSession(String uuid, String carId, DateTime start, DateTime stop, Long totalDistance, Boolean done, Long startMeters, Long stopMeters) {
        this._id = org.bson.types.ObjectId.get().toString();
        this.carId = carId;
        this.uuid = uuid;
        this.start = start;
        this.stop = stop;
        this.totalDistance = totalDistance;
        this.done = done;
        this.startMeters = startMeters;
        this.stopMeters = stopMeters;
    }

    public UserSession(String _id, String uuid, String carId, DateTime start, DateTime stop, Long totalDistance, Boolean done, Long startMeters, Long stopMeters) {
        this._id = _id;
        this.carId = carId;
        this.uuid = uuid;
        this.start = start;
        this.stop = stop;
        this.totalDistance = totalDistance;
        this.done = done;
        this.startMeters = startMeters;
        this.stopMeters = stopMeters;
    }

    @Override
    public String toString() {
        return "UserSession{" +
                "_id='" + _id + '\'' +
                ", uuid='" + uuid + '\'' +
                ", carId='" + carId + '\'' +
                ", start=" + start +
                ", stop=" + stop +
                ", totalDistance=" + totalDistance +
                ", done=" + done +
                ", startMeters=" + startMeters +
                ", stopMeters=" + stopMeters +
                '}';
    }

    public UserSession save() {
        if (store.findById(this._id).isDefined()) {
            return store.update(this._id, this);
        } else {
            return store.insert(this);
        }
    }

    public void delete() {
        store.deleteOne(this._id);
    }

    public JsValue toJson() {
        return FORMAT.write(this);
    }


    public static JsResult<UserSession> parse(String json) {
        return FORMAT.read(Json.parse(json));
    }

    public static JsResult<UserSession> parse(JsValue json) {
        return FORMAT.read(json);
    }

    public static JsResult<UserSession> parse(JsonNode json) {
        return FORMAT.read(Jackson.jsonNodeToJsValue(json));
    }

    public UserSession withId(String _id) {
        return new UserSession(_id, this.uuid, this.carId, this.start, this.stop, this.totalDistance, this.done, this.startMeters, this.stopMeters);
    }

    public UserSession withUuid(String uuid) {
        return new UserSession(this._id, uuid, this.carId, this.start, this.stop, this.totalDistance, this.done, this.startMeters, this.stopMeters);
    }

    public UserSession withStart(DateTime start) {
        return new UserSession(this._id, this.uuid, this.carId, start, this.stop, this.totalDistance, this.done, this.startMeters, this.stopMeters);
    }

    public UserSession withStop(DateTime stop) {
        return new UserSession(this._id, this.uuid, this.carId, this.start, stop, this.totalDistance, this.done, this.startMeters, this.stopMeters);
    }

    public UserSession withTotalDistance(Long totalDistance) {
        return new UserSession(this._id, this.uuid, this.carId, this.start, this.stop, totalDistance, this.done, this.startMeters, this.stopMeters);
    }

    public UserSession withDone(Boolean done) {
        return new UserSession(this._id, this.uuid, this.carId, this.start, this.stop, this.totalDistance, done, this.startMeters, this.stopMeters);
    }

    public UserSession withStartMeters(Long startMeters) {
        return new UserSession(this._id, this.uuid, this.start, this.stop, this.totalDistance, done, startMeters, this.stopMeters);
    }

    public UserSession withStopMeters(Long stopMeters) {
        return new UserSession(this._id, this.uuid, this.carId, this.start, this.stop, this.totalDistance, done, this.startMeters, this.stopMeters);
    }

    public static final Long HOUR = 3600000L;
    public static final Long FOUR_HOUR = HOUR * 3;
    public static final Long EIGHT_HOUR = HOUR * 7;

    public JsObject cost() {
        DateTime now = DateTime.now();
        Double hourlyCost = 0.0;
        Double distanceCost = 0.0;
        Long distanceNow = Await.resultForever(Car.fetchCar(this.carId)).get().vehicle_m - this.startMeters;
        Long duration = (now.toDate().getTime() - this.start.toDate().getTime());
        Long distantInKm = (distanceNow / 1000);
        if (distantInKm < 50) {
            distanceCost = distantInKm * 0.4;
        } else {
            distanceCost = distantInKm * 0.28;
        }
        if (duration < FOUR_HOUR) {
            hourlyCost = ((duration / 6000) * (2.0 / 60.0));
        } else if (duration > FOUR_HOUR && duration < EIGHT_HOUR) {
            hourlyCost = 6.0 + (((duration - FOUR_HOUR) / 6000) * (1.0 / 60.0));
        } else {
            hourlyCost = 10.0 + (((duration - FOUR_HOUR) / 6000) * (0.3 / 60.0));
        }
        return Json.obj(
            $("distanceCost", distanceCost),
            $("hourlyCost", hourlyCost)
        );
    }

    public static final Format<UserSession> FORMAT = new Format<UserSession>() {
        @Override
        public JsResult<UserSession> read(JsValue value) {
            try {
                return JsResult.success(new UserSession(
                    value.field("_id").as(String.class),
                    value.field("uuid").as(String.class),
                    value.field("carId").as(String.class),
                    new DateTime(value.field("start").as(Long.class)),
                    new DateTime(value.field("stop").as(Long.class)),
                    value.field("totalDistance").as(Long.class),
                    value.field("done").as(Boolean.class),
                    value.field("startMeters").as(Long.class),
                    value.field("stopMeters").as(Long.class)
                ));
            } catch (Exception e) {
                e.printStackTrace();
                return JsResult.error(e);
            }
        }

        @Override
        public JsValue write(UserSession value) {
            return Json.obj(
                    $("_id", value._id),
                    $("uuid", value.uuid),
                    $("carId", value.carId),
                    $("start", value.start.toDate().getTime()),
                    $("stop", value.stop.toDate().getTime()),
                    $("totalDistance", value.totalDistance),
                    $("done", value.done),
                    $("startMeters", value.startMeters),
                    $("stopMeters", value.stopMeters)
            );
        }
    };

    public static final MongoStore<UserSession> store = MongoStore.of("sessions", FORMAT);
}
