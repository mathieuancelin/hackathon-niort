package models;

import com.fasterxml.jackson.databind.JsonNode;
import utils.json.Jackson;
import utils.json.JsValue;
import utils.json.Json;
import utils.json.mapping.Format;
import utils.json.mapping.JsResult;
import utils.play.MongoStore;

import java.util.logging.Logger;

import static utils.json.Syntax.$;

/**
 * Created by florianpires on 28/03/15.
 */
public class Parking {

    public final String _id;
    public final String serviceVelo;
    public final Integer capaciteVoiture;
    public final Geo geo;
    public final Location location;
    public final Integer codeCommune;
    public final Integer capaciteVehiculeElectrique;
    public final Integer codePostal;
    public final String telephone;
    public final String moyenPaiment;
    public final String serviceMobiProximite;
    public final String conditionsAcces;
    public final String commune;
    public final String services;
    public final Integer capaciteVelo;
    public final String presentation;
    public final Integer capacitePMR;
    public final String libelleCategorie;
    public final String exploitant;
    public final String siteWeb;
    public final String adresse;
    public final String libelleType;
    public final Integer idObj;

    public Parking(String serviceVelo, Integer capaciteVoiture, Geo geo, Location location, Integer codeCommune, Integer capaciteVehiculeElectrique, Integer codePostal, String telephone, String moyenPaiment, String serviceMobiProximite, String conditionsAcces, String commune, String services, Integer capaciteVelo, String presentation, Integer capacitePMR, String libelleCategorie, String exploitant, String siteWeb, String adresse, String libelleType, Integer idObj) {
        this._id = org.bson.types.ObjectId.get().toString();
        this.serviceVelo = serviceVelo;
        this.capaciteVoiture = capaciteVoiture;
        this.geo = geo;
        this.location = location;
        this.codeCommune = codeCommune;
        this.capaciteVehiculeElectrique = capaciteVehiculeElectrique;
        this.codePostal = codePostal;
        this.telephone = telephone;
        this.moyenPaiment = moyenPaiment;
        this.serviceMobiProximite = serviceMobiProximite;
        this.conditionsAcces = conditionsAcces;
        this.commune = commune;
        this.services = services;
        this.capaciteVelo = capaciteVelo;
        this.presentation = presentation;
        this.capacitePMR = capacitePMR;
        this.libelleCategorie = libelleCategorie;
        this.exploitant = exploitant;
        this.siteWeb = siteWeb;
        this.adresse = adresse;
        this.libelleType = libelleType;
        this.idObj = idObj;
    }

    public Parking(String _id, String serviceVelo, Integer capaciteVoiture, Geo geo, Location location, Integer codeCommune, Integer capaciteVehiculeElectrique, Integer codePostal, String telephone, String moyenPaiment, String serviceMobiProximite, String conditionsAcces, String commune, String services, Integer capaciteVelo, String presentation, Integer capacitePMR, String libelleCategorie, String exploitant, String siteWeb, String adresse, String libelleType, Integer idObj) {
        this._id = _id;
        this.serviceVelo = serviceVelo;
        this.capaciteVoiture = capaciteVoiture;
        this.geo = geo;
        this.location = location;
        this.codeCommune = codeCommune;
        this.capaciteVehiculeElectrique = capaciteVehiculeElectrique;
        this.codePostal = codePostal;
        this.telephone = telephone;
        this.moyenPaiment = moyenPaiment;
        this.serviceMobiProximite = serviceMobiProximite;
        this.conditionsAcces = conditionsAcces;
        this.commune = commune;
        this.services = services;
        this.capaciteVelo = capaciteVelo;
        this.presentation = presentation;
        this.capacitePMR = capacitePMR;
        this.libelleCategorie = libelleCategorie;
        this.exploitant = exploitant;
        this.siteWeb = siteWeb;
        this.adresse = adresse;
        this.libelleType = libelleType;
        this.idObj = idObj;
    }

    public static JsResult<Parking> parse(String json) {
        return PARKING_FORMAT.read(Json.parse(json));
    }

    public static JsResult<Parking> parse(JsValue json) {
        return PARKING_FORMAT.read(json);
    }

    public static JsResult<Parking> parse(JsonNode json) {
        return PARKING_FORMAT.read(Jackson.jsonNodeToJsValue(json));
    }

    public static Format<Parking> PARKING_FORMAT = new Format<Parking>() {
        @Override
        public JsResult<Parking> read(JsValue value) {
            try {
                return JsResult.success(
                        new Parking(
                                value.field("_id").asOpt(String.class).getOrElse(org.bson.types.ObjectId.get().toString()),
                                value.field("SERVICE_VELO").as(String.class),
                                value.field("CAPACITE_VOITURE").as(Integer.class),
                                value.field("geo").as(Geo.GEO_FORMAT),
                                value.field("_l").as(Location.LOC_FORMAT),
                                value.field("CODCOMMUNE").as(Integer.class),
                                value.field("CAPACITE_VEHICULE_ELECTRIQUE").as(Integer.class),
                                value.field("CODE_POSTAL").as(Integer.class),
                                value.field("TELEPHONE").as(String.class),
                                value.field("MOYEN_PAIEMENT").as(String.class),
                                value.field("AUTRES_SERVICE_MOB_PROX").as(String.class),
                                value.field("CONDITIONS_D_ACCES").as(String.class),
                                value.field("COMMUNE").as(String.class),
                                value.field("SERVICES").as(String.class),
                                value.field("CAPACITE_VELO").as(Integer.class),
                                value.field("PRESENTATION").as(String.class),
                                value.field("CAPACITE_PMR").as(Integer.class),
                                value.field("LIBCATEGORIE").as(String.class),
                                value.field("EXPLOITANT").as(String.class),
                                value.field("SITE_WEB").as(String.class),
                                value.field("ADRESSE").as(String.class),
                                value.field("LIBTYPE").asOpt(String.class).getOrElse(""),
                                value.field("_IDOBJ").asOpt(Integer.class).getOrElse(0)
                                )
                );
            } catch (Exception e) {
                e.printStackTrace();
                play.Logger.info(value.pretty());
                return JsResult.error(e);
            }
        }

        @Override
        public JsValue write(Parking value) {
            return Json.obj(
                    $("_id", value._id),
                    $("SERVICE_VELO", value.serviceVelo),
                    $("CAPACITE_VOITURE", value.capaciteVoiture),
                    $("geo", Json.toJson(value.geo, Geo.GEO_FORMAT)),
                    $("_l", Json.toJson(value.location, Location.LOC_FORMAT)),
                    $("CODCOMMUNE", value.codeCommune),
                    $("CAPACITE_VEHICULE_ELECTRIQUE", value.capaciteVehiculeElectrique),
                    $("CODE_POSTAL", value.codePostal),
                    $("TELEPHONE", value.telephone),
                    $("MOYEN_PAIEMENT", value.moyenPaiment),
                    $("AUTRES_SERVICE_MOB_PROX", value.serviceMobiProximite),
                    $("CONDITIONS_D_ACCES", value.conditionsAcces),
                    $("COMMUNE", value.commune),
                    $("SERVICES", value.services),
                    $("CAPACITE_VELO", value.capaciteVoiture),
                    $("PRESENTATION", value.presentation),
                    $("CAPACITE_PMR", value.capacitePMR),
                    $("LIBCATEGORIE", value.libelleCategorie),
                    $("EXPLOITANT", value.exploitant),
                    $("SITE_WEB", value.siteWeb),
                    $("ADRESSE", value.adresse),
                    $("LIBTYPE", value.libelleType),
                    $("_IDOBJ", value.idObj)
                    );
        }
    };

    public static final MongoStore<Parking> store = MongoStore.of("parkings", PARKING_FORMAT);
}
