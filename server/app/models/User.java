package models;

import com.fasterxml.jackson.databind.JsonNode;
import utils.json.Jackson;
import utils.json.JsValue;
import utils.json.Json;
import utils.json.mapping.Format;
import utils.json.mapping.JsResult;
import utils.play.MongoStore;

import static utils.json.Syntax.$;

public class User {

    public final String _id;
    public final String email;
    public final String name;
    public final String surname;
    public final Location location;

    public User(String _id, String email, String name, String surname, Location location) {
        this._id = _id;
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.location = location;
    }

    public User(String email, String name, String surname, Location location) {
        this._id = org.bson.types.ObjectId.get().toString();
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.location = location;
    }

    public User withId(String _id) {
        return new User(_id, this.email, this.name, this.surname, this.location);
    }

    public User withAutoId() {
        return new User(org.bson.types.ObjectId.get().toString(), this.email, this.name, this.surname, this.location);
    }

    public User withEmail(String email) {
        return new User(this._id, email, this.name, this.surname, this.location);
    }

    public User withName(String name) {
        return new User(this._id, this.email, name, this.surname, this.location);
    }

    public User withSurname(String surname) {
        return new User(this._id, this.email, this.name, surname, this.location);
    }

    public User withLocation(Location location) {
        return new User(this._id, this.email, this.name, this.surname, location);
    }

    public JsValue toJson() {
        return USER_FORMAT.write(this);
    }

    public User save() {
        if (store.findById(this._id).isDefined()) {
            return store.update(this._id, this);
        } else {
            return store.insert(this);
        }
    }

    public void delete() {
        store.deleteOne(this._id);
    }

    public static JsResult<User> parse(String json) {
        return USER_FORMAT.read(Json.parse(json));
    }

    public static JsResult<User> parse(JsValue json) {
        return USER_FORMAT.read(json);
    }

    public static JsResult<User> parse(JsonNode json) {
        return USER_FORMAT.read(Jackson.jsonNodeToJsValue(json));
    }

    public static Format<User> USER_FORMAT = new Format<User>() {
        @Override
        public JsResult<User> read(JsValue value) {
            try {
                return JsResult.success(
                    new User(
                        value.field("_id").as(String.class),
                        value.field("email").as(String.class),
                        value.field("name").as(String.class),
                        value.field("surname").as(String.class),
                        value.field("location").as(Location.LOC_FORMAT)
                    )
                );
            } catch (Exception e) {
                return JsResult.error(e);
            }
        }

        @Override
        public JsValue write(User value) {
            return Json.obj(
                $("_id", value._id),
                $("email", value.email),
                $("name", value.name),
                $("surname", value.surname),
                $("location", Json.toJson(value.location, Location.LOC_FORMAT))
            );
        }
    };

    public static final MongoStore<User> store = MongoStore.of("users", USER_FORMAT);
}
