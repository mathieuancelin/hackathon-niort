package services;

import play.Logger;
import play.libs.ws.WS;
import utils.concurrent.Future;
import utils.functional.Option;
import utils.json.JsObject;
import utils.json.JsValue;
import utils.json.Json;

import static utils.json.Syntax.$;

public class CarServiceClient {

    public static final String BOX_URL = "http://hackathon.koolicar.com/api/v1/box/%s/";
    public static final String ACTION_URL = "http://hackathon.koolicar.com/api/v1/box/%s/box_request";
    public static final String REQUEST_URL = "http://hackathon.koolicar.com/api/v1/box/%s/box_request/%s";

    public static final String TOKEN = "DEV_TOKEN";

    public static Future<Option<JsObject>> fetchBoxData(String id) {
        return Future.toFuture(WS.url(String.format(BOX_URL, id)).setHeader("Authorization", "Token token=\"" + TOKEN + "\"").get())
                .map(r -> {
                    JsValue val = Json.fromJsonNode(r.asJson()).as(JsObject.class).field("box");
                    return val.asOpt(JsObject.class);
                })
                .fallbackTo(Future.failed(new RuntimeException("No car with id " + id)));
    }

    public static Future<String> carAction(String id, String action) {
        Logger.info("Calling " + action + " on " + action);
        return Future.toFuture(WS.url(String.format(ACTION_URL, id))
                .setHeader("Authorization", "Token token=\"" + TOKEN + "\"")
                .setQueryParameter("box_method", action)
                .post(Json.obj($("box_method", action)).asJackson()))
                .map(r -> Json.fromJsonNode(r.asJson()).field("box_request_id").as(String.class));
    }

    public static Future<JsObject> requestStatus(String id, String requestId) {
        return Future.toFuture(WS.url(String.format(REQUEST_URL, id, requestId)).setHeader("Authorization", "Token token=\"" + TOKEN + "\"").get())
                .map(r -> {
                    return Json.fromJsonNode(r.asJson()).field("box_request").as(JsObject.class);
                });
    }

    public static Future<JsObject> deleteRequest(String id, String requestId) {
        return Future.toFuture(WS.url(String.format(REQUEST_URL, id, requestId)).setHeader("Authorization", "Token token=\"" + TOKEN + "\"").delete())
                .map(r -> Json.fromJsonNode(r.asJson()).as(JsObject.class));
    }
}
