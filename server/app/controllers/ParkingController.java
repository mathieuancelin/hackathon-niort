package controllers;

import jdk.nashorn.api.scripting.JSObject;
import models.Parking;
import play.api.libs.Files;
import play.mvc.Controller;
import play.mvc.Result;
import utils.collection.Container;
import utils.json.JsObject;
import utils.json.JsValue;
import utils.json.Json;
import utils.json.mapping.DefaultReaders;
import utils.json.mapping.DefaultWriters;
import utils.json.mapping.JsResult;
import utils.json.mapping.Reader;

import static utils.play.ControllerHelper.jsonOk;
import static utils.json.Syntax.*;


import java.util.List;

/**
 * Created by florianpires on 28/03/15.
 */
public class ParkingController extends Controller {

    public static Result index() {
        return ok("Hi !");
    }

    public static Result getNearParkings(Double lng, Double lat, Double dmax) {
        JsObject json = Json.obj(
                $("_l", Json.obj(
                        $("$near", Json.arr(lng, lat)),
                        $("$maxDistance", dmax / 111120)
                ))
        );

//        return ok(" lng : " + lng + " lat : " + lat + " distancemax : " + dmax );
        Container<Parking> parkings = Parking.store.find(json);
        JsValue value = Json.toJson(parkings.toLinkedList(), DefaultWriters.seq(Parking.PARKING_FORMAT));
        return jsonOk(value);
    }

    public static Result getNearParkingsCount(Double lng, Double lat, Double dmax) {
        JsObject json = Json.obj(
                $("_l", Json.obj(
                        $("$near", Json.arr(lng, lat)),
                        $("$maxDistance", dmax / 111120)
                ))
        );

//        return ok(" lng : " + lng + " lat : " + lat + " distancemax : " + dmax );
        Container<Parking> parkings = Parking.store.find(json);
        return jsonOk(Json.obj($("count", parkings.size())));
    }

}
