package controllers;

import com.google.common.base.Function;
import models.Car;
import play.libs.F;
import play.mvc.Controller;
import play.mvc.Result;
import services.CarServiceClient;
import utils.concurrent.Future;
import utils.functional.Option;
import utils.json.Json;
import utils.play.ControllerHelper;

import static utils.play.ControllerHelper.jsonNotFound;
import static utils.play.ControllerHelper.jsonOk;

public class BoxController extends Controller {

    public static F.Promise<Result> car(String id) {
        // TODO : cache
        return CarServiceClient.fetchBoxData(id).map(opt -> {
            if (opt.isDefined()) {
                return jsonOk(opt.get());
            } else {
                return notFound();
            }
        }).toPlayPromise();
    }

    public static F.Promise<Result> lockCar(String id) {
        return Car.fetchCar(id).flatMap(new Function<Option<Car>, Future<Result>>() {
            @Override
            public Future<Result> apply(Option<Car> opt) {
                if (opt.isDefined()) return opt.get().toggleClose().map(ControllerHelper::jsonOk);
                return Future.<Result>successful(jsonNotFound(Json.obj()));
            }
        }).toPlayPromise();
    }

    public static F.Promise<Result> unlockCar(String id) {
        return lockCar(id);
    }

    public static F.Promise<Result> monitorRequest(String id, String rid) {
        return CarServiceClient.requestStatus(id, rid).map(ControllerHelper::jsonOk).toPlayPromise();
    }
}
