package controllers;

import play.Play;
import play.mvc.Controller;
import play.mvc.Result;
import utils.json.JsObject;
import utils.json.Json;

import java.nio.file.Files;

import static utils.play.ControllerHelper.jsonOk;

public class CarController extends Controller {

    private static JsObject car(String id) {
        String obj = "{\n" +
            "    \"id\": \"" + id +"\" ,\n" +
            "    \"brand\": \"Renault\",\n" +
            "    \"vehicle_model\": \"Zoé\",\n" +
            "    \"year\": 2014,\n" +
            "    \"category\": \"Electrique\",\n" +
            "    \"doors_count\": 3,\n" +
            "    \"places_count\": 4,\n" +
            "    \"fuel_type_cd\": 2,\n" +
            "    \"gears_type_cd\": 0,\n" +
            "    \"longitude\": -1.62709,\n" +
            "    \"latitude\": 48.20746,\n" +
            "    \"description\": \"Véhicule électrique\",\n" +
            "    \"options\": [\n" +
            "      8,\n" +
            "      9,\n" +
            "      24,\n" +
            "      27,\n" +
            "      29\n" +
            "    ],\n" +
            "    \"pictures_url\": [\n" +
            "      \"/uploads/photo/203/medium_IMG_20131003_112055_940.jpg\",\n" +
            "      \"/uploads/photo/204/medium_IMG_20131003_112158_371.jpg\"\n" +
            "    ],\n" +
            "    \"can_drive\": true\n" +
            "  }";
        return Json.parse(obj).as(JsObject.class);
    }

    public static Result carDetails(String id) {
        return jsonOk(car(id));
    }

    public static Result images(String file) throws Exception {
        return ok(Files.readAllBytes(Play.application().getFile("public/images/zoe.jpg").toPath())).as("image/jpeg");
    }
}
