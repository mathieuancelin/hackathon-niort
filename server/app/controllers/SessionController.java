package controllers;

import models.Car;
import models.UserSession;
import org.joda.time.DateTime;
import play.mvc.Controller;
import play.mvc.Result;
import utils.concurrent.Await;
import utils.functional.Option;
import utils.json.JsObject;
import utils.json.Json;

import static utils.json.Syntax.$;
import static utils.play.ControllerHelper.jsonBadRequest;
import static utils.play.ControllerHelper.jsonNotFound;
import static utils.play.ControllerHelper.jsonOk;


public class SessionController extends Controller {

    public static Result startSession(String uuid, String carId) {
        Option<UserSession> opt = UserSession.store.findOne(Json.obj($("uuid", uuid)));
        Long distanceNow = Await.resultForever(Car.fetchCar(carId)).get().vehicle_m;
        UserSession session = opt.getOrElse(new UserSession(uuid, carId, DateTime.now(), DateTime.now(), 0L, false, distanceNow, distanceNow));
        session = session.save();
        JsObject payload = session.toJson().as(JsObject.class);
        return jsonOk(payload.add(Json.obj($("cost", session.cost()))).add(Json.obj($("distance", 0))));
    }

    public static Result getSession(String uuid) {
        Option<UserSession> opt = UserSession.store.findOne(Json.obj($("uuid", uuid)));
        if (opt.isEmpty()) {
            return jsonNotFound(Json.obj($("uuid", uuid)));
        }
        UserSession session = opt.get();
        JsObject payload = opt.get().toJson().as(JsObject.class);
        Long distanceNow = Await.resultForever(Car.fetchCar(session.carId)).get().vehicle_m;
        Long run = distanceNow - session.startMeters;
        return jsonOk(payload.add(Json.obj($("cost", opt.get().cost()))).add(Json.obj($("distance", run))));
    }

    public static Result endSession(String uuid) {
        Option<UserSession> opt = UserSession.store.findOne(Json.obj($("uuid", uuid)));
        if (opt.isEmpty()) {
            return jsonNotFound(Json.obj($("uuid", uuid)));
        }
        UserSession session = opt.get();
        if (session.done) {
            return jsonBadRequest(Json.obj($("message", "Session with id : " + uuid + " already ended")));
        }
        Long distanceNow = Await.resultForever(Car.fetchCar(session.carId)).get().vehicle_m;
        Long run = distanceNow - session.startMeters;
        session = session.withStop(DateTime.now()).withTotalDistance(run).withDone(true).withStopMeters(distanceNow).save();
        // TODO : call whatever we need to call
        JsObject payload = session.toJson().as(JsObject.class);
        return jsonOk(payload.add(Json.obj($("cost", opt.get().cost()))).add(Json.obj($("distance", run))));
    }
}
