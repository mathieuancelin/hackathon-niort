package controllers;

import models.User;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import utils.json.JsError;
import utils.json.Json;
import utils.json.mapping.JsResult;
import utils.json.mapping.JsSuccess;
import views.html.index;

import static utils.json.Syntax.$;
import static utils.play.ControllerHelper.jsonInternalServerError;
import static utils.play.ControllerHelper.jsonOk;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }

    public static Result indexJson() {
        return jsonOk(Json.obj($("message", "Hello World!")));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result createUser() {
        JsResult<User> result = User.parse(request().body().asJson());
        for (JsError<User> error : result.asError()) {
            return jsonInternalServerError(Json.obj($("message", "Bad user format"), $("error", error.errors())));
        }
        for (JsSuccess<User> success : result.asSuccess()) {
            return jsonOk(success.get().withAutoId().save().toJson());
        }
        return jsonInternalServerError(Json.obj($("message", "Not possible")));
    }

    public static Result users() {
        return jsonOk(User.store.findAll().asJsArray(User.USER_FORMAT));
    }

    public static Result usersNextToMe() {
        //Env.userIndex.search(Json.obj(
        //    $("query", Json.obj(
        //        $("filtered", Json.obj(
        //            $("query", Json.obj(
        //                $("match_all", Json.obj())
        //            ))
        //        ))
        //    )),
        //    $("filter", Json.obj(
        //        $("geo_distance", Json.obj(
        //            $("distance", "2km"),
        //            $("location", Json.obj(
        //                $("lat", -73.99171),
        //                $("lon", 40.738868)
        //            ))
        //        ))
        //    ))
        //));
        return jsonOk(User.store.find(
            Json.obj(
                $("location", Json.obj(
                    $("$near", Json.arr(-73.99171, 40.738868)),
                    $("$maxDistance", 0.01)
                ))
            )
        ).asJsArray(User.USER_FORMAT));
    }
}
