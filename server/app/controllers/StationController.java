package controllers;

import models.*;
import play.Logger;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import utils.collection.Container;
import utils.json.JsError;
import utils.json.JsObject;
import utils.json.JsValue;
import utils.json.Json;
import utils.json.mapping.DefaultWriters;
import utils.json.mapping.JsResult;
import utils.json.mapping.JsSuccess;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static utils.json.Syntax.$;
import static utils.play.ControllerHelper.jsonInternalServerError;
import static utils.play.ControllerHelper.jsonOk;

/**
 * Created by florianpires on 28/03/15.
 */
public class StationController extends Controller {

    public static Result all() {
        Location location = new Location(new BigDecimal(10), new BigDecimal(50));
        return jsonOk(Location.LOC_FORMAT.write(location));
    }

    public static Result test() {
        Location locationPerson = null;
        Location locationCar = null;
        Double distance = 10.0;

        List<Station> stationsNearPersonne = Station.near(locationPerson, distance).toList();
        List<Station> stationsNearCar = Station.near(locationCar, distance).toList();
        for (Station s1 : stationsNearPersonne) {
            for (Station s2 : stationsNearCar) {
                if (s1.lines.equals(s2.lines)) {
                    return jsonOk(Station.FORMAT.write(s1));
                }
            }
        }

        return jsonOk(Json.obj($("t", "test")));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result nearStation() {
        JsObject positions = Json.fromJsonNode(request().body().asJson()).as(JsObject.class);
        JsResult<Location> locationPerson = Location.OBJ_READER.read(positions.field("start"));
        JsResult<Location> locationCar = Location.OBJ_READER.read(positions.field("end"));

        for (JsError<Location> error : locationPerson.asError()) {
            return jsonInternalServerError(Json.obj($("message", "Bad location format"), $("error", error.errors())));
        }
        for (JsSuccess<Location> success : locationPerson.asSuccess()) {
            List<Station> stationsPerson = Station.near(locationPerson.get(), 1000000d).toLinkedList();
            List<Station> stationCar = Station.near(locationCar.get(), 1000000d).toLinkedList();

            List<Step> steps = new LinkedList<>();

            Station s1 = stationsPerson.get(0);
            Station s2 = stationCar.get(0);

            Step stepStart = new Step("PIED", "A pied", "2 min", "Départ à pied", "jusqu'à la station " + s1.name);
            Step stepEnd = new Step("PIED", "A pied", "3 min", "Départ de la station " + s2.name, "jusqu'à votre voiture KooliCar.");


            steps.add(stepStart);

            for (String line1 : s1.lines) {
                for (String line2 : s2.lines) {
                    if (line1.equals(line2)) {
                        Step step = new Step(s1.type, "En Bus", "23 min", "De la station " + s1.name, "aller jusqu'à la station " + s2.name);
                        steps.add(step);
                    }
                }
            }

            steps.add(stepEnd);

            JsValue value = Json.toJson(steps, DefaultWriters.seq(Step.STEP_FORMAT));
            return jsonOk(value);
        }

        // Create step
        return jsonInternalServerError(Json.obj($("message", "Not possible")));
    }

//    /locationPersonne/locationVoiture

    // Chercher toutes les stations a proximité (near grosse taille)
    // Extraite otut les noms d elignes
    // Pour chaque ligne near d'une station du point de depart et d'arrivé


//    Set<String> lines = ...
//
//            for (String line : lines) {
//        List<Station> s1 = near(loc1, line);
//        List<Station> s2 = near(loc2, line);
//    }


}
