const React = require('react/addons');
const _ = require('lodash');
const $ = require('jquery');

setTimeout(() => console.log('Hello World!'), 100);

exports.jquery = $;
exports.lodash = _;
exports.React = React;

// bootstrap jquery exposition
window.$ = $;
window.jQuery = $;

// TODO basic component