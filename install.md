Liste des paquets à installer
==========================


App Serveur
-----------------

* JDK 8 (http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* Activator 1.3.2 (https://typesafe.com/get-started)
* node js + npm (https://nodejs.org/)

Data stores
----------------------

* Elasticsearch 1.4.x (https://www.elastic.co/products/elasticsearch)
* MongoDB 2.6.x (http://www.mongodb.org/)


Démarrage de l'app serveur
--------------------

dans le dossier server/ui

```
npm install
npm run watch
````

dans le dossier server

```
activator

$ ~run
```

