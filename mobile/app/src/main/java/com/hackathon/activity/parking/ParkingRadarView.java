package com.hackathon.activity.parking;

import android.graphics.Color;
import android.location.Location;

import com.hackathon.ar3.RadarView;
import com.hackathon.ar3.utils.PaintUtils;


public class ParkingRadarView extends RadarView {
	/** The screen */
	public ParkingDataView view;
	/** The radar's range */
	float range;
	/** Radius in pixel on screen */
	public static float RADIUS = 40;
	public static float RADIUSDPI = 40;
	/** Position on screen */
	static float originX = 0 , originY = 0;

	/**
	 * You can change the radar color from here.
	 *   */
	static int radarColor = Color.argb(100, 220, 0, 0);
	Location currentLocation = new Location("provider");
	Location destinedLocation = new Location("provider");

	/*
	 * pass the same set of coordinates to plot POI's on radar
	 * */
	// SF Art Commission, SF Dept. of Public Health, SF Ethics Comm, SF Conservatory of Music, All Star Cafe, Magic Curry Cart, SF SEO Marketing, SF Honda,
	// SF Mun Transport Agency, SF Parking Citation, Mayors Office of Housing, SF Redev Agency, Catario Patrice, Bank of America , SF Retirement System, Bank of America Mortage,
	// Writers Corp., Van Nes Keno Mkt.

	double[] latitudes = new double[] {46.115095};
	double[] longitudes = new double[] {-0.345130};

	public float[][] coordinateArray = new float[latitudes.length][2];

	float angleToShift;
	public float degreetopixel;
	public float bearing;
	public float circleOriginX;
	public float circleOriginY;
	private float mscale;


	public float x = 0;
	public float y = 0;
	public float z = 0;

	float  yaw = 0;
	double[] bearings;
	RechercheParking arView = new RechercheParking();
	public ParkingRadarView(ParkingDataView parkingDataView, double[] bearings){
		super();
		this.bearings = bearings;
		calculateMetrics();
	}
	
	public void calculateMetrics(){

		RADIUSDPI=arView.convertToPix((int)RADIUS);
		circleOriginX = originX + RADIUSDPI;
		circleOriginY = originY + RADIUSDPI;

		range = (float)arView.convertToPix(10) * 50;
		mscale = range / RADIUSDPI;//arView.convertToPix((int)RADIUS);
	}
	
	public void paint(PaintUtils dw, float yaw) {
		
//		circleOriginX = originX + RADIUS;
//		circleOriginY = originY + RADIUS;
		this.yaw = yaw;
//		range = arView.convertToPix(10) * 1000;		/** Draw the radar */
		dw.setFill(true);
		dw.setColor(radarColor);
		dw.paintCircle(circleOriginX,circleOriginY, RADIUSDPI);

		/** put the markers in it */
//		float scale = range / arView.convertToPix((int)RADIUS);
		/**
		 *  Your current location coordinate here.
		 * */

		currentLocation.setLatitude(46.108411);
		currentLocation.setLongitude( -0.344889);

		for(int i = 0; i <latitudes.length;i++){
			destinedLocation.setLatitude(latitudes[i]);
			destinedLocation.setLongitude(longitudes[i]);
			convLocToVec(currentLocation, destinedLocation);
			float x = this.x / mscale;
			float y = this.z / mscale;

			//float y = this.y / mscale;
			
		//	if (x * x + y * y < RADIUSDPI * RADIUSDPI) {
				dw.setFill(true);
				dw.setColor(Color.rgb(255, 255, 255));
			//x y 165  2198

//					12.5
			//120
			//dw.paintText(RADIUSDPI, RADIUSDPI, String.valueOf(this.y));
				dw.paintRect(x + RADIUSDPI, y +RADIUSDPI, arView.convertToPix(2), arView.convertToPix(2));
		//	}
		}
	}


	
	/** Width on screen */
	public float getWidth() {
		return   RADIUSDPI * 2;
	}

	/** Height on screen */
	public float getHeight() {
		return RADIUSDPI* 2;
	}
	
	
	public void set(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public void convLocToVec(Location source, Location destination) {
		float[] z = new float[1];
		z[0] = 0;
		Location.distanceBetween(source.getLatitude(), source.getLongitude(), destination
				.getLatitude(), source.getLongitude(), z);
		float[] x = new float[1];
		Location.distanceBetween(source.getLatitude(), source.getLongitude(), source
				.getLatitude(), destination.getLongitude(), x);
		if (source.getLatitude() < destination.getLatitude())
			z[0] *= -1;
		if (source.getLongitude() > destination.getLongitude())
			x[0] *= -1;

		set(x[0], (float) 0, z[0]);
	}
}