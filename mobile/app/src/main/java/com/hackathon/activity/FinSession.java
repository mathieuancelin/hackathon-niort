package com.hackathon.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.hackathon.Application;
import com.hackathon.R;
import com.hackathon.service.session.Session;
import com.hackathon.service.session.SessionClient;

import java.math.BigDecimal;
import java.math.RoundingMode;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Gaetan on 28/03/2015.
 */
public class FinSession extends Activity {

    @InjectView(R.id.distance)
    TextView distance;

    @InjectView(R.id.price)
    TextView priceView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fin_session);

        ButterKnife.inject(this);

        SessionClient.get().endSession(Application.deviceUUID, new Callback<Session>() {
            @Override
            public void success(Session session, Response response) {
                distance.setText("Vous avez parcouru " + (session.distance / 1000) + " Km");
                BigDecimal price = BigDecimal.valueOf(session.getCost().getDistanceCost() + session.getCost().getHourlyCost()).setScale(2, RoundingMode.HALF_EVEN);
                priceView.setText("Vous serez facturé de " + price.toString() + " €");
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
