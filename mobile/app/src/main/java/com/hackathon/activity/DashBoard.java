package com.hackathon.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

import com.hackathon.Application;
import com.hackathon.R;
import com.hackathon.service.session.Session;
import com.hackathon.service.session.SessionClient;

import java.math.BigDecimal;
import java.math.RoundingMode;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Gaetan on 28/03/2015.
 */
public class DashBoard extends Activity {

    @InjectView(R.id.btn_parking)
    ImageView btnParking;
    @InjectView(R.id.btn_sos)
    ImageView btnSos;
    @InjectView(R.id.voyage_over)
    ImageView btnVoyageOver;

    @InjectView(R.id.dashboard_km)
    TextView dashboard_km;

    @InjectView(R.id.dashboard_prix)
    TextView dashboard_prix;

    Handler handler = new Handler();

    private Boolean dead = false;

    private void updateDashboard() {
        SessionClient.get().createSession(Application.deviceUUID, Application.CARUUID, new Callback<Session>() {
            @Override
            public void success(Session session, Response response) {
                dashboard_km.setText((session.distance / 1000) + " Km");
                BigDecimal price = BigDecimal.valueOf(session.getCost().getDistanceCost() + session.getCost().getHourlyCost()).setScale(2, RoundingMode.HALF_EVEN);
                dashboard_prix.setText(price.toString() );
                if (!dead)
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            updateDashboard();
                        }
                    }, 10000);
            }

            @Override
            public void failure(RetrofitError error) {
                if (!dead)
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            updateDashboard();
                        }
                    }, 10000);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        dead = false;
        updateDashboard();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dead = true;
    }

    @OnClick(R.id.btn_parking)
    public void onParking() {

        Intent intent = new Intent(this, com.hackathon.activity.RechercheParking.class);

        startActivity(intent);

    }

    @OnClick(R.id.btn_sos)
    public void onSos() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:0172865900"));
        startActivity(callIntent);
    }


    @OnClick(R.id.voyage_over)
    public void onOver() {
        Intent intent = new Intent(this, FinSession.class);

        startActivity(intent);

    }
}
