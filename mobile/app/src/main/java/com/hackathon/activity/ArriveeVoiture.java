package com.hackathon.activity;

import android.app.Activity;
import android.content.Intent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hackathon.Application;
import com.hackathon.R;
import com.hackathon.service.koolicar.KoolClient;
import com.hackathon.service.koolicar.MonitorId;

import org.w3c.dom.Text;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Gaetan on 28/03/2015.
 */
public class ArriveeVoiture extends Activity {
    private static final int THRESHOLD = 0;
    OrientationEventListener orientationListener;

    @InjectView(R.id.trouver_kooli)
    LinearLayout trouver;

    @InjectView(R.id.first_line)

    TextView txt_first;

    @InjectView(R.id.kooli_lock)
    ImageView kooliLock;
    @InjectView(R.id.kooli_unlock)
    ImageView kooliUnLock;


    @InjectView(R.id.second_line)
    TextView txt_second;
    @Override


    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arrivee_voiture);
        ButterKnife.inject(this);
        // StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
        //           .permitAll().build();
        //  StrictMode.setThreadPolicy(policy);
        final Activity activity = this;
        orientationListener = new OrientationEventListener(activity, SensorManager.SENSOR_DELAY_UI) {
            @Override
            public void onOrientationChanged(int orientation) {
             //   System.out.println(orientation);
                if (!isPortrait(orientation)) {


                    this.disable();
              //      System.out.println("landscape");
                    Intent intent = new Intent(activity, RechercheVoiture.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                   // startActivity(intent);

                  //  activity.finish();
                }
            }

        };
    }

    ;
    private static final ScheduledExecutorService worker =
            Executors.newSingleThreadScheduledExecutor();
    @Override
    public void onResume() {
        super.onResume();
        Bundle extras = getIntent().getExtras();

        if (extras == null ||(extras != null && extras.getBoolean("EXTRA"))) {
            orientationListener.enable();
        }
      if(extras != null && extras.getBoolean("EXTRA")){

            txt_first.setText("Koolicar en cours d'ouverture...");
            txt_second.setText("");

          final ArriveeVoiture activity=this;

          KoolClient.get().unlock(Application.CARUUID, new Callback<MonitorId>() {
              @Override
              public void success(MonitorId monitorId, Response response) {
                  monitorId = monitorId;
                  //       result.setText(monitorId.getRid());
                  final Handler handler = new Handler();
                  handler.postDelayed(new Runnable() {
                      @Override
                      public void run() {
                          activity.txt_first.setText("votre Koolicar est ouverte...");
                          activity.kooliLock.setVisibility(View.GONE);
                          activity.kooliUnLock.setVisibility(View.VISIBLE);
                          final Handler handler = new Handler();
                          handler.postDelayed(new Runnable() {
                              @Override
                              public void run() {
                                  Intent intent = new Intent(activity, DashBoard.class);
                                  intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                  // intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                  startActivity(intent);
                              }
                              }
                          , 3000);
                      }
                  }, 1000);


              }

              @Override
              public void failure(RetrofitError error) {
                  System.out.println(error);
              }
          });


      }


    }
    


    @Override
    public void onPause() {
        super.onPause();
        orientationListener.disable();
    }

    boolean retour= false;
    @OnClick(R.id.trouver_kooli)
    public void recherVoiture(LinearLayout ll){
        Intent intent = new Intent(this, RechercheVoiture.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
        retour=true;
    }
    private boolean isPortrait(int orientation) {
        return orientation >= 320 || orientation <= 45;
    }

    //private boolean isPortrait(int orientation){
    //   return (orientation >= (360 - THRESHOLD) && orientation <= 360) || (orientation >= 0 && orientation <= THRESHOLD);
    //}


    MonitorId monitorId;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }

    public void lock(Button button) {

        KoolClient.get().lock(Application.CARUUID, new Callback<MonitorId>() {
            @Override
            public void success(MonitorId monitorId, Response response) {
                monitorId = monitorId;
                //    result.setText(monitorId.getRid());

            }

            @Override
            public void failure(RetrofitError error) {
                System.out.println(error);
            }
        });

    }

    //@OnClick(R.id.kooli_unlock)
    public void unlock(Button button) {

        KoolClient.get().unlock(Application.CARUUID, new Callback<MonitorId>() {
            @Override
            public void success(MonitorId monitorId, Response response) {
                monitorId = monitorId;
                //       result.setText(monitorId.getRid());

            }

            @Override
            public void failure(RetrofitError error) {
                System.out.println(error);
            }
        });

    }

}
