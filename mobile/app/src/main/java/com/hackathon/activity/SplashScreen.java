package com.hackathon.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.hackathon.R;
import com.hackathon.ar3.ARView;

/**
 * Created by Gaetan on 28/03/2015.
 */
public class SplashScreen extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {
                                Intent i = new Intent(SplashScreen.this, ListeLocation.class);
                //Intent i = new Intent(SplashScreen.this, ARView.class);
               // Intent i = new Intent(SplashScreen.this, ArriveeVoiture.class);

                startActivity(i);

                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
