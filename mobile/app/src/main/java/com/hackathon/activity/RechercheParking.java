package com.hackathon.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.AbsListView;

import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.hackathon.R;
import com.hackathon.service.koolicar.Koolicar;
import com.hackathon.service.koolicar.Reservation;
import com.hackathon.service.parking.Parking;
import com.hackathon.service.parking.ParkingClient;
import com.hackathon.view.ParkingAdapter;
import com.hackathon.view.ReservationAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Gaetan on 29/03/2015.
 */
public class RechercheParking  extends BaseActivity implements ObservableScrollViewCallbacks {

    private static final String TAG = RechercheParking.class.getSimpleName();
    private List<Parking> liste = new ArrayList<Parking>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_parking);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        ((Toolbar) findViewById(R.id.toolbar)).setTitle(R.string.titre_parking);
        setTitle(R.string.titre_parking);
        ObservableListView listView = (ObservableListView) findViewById(R.id.list);
        listView.setScrollViewCallbacks(this);



        final ParkingAdapter adapter = new ParkingAdapter(this, liste);
        listView.setAdapter(adapter);


        // ObservableListView uses setOnScrollListener, but it still works.
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                Log.v(TAG, "onScrollStateChanged: " + scrollState);
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Log.v(TAG, "onScroll: firstVisibleItem: " + firstVisibleItem + " visibleItemCount: " + visibleItemCount + " totalItemCount: " + totalItemCount);
            }
        });

        ParkingClient.get().getNearParkings(-1.57010, 47.25310, 5000, new Callback<List<Parking>>() {
            @Override
            public void success(List<Parking> parkings, Response response) {
                adapter.clear();
                for (Parking parking : parkings) {
                    adapter.add(parking);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        ActionBar ab = getSupportActionBar();
        if (scrollState == ScrollState.UP) {
            if (ab.isShowing()) {
                ab.hide();
            }
        } else if (scrollState == ScrollState.DOWN) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
    }
}
