package com.hackathon.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.hackathon.R;
import com.hackathon.service.koolicar.Koolicar;
import com.hackathon.service.koolicar.Reservation;
import com.hackathon.service.station.Location;
import com.hackathon.service.station.Locations;
import com.hackathon.service.station.StationClient;
import com.hackathon.service.station.Step;
import com.hackathon.service.station.Steps;
import com.hackathon.view.ReservationAdapter;
import com.hackathon.view.TrajetAdapter;
import com.nineoldandroids.view.ViewHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Gaetan on 28/03/2015.
 */
public class Trajet extends BaseActivity implements ObservableScrollViewCallbacks {

    private static final String TAG = ListeLocation.class.getSimpleName();
    private List<Step> liste = new ArrayList<Step>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trajet);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        ((Toolbar) findViewById(R.id.toolbar)).setTitle(R.string.titre_reservation);
        setTitle(R.string.titre_trajet);
        ObservableListView listView = (ObservableListView) findViewById(R.id.list);

        listView.setScrollViewCallbacks(this);
        final TrajetAdapter adapter = new TrajetAdapter(this, liste);
        listView.setAdapter(adapter);
        // ObservableListView uses setOnScrollListener, but it still works.
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                Log.v(TAG, "onScrollStateChanged: " + scrollState);
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Log.v(TAG, "onScroll: firstVisibleItem: " + firstVisibleItem + " visibleItemCount: " + visibleItemCount + " totalItemCount: " + totalItemCount);
            }
        });

        Log.i(TAG, "0 - loading stations ....");
        // TODO : call with nice coords
        StationClient.get().steps(new Locations(new Location(47.21837, -1.55362), new Location(47.25310, -1.57010)), new Callback<List<Step>>() {
            @Override
            public void success(List<Step> steps, Response response) {
                Log.i(TAG, "1 - Done loading stations !!!!!!");
                adapter.clear();
                for (Step step : steps) {
                    String time = step.getTimeLeft();
                    step.setTimeLeft("");
                    adapter.add(step);
                    Step fake = new Step();
                    fake.setTimeLeft(time);
                    fake.setEndPoint("");
                    fake.setStartPoint("");
                    fake.setLibType("TRAJET");
                    fake.setType("TRAJET");
                    adapter.add(fake);
                }
                adapter.notifyDataSetChanged();
                Log.i(TAG, "2 - Done loading stations !!!!!!");
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        ActionBar ab = getSupportActionBar();
        if (scrollState == ScrollState.UP) {
            if (ab.isShowing()) {
                ab.hide();
            }
        } else if (scrollState == ScrollState.DOWN) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
    }
}
