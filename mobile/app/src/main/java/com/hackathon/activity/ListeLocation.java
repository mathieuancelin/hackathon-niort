package com.hackathon.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.hackathon.R;
import com.hackathon.service.koolicar.Koolicar;
import com.hackathon.service.koolicar.Reservation;
import com.hackathon.view.ReservationAdapter;
import com.nineoldandroids.view.ViewHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Gaetan on 28/03/2015.
 */
public class ListeLocation extends BaseActivity implements ObservableScrollViewCallbacks {

    private static final String TAG = ListeLocation.class.getSimpleName();
    private List<Reservation> liste = new ArrayList<Reservation>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_location);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        ((Toolbar) findViewById(R.id.toolbar)).setTitle(R.string.titre_reservation);
        setTitle(R.string.titre_reservation);
        ObservableListView listView = (ObservableListView) findViewById(R.id.list);
        listView.setScrollViewCallbacks(this);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        try {
            Reservation reservation = new Reservation();
            reservation.setDatedebut(simpleDateFormat.parse("28/03/2015 18:00"));
            reservation.setDatefin(simpleDateFormat.parse("28/03/2015 19:30"));

            Koolicar car = new Koolicar();
            car.setName("Zoe");
            reservation.setCar(car);


            car.setRessourceId(R.drawable.zoe);
            liste.add(reservation);
            Reservation reservation2 = new Reservation();

            simpleDateFormat.parse("28/03/2015 18:00");

            reservation2.setDatedebut(simpleDateFormat.parse("31/03/2015 15:00"));
            reservation2.setDatefin(simpleDateFormat.parse("31/03/2015 17:00"));
            Koolicar car1 = new Koolicar();
            car1.setName("Scenic 3");

            car1.setRessourceId(R.drawable.scenic3);
            reservation2.setCar(car1);
            liste.add(reservation2);

            Reservation reservation3 = new Reservation();
            reservation3.setDatedebut(simpleDateFormat.parse("01/04/2015 10:00"));
            reservation3.setDatefin(simpleDateFormat.parse("01/04/2015 10:45"));
        Koolicar car3 = new Koolicar();
        car3.setName("Golf");
            car3.setRessourceId(R.drawable.golf);
        reservation3.setCar(car3);
        liste.add(reservation3);
        } catch (Exception e) {
            e.printStackTrace();
        }


        final ReservationAdapter adapter = new ReservationAdapter(this, liste);
        listView.setAdapter(adapter);


        // ObservableListView uses setOnScrollListener, but it still works.
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                Log.v(TAG, "onScrollStateChanged: " + scrollState);
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Log.v(TAG, "onScroll: firstVisibleItem: " + firstVisibleItem + " visibleItemCount: " + visibleItemCount + " totalItemCount: " + totalItemCount);
            }
        });
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        ActionBar ab = getSupportActionBar();
        if (scrollState == ScrollState.UP) {
            if (ab.isShowing()) {
                ab.hide();
            }
        } else if (scrollState == ScrollState.DOWN) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
    }
}
