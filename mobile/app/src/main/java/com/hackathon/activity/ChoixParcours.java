package com.hackathon.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.hackathon.Application;
import com.hackathon.R;
import com.hackathon.service.car.Car;
import com.hackathon.service.car.CarDetailsClient;
import com.hackathon.service.koolicar.KoolClient;
import com.hackathon.service.koolicar.Koolicar;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Gaetan on 28/03/2015.
 */
public class ChoixParcours extends BaseActivity implements ObservableScrollViewCallbacks {

    @Override
    public void onScrollChanged(int i, boolean b, boolean b1) {

    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    @InjectView(R.id.toolbar)
    Toolbar mToolbar;
    @InjectView(R.id.bus)
    LinearLayout mBus;
    @InjectView(R.id.train)
    LinearLayout mTrain;
    @InjectView(R.id.pied)
    LinearLayout mPied;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choix_trajet);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        //  mFlexibleSpaceImageHeight = getResources().getDimensionPixelSize(R.dimen.flexible_space_image_height);
        //  mFlexibleSpaceShowFabOffset = getResources().getDimensionPixelSize(R.dimen.flexible_space_show_fab_offset);
        //   mActionBarSize = getActionBarSize();
        // mToolbarColor = getResources().getColor(R.color.primary);

        //   mToolbar = (Toolbar) findViewById(R.id.toolbar);
        // mToolbar.setNavigationIcon(R.drawable.ic_place_black_48dp);
        ButterKnife.inject(this);

        mToolbar.setTitle(getString(R.string.choix_trajet));
        setTitle(getString(R.string.choix_trajet));


        CarDetailsClient.get().details("246", new Callback<Car>() {
            @Override
            public void success(Car car, Response response) {
                mToolbar.setSubtitle(car.getBrand() + " " + car.getVehicle_model() + " - AB-456-ZE");

            }

            @Override
            public void failure(RetrofitError error) {

            }


        });
    }
    @OnClick(R.id.bus)
    public void onBus(LinearLayout linear) {
        Intent intent = new Intent(this, Trajet.class);
        startActivity(intent);
    }


    @OnClick(R.id.train)
    public void onTrain(LinearLayout linear) {
        Intent intent = new Intent(this, Trajet.class);
        startActivity(intent);
    }

    @OnClick(R.id.pied)
    public void onPied(LinearLayout linear) {
        Intent intent = new Intent(this, Trajet.class);
        startActivity(intent);
    }
}
