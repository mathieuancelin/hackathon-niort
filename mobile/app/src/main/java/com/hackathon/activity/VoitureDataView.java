package com.hackathon.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.location.Location;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hackathon.Application;
import com.hackathon.R;
import com.hackathon.ar3.utils.Camera;
import com.hackathon.ar3.utils.PaintUtils;
import com.hackathon.ar3.utils.RadarLines;
import com.hackathon.service.koolicar.KoolClient;
import com.hackathon.service.koolicar.Koolicar;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Currently the markers are plotted with reference to line parallel to the earth surface.
 * We are working to include the elevation and height factors.
 */


public class VoitureDataView {

    private RelativeLayout[] locationMarkerView;
    //ImageView[] subjectImageView;
    //RelativeLayout.LayoutParams[] layoutParams;
    //ViewGroup.LayoutParams[] layoutParams;
    //RelativeLayout.LayoutParams[] subjectImageViewParams;
    //RelativeLayout.LayoutParams[] subjectTextViewParams;
    //TextView[] locationTextView;



	/*
     *  Array or Array lists of latitude and longitude to plot
	 *  In your case you can populate with an ArrayList
	 * */
    // SF Art Commission, SF Dept. of Public Health, SF Ethics Comm, SF Conservatory of Music, All Star Cafe, Magic Curry Cart, SF SEO Marketing, SF Honda,
    // SF Mun Transport Agency, SF Parking Citation, Mayors Office of Housing, SF Redev Agency, Catario Patrice, Bank of America , SF Retirement System, Bank of America Mortage,
    // Writers Corp., Van Nes Keno Mkt.


    double[] latitudes = new double[]{46.115095};
    double[] longitudes = new double[]{-0.345130};

    int[] nextXofText;
    ArrayList<Integer> nextYofText = new ArrayList<Integer>();

    double[] bearings;
    float angleToShift;
    float yPosition;
    Location currentLocation = new Location("provider");
    Location destinedLocation = new Location("provider");

    public String[] places = new String[]{"Chize", "SF Art Commission", "SF Dept. of Public Health", "SF Ethics Comm", "SF Conservatory of Music", "All Star Cafe",
            "Magic Curry Cart", "SF SEO Marketing", " SF Honda", "SF Mun Transport Agency", "SF Parking Citation", "Mayors Office of Housing", "SF Redev Agency", "Catario Patrice", "Bank of America",
            "SF Retirement System", "Bank of America Mortage", "Writers Corp.", "Van Nes Keno Mkt."};
    /**
     * is the view Inited?
     */
    boolean isInit = false;
    boolean isDrawing = true;
    boolean isFirstEntry;
    Context _context;
    /**
     * width and height of the view
     */
    int width, height;
    android.hardware.Camera camera;

    float yawPrevious;
    float yaw = 0;
    float pitch = 0;
    float roll = 0;

    DisplayMetrics displayMetrics;
    VoitureRadarView radarPoints;

    RadarLines lrl = new RadarLines();
    RadarLines rrl = new RadarLines();
    float rx = 10, ry = 20;
    public float addX = 0, addY = 0;
    public float degreetopixelWidth;
    public float degreetopixelHeight;
    public float pixelstodp;
    public float bearing;

    public int[][] coordinateArray = new int[20][2];
    public int locationBlockWidth;
    public int locationBlockHeight;

    public float deltaX;
    public float deltaY;
    Bitmap bmp;

    public VoitureDataView(Context ctx) {
        this._context = ctx;
    }


    public boolean isInited() {
        return isInit;
    }

    public void init(int widthInit, int heightInit, android.hardware.Camera camera, DisplayMetrics displayMetrics, RelativeLayout rel) {
        try {

            LayoutInflater inflater = (LayoutInflater) rel.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            RechercheVoiture arView = new RechercheVoiture();
            locationMarkerView = new RelativeLayout[latitudes.length];
            nextXofText = new int[latitudes.length];

            for (int i = 0; i < latitudes.length; i++) {

                locationMarkerView[i] = (RelativeLayout) inflater.inflate(R.layout.layout_poi_voiture, rel);

                locationMarkerView[i].setId(i);

                OnClickListener listener = new OnClickListener() {
                    //	subjectImageView[i].setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (v.getId() != -1) {

                            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) locationMarkerView[v.getId()].getLayoutParams();
                            Rect rect = new Rect(params.leftMargin, params.topMargin, params.leftMargin + params.width, params.topMargin + params.height);
                            ArrayList<Integer> matchIDs = new ArrayList<Integer>();
                            Rect compRect = new Rect();
                            int index = 0;
                            //for (RelativeLayout.LayoutParams layoutparams : layoutParams) {

                            for (RelativeLayout layout : locationMarkerView) {
                                FrameLayout.LayoutParams layoutparams = (FrameLayout.LayoutParams) layout.getLayoutParams();
                                compRect.set(layoutparams.leftMargin, layoutparams.topMargin,
                                        layoutparams.leftMargin + layoutparams.width, layoutparams.topMargin + layoutparams.height);

                                if (compRect.intersect(rect)) {
                                    matchIDs.add(index);
                                }
                                index++;
                            }

                            if (matchIDs.size() > 1) {

                            }
                            Toast.makeText(_context, "Number of places here = " + matchIDs.size(), Toast.LENGTH_SHORT).show();

                            locationMarkerView[v.getId()].bringToFront();

                        }

                    }
                };


                locationMarkerView[i].setOnClickListener(listener);

                KoolClient.get().get(Application.CARUUID, new Callback<Koolicar>() {
                    @Override
                    public void success(Koolicar koolicar, Response response) {
                        longitudes[0] = koolicar.getLongitude();
                        latitudes[0] = koolicar.getLatitude();
                        calculBearing();
                        EventBus.getDefault().post(new LocationChanged());
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        System.out.println(error);
                    }
                });
            }


            bmp = BitmapFactory.decodeResource(_context.getResources(), R.drawable.wwe_logo);

            this.displayMetrics = displayMetrics;
            this.degreetopixelWidth = this.displayMetrics.widthPixels / camera.getParameters().getHorizontalViewAngle();
            this.degreetopixelHeight = this.displayMetrics.heightPixels / camera.getParameters().getVerticalViewAngle();

            bearings = new double[latitudes.length];
            currentLocation.setLatitude(46.315153);
            currentLocation.setLongitude(-0.410958);

            latitudes[0] = currentLocation.getLatitude();
            longitudes[0] = currentLocation.getLongitude();

            calculBearing();
            radarPoints = new VoitureRadarView(this, bearings);
            this.camera = camera;
            width = widthInit;
            height = heightInit;

            lrl.set(0, -VoitureRadarView.RADIUSDPI);
            lrl.rotate(Camera.DEFAULT_VIEW_ANGLE / 2);
            lrl.add(rx + VoitureRadarView.RADIUSDPI, ry + VoitureRadarView.RADIUSDPI);
            rrl.set(0, -VoitureRadarView.RADIUSDPI);
            rrl.rotate(-Camera.DEFAULT_VIEW_ANGLE / 2);
            rrl.add(rx + VoitureRadarView.RADIUSDPI, ry + VoitureRadarView.RADIUSDPI);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
		
		/*
		 * initialization is done, so dont call init() again.
		 * */
        isInit = true;
    }

    private void calculBearing() {
        if (bearing < 0)
            bearing = 360 + bearing;

        for (int i = 0; i < latitudes.length; i++) {
            destinedLocation.setLatitude(latitudes[i]);
            destinedLocation.setLongitude(longitudes[i]);
            bearing = currentLocation.bearingTo(destinedLocation);
            if (bearing < 0) {
                bearing = 360 + bearing;
            }
            bearings[i] = bearing;

        }
    }

    public void draw(PaintUtils dw, float yaw, float pitch, float roll) {


        this.yaw = yaw;
        this.pitch = pitch;
        this.roll = roll;

        // Draw Radar
        String dirTxt = "";
        int bearing = (int) this.yaw;
        int range = (int) (this.yaw / (360f / 16f));
        if (range == 15 || range == 0) dirTxt = "N";
        else if (range == 1 || range == 2) dirTxt = "NE";
        else if (range == 3 || range == 4) dirTxt = "E";
        else if (range == 5 || range == 6) dirTxt = "SE";
        else if (range == 7 || range == 8) dirTxt = "S";
        else if (range == 9 || range == 10) dirTxt = "SW";
        else if (range == 11 || range == 12) dirTxt = "W";
        else if (range == 13 || range == 14) dirTxt = "NW";


        radarPoints.view = this;

        dw.paintObj(radarPoints, rx + PaintUtils.XPADDING, ry + PaintUtils.YPADDING, -this.yaw, 1, this.yaw);
        dw.setFill(false);
        dw.setColor(Color.argb(100, 220, 0, 0));
        dw.paintLine(lrl.x, lrl.y, rx + VoitureRadarView.RADIUSDPI, ry + VoitureRadarView.RADIUSDPI);
        dw.paintLine(rrl.x, rrl.y, rx + VoitureRadarView.RADIUSDPI, ry + VoitureRadarView.RADIUSDPI);
        dw.setColor(Color.rgb(255, 255, 255));
        dw.setFontSize(12);
        radarText(dw, "" + bearing + ((char) 176) + " " + dirTxt, rx + VoitureRadarView.RADIUSDPI, ry - 5, true, false, -1);
        radarText(dw, "" + bearing + ((char) 176) + " " + dirTxt, rx + VoitureRadarView.RADIUSDPI, ry - 5, true, false, -1);


        drawTextBlock(dw);
    }


    void radarText(PaintUtils dw, String txt, float x, float y, boolean bg, boolean isLocationBlock, int count) {


        float padw = 4, padh = 2;
        float w = dw.getTextWidth(txt) + padw * 2;
        float h;
        if (isLocationBlock) {
            h = dw.getTextAsc() + dw.getTextDesc() + padh * 2 + 10;
        } else {
            h = dw.getTextAsc() + dw.getTextDesc() + padh * 2;
        }
        if (bg) {

            if (isLocationBlock) {
                FrameLayout.LayoutParams rl = (FrameLayout.LayoutParams) locationMarkerView[count].getLayoutParams();
                //layoutParams[count].setMargins((int)(x - w / 2 - 10), (int)(y - h / 2 - 10), 0, 0);
                //rl.setMargins((int)(x - w / 2 - 10), (int)(y - h / 2 - 10),0,0);
                rl.setMargins((int) (x - new RechercheVoiture().convertToPix(rl.width) / 2), (int) (y - h / 2), 0, 0);
                locationMarkerView[count].setLayoutParams(rl);

                //	layoutParams[count].height = 90;
                //	layoutParams[count].width = 90;
                //	locationMarkerView[count].setLayoutParams(layoutParams[count]);

            } else {
                dw.setColor(Color.rgb(0, 0, 255));
                dw.setFill(true);
                dw.paintRect((x - w / 2) + PaintUtils.XPADDING, (y - h / 2) + PaintUtils.YPADDING, w, h);
                pixelstodp = (padw + x - w / 2) / ((displayMetrics.density) / 160);
                dw.setColor(Color.rgb(255, 255, 255));
                dw.setFill(false);
                dw.paintText((padw + x - w / 2) + PaintUtils.XPADDING, ((padh + dw.getTextAsc() + y - h / 2)) + PaintUtils.YPADDING, txt);
            }
        }

    }

    String checkTextToDisplay(String str) {

        if (str.length() > 15) {
            str = str.substring(0, 15) + "...";
        }
        return str;

    }

    void drawTextBlock(PaintUtils dw) {
        RechercheVoiture arView = new RechercheVoiture();
        for (int i = 0; i < bearings.length; i++) {
            if (bearings[i] < 0) {

                if (this.pitch != 90) {
                    yPosition = (this.pitch - 90) * this.degreetopixelHeight + 200;
                } else {
                    yPosition = (float) this.height / 2;
                }

                bearings[i] = 360 - bearings[i];
                angleToShift = (float) bearings[i] - this.yaw;
                nextXofText[i] = (int) (angleToShift * degreetopixelWidth);
                yawPrevious = this.yaw;
                isDrawing = true;
                radarText(dw, places[i], nextXofText[i], yPosition, true, true, i);
                coordinateArray[i][0] = nextXofText[i];
                coordinateArray[i][1] = (int) yPosition;

            } else {
                angleToShift = (float) bearings[i] - this.yaw;
                //  System.out.println("bearing >0: " +bearings[i] + "   "+angleToShift);
                if (angleToShift > 180)
                    angleToShift = angleToShift - 360;
                if (this.pitch != 90) {
                    yPosition = (this.pitch - 90) * this.degreetopixelHeight + 200;
                } else {
                    yPosition = (float) this.height / 2;
                }


                nextXofText[i] = (int) ((displayMetrics.widthPixels / 2) + (angleToShift * degreetopixelWidth));
                if (Math.abs(coordinateArray[i][0] - nextXofText[i]) > 50) {
                    radarText(dw, places[i], (nextXofText[i]), yPosition, true, true, i);
                    coordinateArray[i][0] = (int) ((displayMetrics.widthPixels / 2) + (angleToShift * degreetopixelWidth));
                    coordinateArray[i][1] = (int) yPosition;

                    isDrawing = true;
                } else {
                    radarText(dw, places[i], coordinateArray[i][0], yPosition, true, true, i);
                    isDrawing = false;
                }
            }
        }
    }

    public class NearbyPlacesList extends BaseAdapter {

        ArrayList<Integer> matchIDs = new ArrayList<Integer>();

        public NearbyPlacesList(ArrayList<Integer> matchID) {
            matchIDs = matchID;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return matchIDs.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            return null;
        }

    }
}