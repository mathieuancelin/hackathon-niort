package com.hackathon.service.koolicar;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by grua687 on 24/03/2015.
 */
public interface KoolApi {
    @GET("/box/{id}")
    void get(@Path("id") String id,
                 Callback<Koolicar> callback);

    @POST("/box/{id}/lock")
    void lock(@Path("id") String id,
                   Callback<MonitorId> callback);

    @POST("/box/{id}/unlock")
    void unlock(@Path("id") String id,
                     Callback<MonitorId> callback);

    @GET("/box/{id}/monitor/{rid}")
    void monitor(@Path("id") String id, @Path("rid") String rid,
                 Callback<MonitorId> callback);
}
