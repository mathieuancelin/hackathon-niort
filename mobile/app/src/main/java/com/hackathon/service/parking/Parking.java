package com.hackathon.service.parking;


import com.hackathon.service.station.Location;

import java.util.List;

public class Parking {

    public String  _id;
    public String  SERVICE_VELO;
    public Integer CAPACITE_VOITURE;
    public Geo geo;
    public List<Double> _l;
    public Integer CODCOMMUNE;
    public Integer CAPACITE_VEHICULE_ELECTRIQUE;
    public Integer CODE_POSTAL;
    public String  TELEPHONE;
    public String  MOYEN_PAIEMENT;
    public String  AUTRES_SERVICE_MOB_PROX;
    public String  CONDITIONS_D_ACCES;
    public String  COMMUNE;
    public String  SERVICES;
    public Integer CAPACITE_VELO;
    public String  PRESENTATION;
    public Integer CAPACITE_PMR;
    public String  LIBCATEGORIE;
    public String  EXPLOITANT;
    public String  SITE_WEB;
    public String  ADRESSE;
    public String  LIBTYPE;
    public Integer _IDOBJ;

    public Parking() {
    }

    public Geo getGeo() {
        return geo;
    }

    public void setGeo(Geo geo) {
        this.geo = geo;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getSERVICE_VELO() {
        return SERVICE_VELO;
    }

    public void setSERVICE_VELO(String SERVICE_VELO) {
        this.SERVICE_VELO = SERVICE_VELO;
    }

    public Integer getCAPACITE_VOITURE() {
        return CAPACITE_VOITURE;
    }

    public void setCAPACITE_VOITURE(Integer CAPACITE_VOITURE) {
        this.CAPACITE_VOITURE = CAPACITE_VOITURE;
    }

    public List<Double> get_l() {
        return _l;
    }

    public void set_l(List<Double> _l) {
        this._l = _l;
    }

    public Integer getCODCOMMUNE() {
        return CODCOMMUNE;
    }

    public void setCODCOMMUNE(Integer CODCOMMUNE) {
        this.CODCOMMUNE = CODCOMMUNE;
    }

    public Integer getCAPACITE_VEHICULE_ELECTRIQUE() {
        return CAPACITE_VEHICULE_ELECTRIQUE;
    }

    public void setCAPACITE_VEHICULE_ELECTRIQUE(Integer CAPACITE_VEHICULE_ELECTRIQUE) {
        this.CAPACITE_VEHICULE_ELECTRIQUE = CAPACITE_VEHICULE_ELECTRIQUE;
    }

    public Integer getCODE_POSTAL() {
        return CODE_POSTAL;
    }

    public void setCODE_POSTAL(Integer CODE_POSTAL) {
        this.CODE_POSTAL = CODE_POSTAL;
    }

    public String getTELEPHONE() {
        return TELEPHONE;
    }

    public void setTELEPHONE(String TELEPHONE) {
        this.TELEPHONE = TELEPHONE;
    }

    public String getMOYEN_PAIEMENT() {
        return MOYEN_PAIEMENT;
    }

    public void setMOYEN_PAIEMENT(String MOYEN_PAIEMENT) {
        this.MOYEN_PAIEMENT = MOYEN_PAIEMENT;
    }

    public String getAUTRES_SERVICE_MOB_PROX() {
        return AUTRES_SERVICE_MOB_PROX;
    }

    public void setAUTRES_SERVICE_MOB_PROX(String AUTRES_SERVICE_MOB_PROX) {
        this.AUTRES_SERVICE_MOB_PROX = AUTRES_SERVICE_MOB_PROX;
    }

    public String getCONDITIONS_D_ACCES() {
        return CONDITIONS_D_ACCES;
    }

    public void setCONDITIONS_D_ACCES(String CONDITIONS_D_ACCES) {
        this.CONDITIONS_D_ACCES = CONDITIONS_D_ACCES;
    }

    public String getCOMMUNE() {
        return COMMUNE;
    }

    public void setCOMMUNE(String COMMUNE) {
        this.COMMUNE = COMMUNE;
    }

    public String getSERVICES() {
        return SERVICES;
    }

    public void setSERVICES(String SERVICES) {
        this.SERVICES = SERVICES;
    }

    public Integer getCAPACITE_VELO() {
        return CAPACITE_VELO;
    }

    public void setCAPACITE_VELO(Integer CAPACITE_VELO) {
        this.CAPACITE_VELO = CAPACITE_VELO;
    }

    public String getPRESENTATION() {
        return PRESENTATION;
    }

    public void setPRESENTATION(String PRESENTATION) {
        this.PRESENTATION = PRESENTATION;
    }

    public Integer getCAPACITE_PMR() {
        return CAPACITE_PMR;
    }

    public void setCAPACITE_PMR(Integer CAPACITE_PMR) {
        this.CAPACITE_PMR = CAPACITE_PMR;
    }

    public String getLIBCATEGORIE() {
        return LIBCATEGORIE;
    }

    public void setLIBCATEGORIE(String LIBCATEGORIE) {
        this.LIBCATEGORIE = LIBCATEGORIE;
    }

    public String getEXPLOITANT() {
        return EXPLOITANT;
    }

    public void setEXPLOITANT(String EXPLOITANT) {
        this.EXPLOITANT = EXPLOITANT;
    }

    public String getSITE_WEB() {
        return SITE_WEB;
    }

    public void setSITE_WEB(String SITE_WEB) {
        this.SITE_WEB = SITE_WEB;
    }

    public String getADRESSE() {
        return ADRESSE;
    }

    public void setADRESSE(String ADRESSE) {
        this.ADRESSE = ADRESSE;
    }

    public String getLIBTYPE() {
        return LIBTYPE;
    }

    public void setLIBTYPE(String LIBTYPE) {
        this.LIBTYPE = LIBTYPE;
    }

    public Integer get_IDOBJ() {
        return _IDOBJ;
    }

    public void set_IDOBJ(Integer _IDOBJ) {
        this._IDOBJ = _IDOBJ;
    }
}
