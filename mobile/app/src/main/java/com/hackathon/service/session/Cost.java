package com.hackathon.service.session;

/**
 * Created by mathieuancelin on 28/03/2015.
 */
public class Cost {

    public Double distanceCost;
    public Double hourlyCost;

    public Cost() {
    }

    public Double getDistanceCost() {
        return distanceCost;
    }

    public void setDistanceCost(Double distanceCost) {
        this.distanceCost = distanceCost;
    }

    public Double getHourlyCost() {
        return hourlyCost;
    }

    public void setHourlyCost(Double hourlyCost) {
        this.hourlyCost = hourlyCost;
    }
}
