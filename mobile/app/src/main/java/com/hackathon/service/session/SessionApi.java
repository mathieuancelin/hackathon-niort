package com.hackathon.service.session;

import com.hackathon.service.koolicar.Koolicar;
import com.hackathon.service.koolicar.MonitorId;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;


public interface SessionApi {
    @POST("/session/{uuid}/{carId}")
    void createSession(@Path("uuid") String id, @Path("carId") String carId,
              Callback<Session> callback);

    @GET("/session/{uuid}")
    void getSession(@Path("uuid") String id,
             Callback<Session> callback);

    @PUT("/session/{uuid}")
    void endSession(@Path("uuid") String id,
                Callback<Session> callback);


}
