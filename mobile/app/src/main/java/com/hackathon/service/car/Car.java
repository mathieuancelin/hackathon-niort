package com.hackathon.service.car;

import java.util.List;

/**
 * Created by mathieuancelin on 28/03/2015.
 */
public class Car {

    public String id;
    public String brand;
    public String vehicle_model;
    public Integer year;
    public String category;
    public Integer doors_count;
    public Integer places_count;
    public Integer fuel_type_cd;
    public Integer gears_type_cd;
    public Double longitude;
    public Double latitude;
    public String description;
    public List<Integer> options;
    public List<String> pictures_url;
    public Boolean can_drive;

    public Car() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getVehicle_model() {
        return vehicle_model;
    }

    public void setVehicle_model(String vehicle_model) {
        this.vehicle_model = vehicle_model;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getDoors_count() {
        return doors_count;
    }

    public void setDoors_count(Integer doors_count) {
        this.doors_count = doors_count;
    }

    public Integer getPlaces_count() {
        return places_count;
    }

    public void setPlaces_count(Integer places_count) {
        this.places_count = places_count;
    }

    public Integer getFuel_type_cd() {
        return fuel_type_cd;
    }

    public void setFuel_type_cd(Integer fuel_type_cd) {
        this.fuel_type_cd = fuel_type_cd;
    }

    public Integer getGears_type_cd() {
        return gears_type_cd;
    }

    public void setGears_type_cd(Integer gears_type_cd) {
        this.gears_type_cd = gears_type_cd;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Integer> getOptions() {
        return options;
    }

    public void setOptions(List<Integer> options) {
        this.options = options;
    }

    public List<String> getPictures_url() {
        return pictures_url;
    }

    public void setPictures_url(List<String> pictures_url) {
        this.pictures_url = pictures_url;
    }

    public Boolean getCan_drive() {
        return can_drive;
    }

    public void setCan_drive(Boolean can_drive) {
        this.can_drive = can_drive;
    }
}
