package com.hackathon.service.parking;

import com.hackathon.service.koolicar.Koolicar;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by mathieuancelin on 28/03/2015.
 */
public interface ParkingApi {

    @GET("/parking/near/lng/{lng}/lat/{lat}/dmax/{dmax}")
    void getNearParkings(@Path("lat") Double lat, @Path("lng") Double lng, @Path("dmax") Integer distance,
             Callback<List<Parking>> callback);

    @GET("/parking/near/lng/{lng}/lat/{lat}/dmax/{dmax}/count")
    void countNearParkings(@Path("lat") Double lat, @Path("lng") Double lng, @Path("dmax") Double distance,
             Callback<ParkingCount> callback);
}
