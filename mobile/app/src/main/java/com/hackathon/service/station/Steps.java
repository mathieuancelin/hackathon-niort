package com.hackathon.service.station;

import java.util.List;

/**
 * Created by mathieuancelin on 28/03/2015.
 */
public class Steps {

    public List<Step> steps;

    public Steps() {
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }
}
