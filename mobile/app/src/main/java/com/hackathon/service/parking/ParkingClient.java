package com.hackathon.service.parking;

import com.hackathon.Application;
import com.hackathon.service.session.SessionApi;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by mathieuancelin on 28/03/2015.
 */
public class ParkingClient {


    private static ParkingApi REST_CLIENT;




    static {
        setupRestClient();
    }

    private ParkingClient() {}

    public static ParkingApi get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(Application.ROOT)
                .setClient(new OkClient(new OkHttpClient()))
                .setLogLevel(RestAdapter.LogLevel.FULL);

        RestAdapter restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(ParkingApi.class);
    }
}
