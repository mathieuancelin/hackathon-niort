package com.hackathon.service.station;

import java.util.List;

/**
 * Created by mathieuancelin on 28/03/2015.
 */
public class Station {

    public String _id;
    public List<Double> location;
    public String name;
    public String type;
    public List<String> lines;

    public Station() {
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public List<Double> getLocation() {
        return location;
    }

    public void setLocation(List<Double> location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getLines() {
        return lines;
    }

    public void setLines(List<String> lines) {
        this.lines = lines;
    }
}
