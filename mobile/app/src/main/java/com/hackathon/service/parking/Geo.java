package com.hackathon.service.parking;

/**
 * Created by mathieuancelin on 29/03/2015.
 */
public class Geo {

    public String name;

    public Geo() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
