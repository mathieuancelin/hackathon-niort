package com.hackathon.service.station;

/**
 * Created by mathieuancelin on 28/03/2015.
 */
public class Locations {

    public Location start;

    public Location end;

    public Locations() {
    }

    public Locations(Location start, Location end) {
        this.start = start;
        this.end = end;
    }

    public Location getStart() {
        return start;
    }

    public void setStart(Location start) {
        this.start = start;
    }

    public Location getEnd() {
        return end;
    }

    public void setEnd(Location end) {
        this.end = end;
    }
}
