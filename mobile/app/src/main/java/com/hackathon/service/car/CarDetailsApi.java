package com.hackathon.service.car;

import com.hackathon.service.session.Session;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by mathieuancelin on 28/03/2015.
 */
public interface CarDetailsApi {

    @GET("/car/{id}")
    void details(@Path("id") String id, Callback<Car> callback);
}
