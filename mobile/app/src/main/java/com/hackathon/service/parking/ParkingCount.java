package com.hackathon.service.parking;

/**
 * Created by mathieuancelin on 28/03/2015.
 */
public class ParkingCount {
    public Integer count;

    public ParkingCount() {
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
