package com.hackathon.service.koolicar;

import com.hackathon.Application;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by grua687 on 24/03/2015.
 */
public class KoolClient {
    private static KoolApi REST_CLIENT;
//    private static String ROOT =
 //           "http://localhost:9000";



    static {
        setupRestClient();
    }

    private KoolClient() {}

    public static KoolApi get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(Application.ROOT)
                .setClient(new OkClient(new OkHttpClient()))
                .setLogLevel(RestAdapter.LogLevel.FULL);

        RestAdapter restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(KoolApi.class);
    }
}
