package com.hackathon.service.koolicar;

/**
 * Created by grua687 on 24/03/2015.
 */
public class Koolicar {
    public String updated_at_utc;
    public Long vehicle_m;
    public Double latitude;
    public String connection_status;
    public String serial_number;
    public String public_id;

    public String mode;
    public String functional_state;
    public Long updated_at_ts;
    public String technical_state_string;
    public Integer current_rfid;
    public Integer matrice_state;
    public String comment;
    public String phone_number;
    public Double longitude;
    private String name;
    private int ressourceId;

    public int getRessourceId() {
        return ressourceId;
    }

    public void setRessourceId(int ressourceId) {
        this.ressourceId = ressourceId;
    }

    public String getUpdated_at_utc() {
        return updated_at_utc;
    }

    public void setUpdated_at_utc(String updated_at_utc) {
        this.updated_at_utc = updated_at_utc;
    }

    public Long getVehicle_m() {
        return vehicle_m;
    }

    public void setVehicle_m(Long vehicle_m) {
        this.vehicle_m = vehicle_m;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getConnection_status() {
        return connection_status;
    }

    public void setConnection_status(String connection_status) {
        this.connection_status = connection_status;
    }

    public String getSerial_number() {
        return serial_number;
    }

    public void setSerial_number(String serial_number) {
        this.serial_number = serial_number;
    }

    public String getPublic_id() {
        return public_id;
    }

    public void setPublic_id(String public_id) {
        this.public_id = public_id;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getFunctional_state() {
        return functional_state;
    }

    public void setFunctional_state(String functional_state) {
        this.functional_state = functional_state;
    }

    public Long getUpdated_at_ts() {
        return updated_at_ts;
    }

    public void setUpdated_at_ts(Long updated_at_ts) {
        this.updated_at_ts = updated_at_ts;
    }

    public String getTechnical_state_string() {
        return technical_state_string;
    }

    public void setTechnical_state_string(String technical_state_string) {
        this.technical_state_string = technical_state_string;
    }

    public Integer getCurrent_rfid() {
        return current_rfid;
    }

    public void setCurrent_rfid(Integer current_rfid) {
        this.current_rfid = current_rfid;
    }

    public Integer getMatrice_state() {
        return matrice_state;
    }

    public void setMatrice_state(Integer matrice_state) {
        this.matrice_state = matrice_state;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
    public Koolicar() {

    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
