package com.hackathon.service.session;

/**
 * Created by Gaetan on 28/03/2015.
 */
public class Session {

    public String   _id          ;
    public String   uuid         ;
    public String   carId        ;
    public Long start        ;
    public Long stop         ;
    public Long     totalDistance;
    public Boolean  done         ;
    public Long startMeters;
    public Long stopMeters;
    public Cost cost;
    public Long distance;

    public Session() {
    }

    public Long getDistance() {
        return distance;
    }

    public void setDistance(Long distance) {
        this.distance = distance;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getStop() {
        return stop;
    }

    public void setStop(Long stop) {
        this.stop = stop;
    }

    public Long getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(Long totalDistance) {
        this.totalDistance = totalDistance;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public Long getStartMeters() {
        return startMeters;
    }

    public void setStartMeters(Long startMeters) {
        this.startMeters = startMeters;
    }

    public Long getStopMeters() {
        return stopMeters;
    }

    public void setStopMeters(Long stopMeters) {
        this.stopMeters = stopMeters;
    }

    public Cost getCost() {
        return cost;
    }

    public void setCost(Cost cost) {
        this.cost = cost;
    }
}
