package com.hackathon.service.koolicar;

import java.util.Date;

/**
 * Created by Gaetan on 28/03/2015.
 */
public class Reservation {

    Date datedebut;
    Date datefin;
    Koolicar car;


    public Date getDatedebut() {
        return datedebut;
    }

    public void setDatedebut(Date datedebut) {
        this.datedebut = datedebut;
    }

    public Date getDatefin() {
        return datefin;
    }

    public void setDatefin(Date datefin) {
        this.datefin = datefin;
    }

    public Koolicar getCar() {
        return car;
    }

    public void setCar(Koolicar car) {
        this.car = car;
    }
}
