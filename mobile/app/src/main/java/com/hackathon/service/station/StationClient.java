package com.hackathon.service.station;

import com.hackathon.Application;
import com.hackathon.service.session.SessionApi;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by mathieuancelin on 28/03/2015.
 */
public class StationClient {

    private static StationApi REST_CLIENT;




    static {
        setupRestClient();
    }

    private StationClient() {}

    public static StationApi get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(Application.ROOT)
                .setClient(new OkClient(new OkHttpClient()))
                .setLogLevel(RestAdapter.LogLevel.FULL);

        RestAdapter restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(StationApi.class);
    }
}
