package com.hackathon.service.session;

import com.hackathon.Application;
import com.hackathon.service.koolicar.KoolApi;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by grua687 on 24/03/2015.
 */
public class SessionClient {
    private static SessionApi REST_CLIENT;




    static {
        setupRestClient();
    }

    private SessionClient() {}

    public static SessionApi get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(Application.ROOT)
                .setClient(new OkClient(new OkHttpClient()))
                .setLogLevel(RestAdapter.LogLevel.FULL);

        RestAdapter restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(SessionApi.class);
    }
}
