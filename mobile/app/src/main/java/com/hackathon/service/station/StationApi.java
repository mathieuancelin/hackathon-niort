package com.hackathon.service.station;

import com.hackathon.service.session.Session;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by mathieuancelin on 28/03/2015.
 */
public interface StationApi {

    @POST("/station/near")
    void steps(@Body Locations locations, Callback<List<Step>> callback);

}
