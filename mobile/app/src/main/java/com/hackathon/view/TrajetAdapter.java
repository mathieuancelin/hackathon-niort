/*
 * Copyright 2014 Soichiro Kashima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hackathon.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hackathon.R;
import com.hackathon.activity.ArriveeVoiture;
import com.hackathon.activity.DashBoard;
import com.hackathon.activity.DetailVoiture;
import com.hackathon.activity.RechercheParking;
import com.hackathon.service.koolicar.Reservation;
import com.hackathon.service.station.Step;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public class TrajetAdapter extends ArrayAdapter<Step> {
    public TrajetAdapter(Context context, List<Step> resas) {
        super(context, 0, resas);


    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Step step = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_trajet, parent, false);
        }
        // Lookup view for data population
        // ImageView image = (ImageView) convertView.findViewById(R.id.reservation_image);
        //TextView date = (TextView) convertView.findViewById(R.id.reservation_date_heure);

        TextView nom = (TextView) convertView.findViewById(R.id.reservation_nom);
        ImageView reservation_image = (ImageView) convertView.findViewById(R.id.reservation_image);
        LinearLayout cell_layout = (LinearLayout) convertView.findViewById(R.id.cell_layout);


        // Populate the data into the template view using the data object
        nom.setText(step.getStartPoint() + " " + step.getEndPoint());

        //date.setText(step.getTimeLeft());

        if (step.getType().equals("PIED")) {
            reservation_image.setBackgroundResource(R.drawable.walk);
        } else if (step.getType().equals("BUS")) {
            reservation_image.setBackgroundResource(R.drawable.bus);
        } else if (step.getType().equals("SUBWAY")) {
            reservation_image.setBackgroundResource(R.drawable.subway);
        } else {
            nom.setText(step.getTimeLeft());
            //nom.setTextAppearance(, R.style.Base_TextAppearance_Ma);
            reservation_image.setBackgroundResource(R.drawable.petitspoints);
            nom.setTextColor(R.color.secondary_text);
            cell_layout.setBackgroundResource(R.color.background_material_light);
        }
        // Return the completed view to render on screen

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ArriveeVoiture.class);
                //Intent intent = new Intent(v.getContext(), com.hackathon.activity.RechercheParking.class);
                v.getContext().startActivity(intent);
            }
        });
        return convertView;
    }
}
