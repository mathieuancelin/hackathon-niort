/*
 * Copyright 2014 Soichiro Kashima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hackathon.view;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.hackathon.R;
import com.hackathon.activity.DetailVoiture;
import com.hackathon.service.koolicar.Reservation;
import com.hackathon.service.parking.Parking;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ParkingAdapter extends ArrayAdapter<Parking> {

    public ParkingAdapter(Context context, List<Parking> resas) {
        super(context, 0, resas);
    }

    public static AtomicInteger counter = new AtomicInteger(5);

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final Parking parking = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_parking, parent, false);
        }
        // Lookup view for data population
       // ImageView image = (ImageView) convertView.findViewById(R.id.reservation_image);


        TextView parkingDuree = (TextView) convertView.findViewById(R.id.parkin_place);

        TextView nom = (TextView) convertView.findViewById(R.id.reservation_nom);

        TextView parkingPlace = (TextView) convertView.findViewById(R.id.parking_duree);
        // Populate the data into the template view using the data object
        nom.setText(parking.getGeo().getName());
        parkingPlace.setText(parking.getCAPACITE_VOITURE() + " places");
        parkingDuree.setText( counter.get() + " min");
        counter.addAndGet(2);
      //  image.setBackgroundResource(R.drawable.wwe_logo);
        //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM");

        //SimpleDateFormat simpleHeureFormat = new SimpleDateFormat("HH:mm");

     //   date.setText(simpleDateFormat.format(user.getDatedebut())+" " +simpleHeureFormat.format(user.getDatedebut())+  " - "+simpleHeureFormat.format(user.getDatefin()));
        // Return the completed view to render on screen

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO : appeler maps

                String lat = parking._l.get(0).toString().replace(",", ".");
                String lon = parking._l.get(1).toString().replace(",", ".");
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=47.25310,-1.57010&daddr=" + lat + "," + lon));
                v.getContext().startActivity(intent);
                //Intent intent = new Intent(v.getContext(), DetailVoiture.class);
                //v.getContext().startActivity(intent);


            }
        });
        return convertView;
    }
}
