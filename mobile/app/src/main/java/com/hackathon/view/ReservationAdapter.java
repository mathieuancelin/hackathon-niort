/*
 * Copyright 2014 Soichiro Kashima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hackathon.view;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hackathon.R;
import com.hackathon.activity.DetailVoiture;
import com.hackathon.service.koolicar.Reservation;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public class ReservationAdapter extends ArrayAdapter<Reservation> {
    public ReservationAdapter(Context context, List<Reservation> resas) {
        super(context, 0, resas);


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Reservation user = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_reservation, parent, false);
        }
        // Lookup view for data population
       // ImageView image = (ImageView) convertView.findViewById(R.id.reservation_image);
        TextView date = (TextView) convertView.findViewById(R.id.reservation_date_heure);

        TextView nom = (TextView) convertView.findViewById(R.id.reservation_nom);


        ImageView image = (ImageView) convertView.findViewById(R.id.reservation_image);
        image.setBackgroundResource(user.getCar().getRessourceId());
        // Populate the data into the template view using the data object
        nom.setText(user.getCar().getName());
      //  image.setBackgroundResource(R.drawable.wwe_logo);
System.out.print(position);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM");

        SimpleDateFormat simpleHeureFormat = new SimpleDateFormat("HH:mm");

        date.setText(simpleDateFormat.format(user.getDatedebut())+" " +simpleHeureFormat.format(user.getDatedebut())+  " - "+simpleHeureFormat.format(user.getDatefin()));
        // Return the completed view to render on screen
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), DetailVoiture.class);

               v.getContext().startActivity(intent);


            }
        });
        return convertView;
    }
}
